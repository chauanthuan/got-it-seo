var functions = require('firebase-functions');
var querystring = require('querystring');
var admin = require("firebase-admin");
var https = require('https');

var creditCardType = require('credit-card-type');

//var cons = require('consolidate');
var request = require('request');

const crypto = require('crypto');
const CryptoJS = require("crypto-js");
const moment = require('moment');
const uuidv5 = require('uuid/v5');
const uuidv4 = require('uuid/v4');

const express = require('express');
const cookieParser = require('cookie-parser')();
const cors = require('cors')({
    origin: true
});

const gcs = require('@google-cloud/storage')();
const path = require('path');
const os = require('os');
const fs = require('fs');

const app = express();
const checkout = express();
const momo = express();
const send = express();

admin.initializeApp(functions.config().firebase);

var db = admin.firestore();

var realTimeDB = admin.database();

// app.get('/hellofirestore', function (req, res) {
//     var uid = 'TH314vY5zbeUJTmQA5JWBznpCjo1';
//
//     var fileName = 'hahmjt@gmail.com';
//     var tempFilePath = "C:\\Users\\hahmj\\Downloads\\hahmjt.txt";
//
//     admin.initializeApp(functions.config().firebase);
//
//     var bucket = admin.storage().bucket();
//
//     fs.readFile(tempFilePath, 'utf8', function(err, data) {
//           var userData = JSON.parse(data);
//
//
//           // admin.auth().updateUser(user.uid, {
//           //   phoneNumber: '+84'+userData.customer.phone.replace(/^0/g,''),
//           //   emailVerified: true
//           // })
//           // .then(function(userRecord) {
//           //   // See the UserRecord reference doc for the contents of userRecord.
//           //   console.log("Successfully updated user", userRecord.toJSON());
//           // })
//           // .catch(function(error) {
//           //   console.log("Error updating user:", error);
//           // });
//           //
//           // admin.firestore().collection("user").doc(user.uid).set(userData.customer)
//           // .then(function() { console.log('Customer Data Migrated'); })
//           // .catch(function(error) { console.log('Customer Data Migration Error'); });
//
//
//           var voucherLog = userData.voucher_log
//
//
//
//
//           for(var k in voucherLog) {
//             voucherLog[k].legacy = true;
//             voucherLog[k].quantity = 1;
//             voucherLog[k].step = 3;
//
//             admin.firestore().collection("user").doc(uid).collection("sent").doc(voucherLog[k].invoiceNo).set(voucherLog[k])
//             .then(function() { console.log('Log Data'+voucherLog[k].invoiceNo+' Data Migrated'); })
//             .catch(function(error) { console.log('Log Data'+voucherLog[k].invoiceNo+' Data Migration Error');});
//           }
//     });
//
// });


//tesst make html template
//var fs = require('fs') // this engine requires the fs module
checkout.engine('ntl', function (filePath, options, callback) { // define the template engine
  fs.readFile(filePath, function (err, content) {
    if (err) return callback(err)
    // this is an extremely simple template engine
    var rendered = content.toString().replace('#title#', '<title>' + options.title + '</title>')
    .replace('#message#', options.message)
    .replace('#imageUrl#',options.imageUrl)
    .replace('#dataResponse#', options.dataResponse)
    .replace('#result#',options.result)
    .replace('#cybersource_url#', options.cybersource_url)
    return callback(null, rendered)
  })
})
checkout.set('views', './views') // specify the views directory

checkout.set('view engine', 'ntl') // register the template engine


momo.engine('ntl', function (filePath, options, callback) { // define the template engine
  fs.readFile(filePath, function (err, content) {
    if (err) return callback(err)
    // this is an extremely simple template engine
    var rendered = content.toString().replace('#title#', options.title)
    .replace('#message#', options.message)
    .replace('#imageUrl#',options.imageUrl)
    .replace('#dataResponse#', options.dataResponse)
    .replace('#result#',options.result)
    .replace('#cybersource_url#', options.cybersource_url)
    return callback(null, rendered)
  })
})
momo.set('views', './views') // specify the views directory

momo.set('view engine', 'ntl') // register the template engine

// checkout.get('/', function (req, res) {
//   res.render('index', { title: 'Hey', message: 'Hello there!', imageUrl: ''});
// })

// checkout.get('/failed', function (req, res) {
//   res.render('failed', { title: '', message: 'Payment Failed!', imageUrl: 'https://img.gotit.vn/close-white.png' });
// })
// checkout.get('/waiting', function (req, res) {
//   res.render('waiting', { title: '', message: 'Payment Processing!', imageUrl: 'https://img.gotit.vn/spin.gif', dataResponse: 123  });
// })


checkout.use(express.static('public'));

momo.use(express.static('public'));

momo.post('/atm/checkout', function(req, res) {

  var body = req.body;

  var paymentId = body.paymentSession;
  var uid = body.uid;
  console.log(body);
  if(paymentId && uid) {

    var paymentRef = db.collection('user').doc(uid).collection('sent').doc(paymentId);

    var getDoc = paymentRef.get().then(function(doc) {
      res.header("Access-Control-Allow-Origin", functions.config().cors.origin);

      if (!doc.exists) {

        res.json({
          success: false,
          error: 'Payment ' + paymentId + ' not found'
        });

      } else {
        var paymentDocument = doc.data();

        var totalAmount = paymentDocument.totalAmount;

        var partnerCode = functions.config().momo.partner_code;
        var accessKey = functions.config().momo.access_key;
        var secretKey = functions.config().momo.secret_key;
        var orderInfo = "pay with MoMo ATM";
        var bankCode = body.atmBank;
        var returnUrl = functions.config().app.base_url + '/momo/atm/return?paymentRef=' + paymentId + '_' + uid;
        // var returnUrl = 'https://vnexpress.net';
        var notifyUrl = functions.config().app.base_url + '/momo/atm/return?paymentRef=' + paymentId + '_' + uid;
        var amount = totalAmount + '';
        var orderId = moment().valueOf()  + '';
        var requestId = moment().valueOf() + '';
        var requestType = "payWithMoMoATM";
        var extraData = paymentId + '_' + uid;
        //before sign HMAC SHA256 signature
        var rawHash = "partnerCode=" + partnerCode + "&accessKey=" + accessKey + "&requestId=" + requestId + "&bankCode=" + bankCode + "&amount=" + amount + "&orderId=" + orderId + "&orderInfo=" + orderInfo + "&returnUrl="  + returnUrl + "&notifyUrl=" + notifyUrl + "&extraData=" + extraData + "&requestType=" + requestType;

        var bytes = CryptoJS.HmacSHA256(rawHash, secretKey);

        var signature = bytes.toString(CryptoJS.enc.Hex);

        var data = {
          'partnerCode': partnerCode,
          'accessKey': accessKey,
          'requestId': requestId,
          'amount': amount,
          'orderId': orderId,
          'orderInfo': orderInfo,
          'returnUrl': returnUrl,
          'bankCode': bankCode,
          'notifyUrl': notifyUrl,
          'extraData': extraData,
          'requestType': requestType,
          'signature': signature
        };

        console.log(data);

        var headers = {
          'Content-Type': 'application/json',
          'Content-Length': JSON.stringify(data).length
        };

        var options = {
          host: 'payment.momo.vn',
          port: 18081,
          path: '/gw_payment/transactionProcessor',
          method: 'POST',
          headers: headers
        };

        var req = https.request(options, function (result) {
          result.setEncoding('utf-8');

          var responseString = '';

          result.on('data', function (data) {
            responseString += data;
          });

          result.on('end', function () {
            console.log(responseString);
            var resObject = JSON.parse(responseString);
            res.header("Access-Control-Allow-Origin", functions.config().cors.origin);
            res.send(resObject);
          });
        });
        var dataString = JSON.stringify(data);
        req.write(dataString);
        req.end();

      }
    });
  }

});

momo.get('/atm/return', function(req, res) {
  console.log('atm callback from MoMo');
  var data = req.query;

  var errorCode = data.errorCode;

  if(errorCode == 0) {
    var referenceId = data.paymentRef;

    var paymentId = referenceId.split('_')[0];
    var uid = referenceId.split('_')[1];

    console.log(paymentId + '_' + uid);
    var paymentRef = db.collection('user').doc(uid).collection('sent').doc(paymentId);


  res.header("Access-Control-Allow-Origin", functions.config().cors.origin);
  paymentRef.get().then(function(doc) {
      var result2 = {
        success: false
      };
      if (doc.exists) {
        var payment = doc.data();

        var priceId = payment.priceID;
        var productId = payment.productID;
        var quantity = payment.quantity;
        var now = moment();
        var refId = functions.config().biz.campaign_name + now.format('MMMYYYY');
        var expireDate = now.add(3, 'months').format('YYYY-MM-DD');

        var sendType = payment.sendType;

        if (quantity <= 1) {
          var params = {
            "productId": productId,
            "productPriceId": priceId,
            "refId": refId,
            "quantity": quantity,
            "campaignNm": refId,
            "expiryDate": expireDate,
            // 3 tháng kể từ ngày đặt
            "voucherRefId": paymentId
          };

          var endpoint = '/api/transaction';
        } else {
          var params = {
            "productList": [{
              "productId": productId,
              "productPriceId": priceId,
              "quantity": quantity,
            }],
            "refId": refId,
            "campaignNm": refId,
            "expiryDate": expireDate,
            // 3 tháng kể từ ngày đặt
            "voucherRefId": paymentId
          };

          var endpoint = '/api/transactiongroup';
        }

        performRequest(functions.config().biz.url, endpoint, 'POST', params, function(result) {
          if (result) {
            if (quantity <= 1) var voucher = result[0].vouchers[0];
            else var voucher = result[0].groupVouchers;

            var batch = db.batch();

            batch.update(paymentRef, {
              legacy: false,
              transaction: data,
              voucherLink: voucher.voucherLink,
              voucherLinkCode: voucher.voucherLinkCode,
              step: 2,
              state:5
            });

            batch.commit().then(function() {

              var dataSendToParent = {
                success: true
              };

              // var html = '<html>';
              // html += "<head>\n" + "    <title>Payment Successfully</title>\n" + "<script\n" + "  src='https://code.jquery.com/jquery-1.12.4.min.js'\n" + "  integrity='sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ='\n" + "  crossorigin='anonymous'></script>\n" + "</head>";
              // html += '<body>';
              // html += "<body style='font-family:Gotham Rounded,Roboto,Helvetica,Arial,sans-serif; overflow-x:hidden'>";
              // html += "<div class='loadingPayment' style='width: 100%;text-align: center;position: absolute;top: 50%;-webkit-transform: translateY(-50%);transform: translateY(-50%);'>"
              // html += "<p class='img' style='font-family: Gotham Rounded;font-size: 16px;font-weight: 500;font-style: normal;letter-spacing: normal;color: #636363;text-align: center;'>";
              // html += "<span style='width: 75px;height: 75px;display: inline-block;''>";
              // html += "<img style='width:75px; height: 75px' src='https://img.gotit.vn/success_icon.png'/>";
              // html += "</span>";
              // html += "</p>";
              // html += " <h2 class='iframe_title'>Payment Processing</h2>";
              // html += '<script>\n' + '    // addEventListener support for IE8\n' + '    function bindEvent(element, eventName, eventHandler) {\n' + '        if (element.addEventListener) {\n' + '            element.addEventListener(eventName, eventHandler, false);\n' + '        } else if (element.attachEvent) {\n' + '            element.attachEvent(\'on\' + eventName, eventHandler);\n' + '        }\n' + '    }\n' + '    // Send a message to the parent\n' + '    var sendDataBackToParent = function () {\n' + '        window.opener.postMessage(' + JSON.stringify(dataSendToParent) + ', \'*\');\n' + '    };\n' + '\n' + '    sendDataBackToParent();\n' + '\n' + '</script></body></html>';
              // res.send(html);

              // res.send({ title: 'Payment Success', message: 'Payment Success!', imageUrl: '../images/close-white.png' , dataResponse: 'OK', result: true });
              res.render('momoresult', { title: 'Payment Success', message: 'Payment Success!', imageUrl: 'https://img.gotit.vn/success_icon.png' , dataResponse: JSON.stringify(dataSendToParent), result: true });

            });
          }
        });
      }else {
        // res.render('momoresult', { title: 'Payment Failed', message: 'Payment Failed!', imageUrl: 'https://img.gotit.vn/close-white.png' , dataResponse: JSON.stringify(result2), result: true });

        res.send({ title: 'Payment Failed', message: 'Payment Failed!', imageUrl: '../images/close-white.png' , dataResponse: 'FAILED', result: false });
      }
    });
  }else {

    var dataSendToParent = {
      success: false
    };

    // var html = '<html>';
    // html += "<head>\n" + "    <title>Secure Acceptance - API Payment Form Example</title>\n" + "<script\n" + "  src='https://code.jquery.com/jquery-1.12.4.min.js'\n" + "  integrity='sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ='\n" + "  crossorigin='anonymous'></script>\n" + "</head>";
    // html += "<body style='font-family:Gotham Rounded,Roboto,Helvetica,Arial,sans-serif; overflow-x:hidden'>";
    // html += "<div class='loadingPayment' style='width: 100%;text-align: center;position: absolute;top: 50%;-webkit-transform: translateY(-50%);transform: translateY(-50%);'>"
    // html += "<p class='img' style='font-family: Gotham Rounded;font-size: 16px;font-weight: 500;font-style: normal;letter-spacing: normal;color: #636363;text-align: center;'>";
    // html += "<span style='width: 75px;height: 75px;position:relative;display: inline-block;border-radius:50%;background:#ff5f5f'>";
    // html += "<img style='width:45px; height: 45px; position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);' src='https://img.gotit.vn/close-white.png'/>";
    // html += "</span>";
    // html += "</p>";
    // html += "    <h2>Payment Failed</h2>";

    // html += "</div>";
    // html +='<script>\n' + '    // addEventListener support for IE8\n' + '    function bindEvent(element, eventName, eventHandler) {\n' + '        if (element.addEventListener) {\n' + '            element.addEventListener(eventName, eventHandler, false);\n' + '        } else if (element.attachEvent) {\n' + '            element.attachEvent(\'on\' + eventName, eventHandler);\n' + '        }\n' + '    }\n' + '    // Send a message to the parent\n' + '    var sendDataBackToParent = function () {\n' + '        window.parent.postMessage(' + JSON.stringify(dataSendToParent) + ', \'*\');\n' + '    };\n' + '\n' + '    sendDataBackToParent();\n' + '\n' + '</script></body></html>';
    // res.send(html);
    res.render('momoresult', { title: 'Payment Failed', message: 'Payment Failed!', imageUrl: 'https://img.gotit.vn/close-white.png' , dataResponse: JSON.stringify(dataSendToParent), result: true });
  }
});

app.get('/brand/getAll', function(req, res) {
    console.log('Request to get brand list');

    var brandRef = realTimeDB.ref('/brands');

    brandRef.once('value').then(function(snapshot) {
    var brandSnapshotValue = snapshot.val();
    var lastUpdated = null;
    if(brandSnapshotValue) {
        lastUpdated = brandSnapshotValue.last_updated;
    }

    var now = moment();

    if(!lastUpdated || moment(lastUpdated, 'YYYY-MM-DD HH:mm:ss').add(1,'w').isBefore(now)) {
    var endpoint = '/api/brand/getAll';

    performRequest(functions.config().biz.url, endpoint, 'GET', {

    }, function(data) {
          brandRef.set({
            'data': data,
            'last_updated': now.format('YYYY-MM-DD HH:mm:ss')
          });
          console.log('fetch brand list from Biz api');
          res.header("Access-Control-Allow-Origin", functions.config().cors.origin);
          res.send(data);
        });
        }else {
        console.log('fetch brand list from realtime db');
        res.header("Access-Control-Allow-Origin", functions.config().cors.origin);
        res.send(brandSnapshotValue.data);

        }
    });

});

app.get('/store/search', function(req, res) {
    console.log('Request to get store');
    var endpoint = '/api/brand/getStores';

    performRequest(functions.config().biz.url, endpoint, 'GET', {
        'brand_id': req.query.brand_id,
        'city_id': req.query.city_id,
        'district_id': req.query.district_id
    }, function(data) {
        res.header("Access-Control-Allow-Origin", functions.config().cors.origin);
        res.send(data);
    });
});

var language = "vi";

app.get('/product/detail', function(req, res) {
    console.log('Request to get product detail');
    language = (req.query.language==null || req.query.language=="")?"vi":req.query.language;
    var productDetail = realTimeDB.ref('/products/'+req.query.product_id+"/detail_"+language+"/");

    productDetail.once('value').then(function(snapshot) {
        var productSnapshotValue = snapshot.val();
        var lastUpdated = null;
        if(productSnapshotValue) {
            lastUpdated = productSnapshotValue.last_updated;
        }

        var now = moment();

        if(!lastUpdated || moment(lastUpdated, 'YYYY-MM-DD HH:mm:ss').add(1,'w').isBefore(now)) {
            console.log('Getting detail of Product ID from Biz API: ' + req.query.product_id);
            var endpoint = '/api/product/detail';

            performRequest(functions.config().biz.url, endpoint, 'POST', {
                'productId': req.query.product_id
            }, function(data) {
                productDetail.set({
                    'data': data,
                    'last_updated': now.format('YYYY-MM-DD HH:mm:ss')
                });
                console.log('Fetch product detail from Biz api');
                res.header("Access-Control-Allow-Origin", functions.config().cors.origin);
                res.send(data);
            });

        }else {
            console.log('Fetch product detail from realtime db');
            res.header("Access-Control-Allow-Origin", functions.config().cors.origin);
            res.send(productSnapshotValue.data);

        }
    });

});

checkout.get('/creditcard/confirm', function(req, res) {
    // return HTML -> SIGN & POST->Cybersource;
    console.log(req);
    var data = req.query;

    // var html = '';
    // html += "<body style='font-family:Gotham Rounded,Roboto,Helvetica,Arial,sans-serif; overflow-x:hidden'>";
    // html += "<div class='loadingPayment' style='width: 100%;text-align: center;position: absolute;top: 50%;-webkit-transform: translateY(-50%);transform: translateY(-50%);'>"
    // html += "<p class='img' style='font-family: Gotham Rounded;font-size: 16px;font-weight: 500;font-style: normal;letter-spacing: normal;color: #636363;text-align: center;'>";
    // html += "<span style='width: 75px;height: 75px;display: inline-block;''>";
    // html += "<img style='width:75px; height: 75px' src='https://img.gotit.vn/spin.gif'/>";
    // html += "</span>";
    // html += "</p>";
    // html += " <h2 class='iframe_title'>Payment Processing</h2>";
    // html += "<form style='display:none;' action='https://testsecureacceptance.cybersource.com/silent/pay' id='confirmForm'  method='post'>";


    // for (var key in data) {
    //      console.log(key + ': ' + data[key]);
    //     html += "<span>" + key + ":</span><input type= 'text' name='" + key + "' value='" + data[key] + "' style='width: 200px' /><br>";
    // }

    // html += "<input type= 'submit' name='Submit'/>";
    // html += "</form>";
    // html += "<script type='application/javascript'>document.getElementById('confirmForm').submit()</script>";
    // html += "</body>";
    // res.header("Access-Control-Allow-Origin", functions.config().cors.origin);
    // res.send(html);
    var fomrData = '';

    for (var key in data) {
        fomrData += "<span>" + key + ":</span><input type= 'text' name='" + key + "' value='" + data[key] + "' style='width: 200px' /><br>";
    }

    res.render('waiting', { title: 'test', message: 'Payment Processing!', imageUrl: 'https://img.gotit.vn/spin.gif' , dataResponse: fomrData, cybersource_url:functions.config().cybersource.post_url });
});

checkout.post('/creditcard/receipt', function(req, res) {
    // return HTML -> SIGN & POST->Cybersource;
    var data = req.body;

    var reasonCode = data.reason_code;

    console.log(data);

    var result2 = {
      success: false
    };

    var referenceId = data.req_reference_number;

    var paymentId = referenceId.split('_')[0];
    var uid = referenceId.split('_')[1];

    // console.log(paymentId + '_' + uid);
    var paymentRef = db.collection('user').doc(uid).collection('sent').doc(paymentId);

    if(reasonCode == '100') { // PAYMENT SUCCESS
      paymentRef.get().then(function(doc) {
        if (doc.exists) {
          var payment = doc.data();

          var priceId = payment.priceID;
          var productId = payment.productID;
          var quantity = payment.quantity;
          var now = moment();
          var refId = functions.config().biz.campaign_name + now.format('MMMYYYY');
          var expireDate = now.add(3, 'months').format('YYYY-MM-DD');

          var sendType = payment.sendType;

          if (quantity <= 1) {
            var params = {
              "productId": productId,
              "productPriceId": priceId,
              "refId": refId,
              "quantity": quantity,
              "campaignNm": refId,
              "expiryDate": expireDate,
              // 3 tháng kể từ ngày đặt
              "voucherRefId": paymentId
            };

            var endpoint = '/api/transaction';
          } else {
            var params = {
              "productList": [{
                "productId": productId,
                "productPriceId": priceId,
                "quantity": quantity,
              }],
              "refId": refId,
              "campaignNm": refId,
              "expiryDate": expireDate,
              // 3 tháng kể từ ngày đặt
              "voucherRefId": paymentId
            };

            var endpoint = '/api/transactiongroup';
          }

          performRequest(functions.config().biz.url, endpoint, 'POST', params, function(result) {
            if (result) {
                if (quantity <= 1) var voucher = result[0].vouchers[0];
                else var voucher = result[0].groupVouchers;

                var batch = db.batch();

                batch.update(paymentRef, {
                    legacy: false,
                    transaction: data,
                    voucherLink: voucher.voucherLink,
                    voucherLinkCode: voucher.voucherLinkCode,
                    step: 2,
                    state:5
                });

                batch.commit().then(function() {
                    //if(sendType === 'sms' || sendType === 'email') {
                    //  send_sms_or_email(uid, paymentId , function(){});
                    //}

                    result2.success = true;

                    var html = '<html>';
                    html += "<head>\n" + "    <title>Secure Acceptance - API Payment Form Example</title>\n" + "<script\n" + "  src='https://code.jquery.com/jquery-1.12.4.min.js'\n" + "  integrity='sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ='\n" + "  crossorigin='anonymous'></script>\n" + "</head>";
                    html += '<body>';
                    html += "<body style='font-family:Gotham Rounded,Roboto,Helvetica,Arial,sans-serif; overflow-x:hidden'>";
                    html += "<div class='loadingPayment' style='width: 100%;text-align: center;position: absolute;top: 50%;-webkit-transform: translateY(-50%);transform: translateY(-50%);'>"
                    html += "<p class='img' style='font-family: Gotham Rounded;font-size: 16px;font-weight: 500;font-style: normal;letter-spacing: normal;color: #636363;text-align: center;'>";
                    html += "<span style='width: 75px;height: 75px;display: inline-block;''>";
                    html += "<img style='width:75px; height: 75px' src='https://img.gotit.vn/spin.gif'/>";
                    html += "</span>";
                    html += "</p>";
                    html += " <h2 class='iframe_title'>Payment Processing</h2>";
                    html += '<script>\n' + '    // addEventListener support for IE8\n' + '    function bindEvent(element, eventName, eventHandler) {\n' + '        if (element.addEventListener) {\n' + '            element.addEventListener(eventName, eventHandler, false);\n' + '        } else if (element.attachEvent) {\n' + '            element.attachEvent(\'on\' + eventName, eventHandler);\n' + '        }\n' + '    }\n' + '    // Send a message to the parent\n' + '    var sendDataBackToParent = function () {\n' + '        var receiptData = $("#receipt").serializeArray();\n' + '        window.parent.postMessage(' + JSON.stringify(result2) + ', \'*\');\n' + '    };\n' + '\n' + '    sendDataBackToParent();\n' + '\n' + '</script></body></html>';
                    res.send(html);

                    // var html = '<html>';
                    // html += "<head>\n" + "    <title>Secure Acceptance - API Payment Form Example</title>\n" + "<script\n" + "  src='https://code.jquery.com/jquery-1.12.4.min.js'\n" + "  integrity='sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ='\n" + "  crossorigin='anonymous'></script>\n" + "</head>";
                    // html += '<body>';
                    // html += '<script>\n' + '    // addEventListener support for IE8\n' + '    function bindEvent(element, eventName, eventHandler) {\n' + '        if (element.addEventListener) {\n' + '            element.addEventListener(eventName, eventHandler, false);\n' + '        } else if (element.attachEvent) {\n' + '            element.attachEvent(\'on\' + eventName, eventHandler);\n' + '        }\n' + '    }\n' + '    // Send a message to the parent\n' + '    var sendDataBackToParent = function () {\n' + '        var receiptData = $("#receipt").serializeArray();\n' + '        window.parent.postMessage(' + JSON.stringify(result2) + ', \'*\');\n' + '    };\n' + '\n' + '    sendDataBackToParent();\n' + '\n' + '</script></body></html>';
                    // res.send(html);
                    res.render('result', { title: 'Payment Success', message: 'Payment Success!', imageUrl: '../images/close-white.png' , dataResponse: JSON.stringify(result2), result: true });
                });


            }

          });

        }else {

          // var html = '<html>';
          // html += "<head>\n" + "    <title>Secure Acceptance - API Payment Form Example</title>\n" + "<script\n" + "  src='https://code.jquery.com/jquery-1.12.4.min.js'\n" + "  integrity='sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ='\n" + "  crossorigin='anonymous'></script>\n" + "</head>";

          // html += '<body><script>\n' + '    // addEventListener support for IE8\n' + '    function bindEvent(element, eventName, eventHandler) {\n' + '        if (element.addEventListener) {\n' + '            element.addEventListener(eventName, eventHandler, false);\n' + '        } else if (element.attachEvent) {\n' + '            element.attachEvent(\'on\' + eventName, eventHandler);\n' + '        }\n' + '    }\n' + '    // Send a message to the parent\n' + '    var sendDataBackToParent = function () {\n' + '        var receiptData = $("#receipt").serializeArray();\n' + '        window.parent.postMessage(' + JSON.stringify(result2) + ', \'*\');\n' + '    };\n' + '\n' + '    sendDataBackToParent();\n' + '\n' + '</script></body></html>';
          // res.send(html);
          res.render('result', { title: 'Payment Failed', message: 'Payment Failed!', imageUrl: '../images/close-white.png' , dataResponse: JSON.stringify(result2), result: false });
        }

      });
    }else {

      paymentRef.get().then(function(doc) {
        if (doc.exists) {
          var batch = db.batch();

          batch.update(paymentRef, {
            transaction: data
          });

          batch.commit().then(function() {

          });
        }
      });

      var html = '<html>';
      html += "<head>\n" + "    <title>Secure Acceptance - API Payment Form Example</title>\n" + "<script\n" + "  src='https://code.jquery.com/jquery-1.12.4.min.js'\n" + "  integrity='sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ='\n" + "  crossorigin='anonymous'></script>\n" + "</head>";
      html += "<body style='font-family:Gotham Rounded,Roboto,Helvetica,Arial,sans-serif; overflow-x:hidden'>";
      html += "<div class='loadingPayment' style='width: 100%;text-align: center;position: absolute;top: 50%;-webkit-transform: translateY(-50%);transform: translateY(-50%);'>"
      html += "<p class='img' style='font-family: Gotham Rounded;font-size: 16px;font-weight: 500;font-style: normal;letter-spacing: normal;color: #636363;text-align: center;'>";
      html += "<span style='width: 75px;height: 75px;position:relative;display: inline-block;border-radius:50%;background:#ff5f5f'>";
      html += "<img style='width:45px; height: 45px; position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);' src='https://img.gotit.vn/close-white.png'/>";
      html += "</span>";
      html += "</p>";
      html += "    <h2>Payment Failed</h2>";

    html += "</div>";
      html +='<script>\n' + '    // addEventListener support for IE8\n' + '    function bindEvent(element, eventName, eventHandler) {\n' + '        if (element.addEventListener) {\n' + '            element.addEventListener(eventName, eventHandler, false);\n' + '        } else if (element.attachEvent) {\n' + '            element.attachEvent(\'on\' + eventName, eventHandler);\n' + '        }\n' + '    }\n' + '    // Send a message to the parent\n' + '    var sendDataBackToParent = function () {\n' + '        var receiptData = $("#receipt").serializeArray();\n' + '        window.parent.postMessage(' + JSON.stringify(result2) + ', \'*\');\n' + '    };\n' + '\n' + '    sendDataBackToParent();\n' + '\n' + '</script></body></html>';
      res.send(html);

    //  var html = '<html>';
    //  html += "<head>\n" + "    <title>Secure Acceptance - API Payment Form Example</title>\n" + "<script\n" + "  src='https://code.jquery.com/jquery-1.12.4.min.js'\n" + "  integrity='sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ='\n" + "  crossorigin='anonymous'></script>\n" + "</head>";
    //   html += "<body style='font-family:Gotham Rounded,Roboto,Helvetica,Arial,sans-serif; overflow-x:hidden'>";
    //   html += "<div class='loadingPayment' style='width: 100%;text-align: center;position: absolute;top: 50%;-webkit-transform: translateY(-50%);transform: translateY(-50%);'>"
    //   html += "<p class='img' style='font-family: Gotham Rounded;font-size: 16px;font-weight: 500;font-style: normal;letter-spacing: normal;color: #636363;text-align: center;'>";
    //   html += "<span style='width: 75px;height: 75px;position:relative;display: inline-block;border-radius:50%;background:#ff5f5f'>";
    //   html += "<img style='width:45px; height: 45px; position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);' src='https://doc-0c-4s-docs.googleusercontent.com/docs/securesc/1jjuejr7jegm7n5o1svevva9vniu91o9/0ottmr9lbucmfnt28i8l4qq45kfsga8j/1517299200000/03857214533101298923/03857214533101298923/1ywI8p-8gmc0q9peGRMgUYTDyI9S1MfIa?e=view'/>";
    //   html += "</span>";
    //   html += "</p>";
    //   html += "    <h2>Payment Failed</h2>";

    // html += "</div>";
    //  html +='<script>\n' + '    // addEventListener support for IE8\n' + '    function bindEvent(element, eventName, eventHandler) {\n' + '        if (element.addEventListener) {\n' + '            element.addEventListener(eventName, eventHandler, false);\n' + '        } else if (element.attachEvent) {\n' + '            element.attachEvent(\'on\' + eventName, eventHandler);\n' + '        }\n' + '    }\n' + '    // Send a message to the parent\n' + '    var sendDataBackToParent = function () {\n' + '        var receiptData = $("#receipt").serializeArray();\n' + '        window.parent.postMessage(' + JSON.stringify(result2) + ', \'*\');\n' + '    };\n' + '\n' + '    sendDataBackToParent();\n' + '\n' + '</script></body></html>';
    //  res.send(html);

    /*render failed html*/
      res.render('result', { title: 'Payment Failed', message: 'Payment Failed!', imageUrl: '../images/close-white.png' , dataResponse: JSON.stringify(result2), result: false });
    }

    //IF PAYMENT SUCCESS -> GENERATE VOUCHER -> RETURN CLOSE IFRAME -> GO TO STEP 3
    //IF PAYMENT FAIL -> RETURN FAIL PAGE AND SHOW ON IFRAME (WITH CLOSE BUTTON)

});

// checkout.get('/creditcard/result', function (req, res){
//   // return Cybersource->Redirect->Result with Data Close IFrame -> Listen to FireStore Data Change->(Success) Go to Step 3->(Fail) Return to Step 2;
// });
//
// checkout.get('/creditcard/webhook', function (req, res){
//   // return Cybersource->(this)->Change firestore info+call biz API
// });
checkout.post('/creditcard/sign', function(req, res) {

    var paymentId = req.body.paymentId;
    var uid = req.body.uid;

    var cardCvn = req.body.cardCvn;
    var cardNumber = req.body.cardNumber;
    var cardExpire = req.body.cardExpire;

    var firstName = req.body.firstName;
    var lastName = req.body.lastName;
    var email = req.body.email;
    if (!email) {
        email = '';
    }

    var visaCards = creditCardType(cardNumber);
    console.log(visaCards);
    var cardTypeName = (typeof visaCards[0] !== 'undefined' && visaCards[0] !== null)?visaCards[0].type:'';
    var cardType = '';

    if(cardTypeName === 'visa') {
      cardType = '001';
    }
    if(cardTypeName === 'master-card') {
      cardType = '002';
    }
    if(cardTypeName === 'american-express') {
      cardType = '003';
    }
    if(cardTypeName === 'discover') {
      cardType = '004';
    }
    if(cardTypeName === 'jcb') {
      cardType = '007';
    }
    console.log('Card type name: ' + cardTypeName);

    if (paymentId && uid) {

        var paymentRef = db.collection('user').doc(uid).collection('sent').doc(paymentId);



        var getDoc = paymentRef.get().then(function(doc) {

            res.header("Access-Control-Allow-Origin", functions.config().cors.origin);

            if (!doc.exists) {



                res.json({
                    success: false,
                    error: 'Payment ' + paymentId + ' not found'
                });

            } else {
                var paymentDocument = doc.data();

                var totalAmount = paymentDocument.totalAmount;

                var profile_id = functions.config().cybersource.profile_id;
                var access_key = functions.config().cybersource.access_key;
                var secret_key = functions.config().cybersource.secret_key;

                var signed_field_name = 'access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency';
                var unsigned_field_names = 'payment_method,bill_to_forename,bill_to_surname,bill_to_email,bill_to_address_line1,bill_to_address_city,bill_to_address_postal_code,bill_to_address_country,card_type,card_cvn,card_number,card_expiry_date';

                var data = {
                    transaction_uuid: uuidv5('gotit.vn' + moment().format('YYYY-MM-DD hh:mm:ss'), uuidv5.URL),
                    signed_field_names: signed_field_name,
                    signed_date_time: getUTCDate(),
                    //"2017-02-28T14:38:33Z",
                    locale: "en",
                    payment_method: "card",
                    bill_to_forename: lastName,
                    bill_to_surname: firstName,
                    bill_to_email: email,
                    bill_to_address_line1: "Ho Chi Minh",
                    bill_to_address_city: "Ho Chi Minh",
                    bill_to_address_postal_code: "94107",
                    bill_to_address_country: "VN",
                    transaction_type: "sale",
                    reference_number: paymentId + '_' + uid,
                    amount: totalAmount,
                    currency: "vnd",
                    card_type: cardType,
                    card_cvn: cardCvn,
                    card_number: cardNumber,
                    card_expiry_date: cardExpire,
                    access_key: access_key,
                    profile_id: profile_id,
                    unsigned_field_names: unsigned_field_names,
                    //device_fingerprint_id: 'gotitvn' + fingerprint,
                    signature: ""
                };

                var signedFieldNames = signed_field_name.split(",");

                var dataToSign = [];
                signedFieldNames.forEach(function(item) {
                    dataToSign.push(item + "=" + data[item]);
                });
                dataToSign = dataToSign.join(",");

                data.signature = crypto.createHmac('sha256', secret_key).update(dataToSign).digest('base64');

                console.log(data);

                res.json(data);
            }
        });
    }
});

send.post('/email_or_sms', function(req, res) {
    console.log('Request send SMS.' + req.body);

    var reqParams = req.body;
    var paymentId = reqParams.id;
    var uId = reqParams.uid;

    send_sms_or_email(uId, paymentId, function(result){
        res.header("Access-Control-Allow-Origin", functions.config().cors.origin);
        res.status(200).send(result);
    })
});

function send_sms_or_email(uId, paymentId, callback) {
    var transaction = db.collection('user').doc(uId).collection('sent').doc(paymentId).get()
        .then(function (doc) {
            var result = "";
            var detail = doc.data();
            var paymentStatus = detail.step;
            var voucherLinkCode = detail.voucherLinkCode;
            var receiverPhone = detail.receiverPhone;
            var receiverEmail = detail.receiverEmail;
            var receiverName = detail.receiverName;
            var senderName = detail.senderName;
            var senderType = detail.sendType;
            var params = {
                "voucherLinkCode": voucherLinkCode,
                "receiverNm": receiverName,
                "senderNm": senderName
            };

            if (paymentStatus > 1 && voucherLinkCode != "") {
                if (receiverPhone != "" && senderType == "sms") {
                    var endpoint = '/api/send/sms';

                    params["phoneNo"] = receiverPhone;
                }
                else if (receiverEmail != "" && senderType == "email") {
                    var endpoint = '/api/send/email';

                    params["email"] = receiverEmail;
                }

                var headers = {
                    'Content-Type': 'application/json',
                    'x-gi-authorization': functions.config().biz.key
                };

                var options = {
                    host: functions.config().biz.url,
                    path: endpoint,
                    method: 'POST',
                    headers: headers
                };

                var req = https.request(options, function (result) {
                    result.setEncoding('utf-8');

                    var responseString = '';

                    result.on('data', function (data) {
                        responseString += data;
                    });

                    result.on('end', function () {
                        console.log(responseString);
                        var resObject = JSON.parse(responseString);
                        if(resObject && resObject.stt == 1) {
                            db.collection('user').doc(uId).collection('sent').doc(paymentId).update({
                                step: 3,
                                sendTime: moment().format('YYYY-MM-DD hh:mm:ss')
                            })
                            .then(function (data2) {
                                console.log("Document successfully updated!");
                            })
                            .catch(function (error) {
                                // The document probably doesn't exist.
                                console.error("Error updating document: ", error);
                            });
                        }
                        result = responseString;
                        callback(result);
                    });
                });
                var dataString = JSON.stringify(params);
                req.write(dataString);
                req.end();
            }
            else {
                params["paymentStatus"] = paymentStatus;
                console.log("Not enough data to send!" + JSON.stringify(params));
                result = "Not enough data to send!" + JSON.stringify(params);
                callback(result);
            }


        });

}

send.post('/email', function(req, res) {
    console.log('Request send email.');
    var endpoint = '/api/send/email';
    var params = req.body;
    admin.database().ref('/emailReq').push(params).then(snapshot => {});
    performRequest(functions.config().biz.url, endpoint, 'POST', params, function(data) {
        admin.database().ref('/emailRes').push(data).then(snapshot => {})
        res.status(200).send(data);
    });
});
send.post('/fb', function(req, res) {
    console.log('Request send Facebook.');
    var endpoint = '/api/send/fb';
    var params = req.body;
    admin.database().ref('/fbReq').push(params).then(snapshot => {});
    performRequest(functions.config().biz.url, endpoint, 'POST', params, function(data) {
        admin.database().ref('/fbRes').push(data).then(snapshot => {})
        res.status(200).send(data);
    });
});

/*UNIVERSAL FUNCTIONS */

function performRequest(host, endpoint, method, data, success) {
    var dataString = JSON.stringify(data);

    var headers = {
        'Content-Type': 'application/json',
        'x-gi-authorization': functions.config().biz.key,
        'Accept-Language': language
    };

    if (method == 'GET') {
        endpoint += '?' + querystring.stringify(data);
    } else {
        headers['Content-Length'] = dataString.length;
    }

    var options = {
        host: host,
        path: endpoint,
        method: method,
        headers: headers
    };

    var req = https.request(options, function(res) {
        res.setEncoding('utf-8');

        var responseString = '';

        res.on('data', function(data) {
            responseString += data;
        });

        res.on('end', function() {
            //console.log(responseString);
            var responseObject = JSON.parse(responseString);
            success(responseObject);
        });
    });

    req.write(dataString);
    req.end();
}

function getUTCDate() {
    var now = new Date();
    var now_utc = now.getUTCFullYear() + "-" + appendZero(now.getUTCMonth() + 1) + "-" + appendZero(now.getUTCDate()) + "T" + appendZero(now.getUTCHours()) + ":" + appendZero(now.getUTCMinutes()) + ":" + appendZero(now.getUTCSeconds()) + "Z";
    return now_utc;
}

function appendZero(digit) {
    if (digit < 10) return "0" + digit;
    else return digit;
}

app.use(cors);
app.use(cookieParser);
exports.app = functions.https.onRequest(app);

checkout.use(cors);
checkout.use(cookieParser);
exports.checkout = functions.https.onRequest(checkout);

momo.use(cors);
momo.use(cookieParser);
exports.momo = functions.https.onRequest(momo);

send.use(cors);
send.use(cookieParser);
exports.send = functions.https.onRequest(send);

exports.onCreateAccount = functions.auth.user().onCreate(event => {
    const user = event.data; // The Firebase user.
    const email = user.email; // The email of the user.
    const displayName = user.displayName; // The display name of the user.

    var bucket = admin.storage().bucket();

    var fileName = email;
    var tempFilePath = path.join(os.tmpdir(), fileName);

    return bucket.file('legacy_users/'+fileName).download({
        destination: tempFilePath
    }).then(function() {

        var userData = JSON.parse(fs.readFileSync(tempFilePath, 'utf8'));
        //console.log()
        return userData;


    }).then(function(userData) {

        var updateUserPromise = admin.auth().updateUser(user.uid, {
            phoneNumber: '+84' + userData.customer.phone.replace(/^0/g, ''),
            emailVerified: true
        }).then(function(userRecord) {
            console.log("Successfully updated user", userRecord.toJSON());
        }).
        catch(function(error) {
            console.log("Error updating user:", error);
        });

        var updateFirestoreInfoPromise = admin.firestore().collection("user").doc(user.uid).set(userData.customer).then(function() {
            console.log('Customer Data Migrated');
        }).
        catch(function(error) {
            console.log('Error in Customer Data Migration');
        });

        var voucherLog = userData.voucher_log;

        var batch = admin.firestore().batch();

        for (var k in voucherLog) {
            voucherLog[k].legacy = true;
            voucherLog[k].quantity = 1;
            voucherLog[k].step = 3;
            batch.set(admin.firestore().collection("user").doc(user.uid).collection("sent").doc(voucherLog[k].invoiceNo), voucherLog[k]);

        }

        var insertFirestoreSentPromise = batch.commit().then(function() {
            console.log('Log Data' + voucherLog[k].invoiceNo + ' Data Migrated');
        }).
        catch(function(error) {
            console.log('Log Data' + voucherLog[k].invoiceNo + ' Data Migration Error');
        });


        return Promise.all([updateUserPromise, updateFirestoreInfoPromise, insertFirestoreSentPromise]).then(console.log('Everything Done!'));


    }).
    catch((err) => {
        console.error('Failed to download file.', err);
    });

});
