import  'zone.js/dist/zone-node';
import  'reflect-metadata';
import { enableProdMode } from  '@angular/core';
import  *  as  express  from  'express';
import { join } from  'path';
import { renderModuleFactory } from '@angular/platform-server';
import * as fs from 'fs';
const functions = require('firebase-functions');

// NOTE: leave this as require() since this file is built Dynamically from webpack
const  DIST_FOLDER  =  join(process.cwd(), 'dist');
// const document = fs.readFileSync(join(DIST_FOLDER, 'index.html'), 'utf8');
const { AppServerModuleNgFactory, LAZY_MODULE_MAP } =  require('../dist-server/main.bundle');
// NgUniversalTools: Express Engine and moduleMap for lazy loading
import { ngExpressEngine } from  '@nguniversal/express-engine';
import { provideModuleMap } from  '@nguniversal/module-map-ngfactory-loader';

(global as any).WebSocket = require('ws');
(global as any).XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();
// Express server
const  app  =  express();
const  PORT  =  process.env.PORT  ||  4200;

// app.get('**',(req, res)=>{
// 	const url = req.path;
// 	renderModuleFactory(AppServerModuleNgFactory,{document , url}).then(html =>{
// 		res.set('Cache-Control', 'public, max-age=600, s-maxage=1200');
// 		res.send(html);
// 	})
// });




// const  DIST_FOLDER  =  join(process.cwd(), 'dist');
app.engine('html', ngExpressEngine({
  bootstrap:  AppServerModuleNgFactory,
  providers: [
      provideModuleMap(LAZY_MODULE_MAP)
  ]
}));
app.set('view engine', 'html');
app.set('views', DIST_FOLDER);

// Server static files from browser
app.get('*.*', express.static(DIST_FOLDER));
// All regular routes use the Universal engine
app.get('*', (req, res) => {
  res.render(join(DIST_FOLDER, 'index.html'), { req });
});


// Start up the Node server
app.listen(PORT, () => {
  console.log(`Node server listening on http://localhost:${PORT}`);
});

exports.ssr = functions.https.onRequest(app);