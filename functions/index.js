const anotherFunc = require('./another-func');
const ssrFunc = require('./ssr-func');

exports.ssr = ssrFunc.ssr;
exports.app = anotherFunc.app;
exports.checkout = anotherFunc.checkout;
exports.momo = anotherFunc.momo;
exports.send = anotherFunc.send;