const ngtools = require('@ngtools/webpack');
const webpack = require('webpack');
const path = require('path');
const ExtractTextWebpackPlugin = require("extract-text-webpack-plugin");
const nodeExternals = require('webpack-node-externals');
module.exports = {
    entry: {
       server : './server/index.ts',
    },
    resolve: {
        extensions: ['.ts', '.js']
    },
    target: 'node',
    // externals: [/node_modules/],
    externals: [nodeExternals({
         whitelist: [
            /^ngx-bootstrap/,
            /^@ngx-translate\/core/,
            /^@agm\/core/,
            /^@agm\/snazzy-info-window/,
            /^ng2-select/,
            /^ngx-facebook/
         ]
       })],
    node: {
        __dirname: true
    },
    output: {
        path: path.join(__dirname, "dist"),
        filename: 'index.js'
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                loaders: ['awesome-typescript-loader', 'angular2-template-loader'],
                exclude: [/node_modules\/(?!(ng2-.+|ngx-.+|@ngx-.+))/]
            },
            {
                test: /\.(scss|html|png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                use: 'raw-loader'
            },
            {
                test: /\.(png|jpg|woff|woff2|eot|ttf|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url?limit=512&&name=[path][name].[ext]?[hash]'
            },
            { test: /\.scss$/, 
                use: [{
                    loader: "style-loader" // creates style nodes from JS strings
                }, 
                {
                    loader: "css-loader" // translates CSS into CommonJS
                }, 
                {
                    loader: "sass-loader" // compiles Sass to CSS
                }] 
            },

            { test: /\.(ts|js)$/, loader: 'regexp-replace-loader', query: { match: { pattern: '\\[(Mouse|Keyboard|Focus)Event\\]', flags: 'g' }, replaceWith: '[]', } },
            { test: /\.ts$/, exclude: /\.ts$/, loader: 'ts-loader' }
        ]
    },
    plugins: [
        new webpack.ContextReplacementPlugin(
          // fixes WARNING Critical dependency: the request of a dependency is an expression
          /(.+)?angular(\\|\/)core(.+)?/,
          path.join(__dirname, 'src'), // location of your src
          {} // a map of your routes
        ),
        new webpack.ContextReplacementPlugin(
          // fixes WARNING Critical dependency: the request of a dependency is an expression
          /(.+)?express(\\|\/)(.+)?/,
          path.join(__dirname, 'src'),
          {}
        )
    ]
}