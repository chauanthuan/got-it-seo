import {Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef, Inject, PLATFORM_ID} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import {MenuService} from '../menu/menu.service';
import {CheckoutService} from './checkout.service';
import { ProductService } from "../product/product.service";
import {Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { AuthService } from '../services/auth.service';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import {dateFormatPipe} from '../common/formatDate.pipe';
import { DatePipe } from '@angular/common';
import { Validators, FormGroup, FormArray, FormBuilder, FormControl} from '@angular/forms';
import {ModalDirective} from "ngx-bootstrap/modal";
import { SlicePipe } from '@angular/common';
import { FacebookService, LoginResponse, LoginOptions, UIResponse, UIParams, FBVideoComponent } from 'ngx-facebook';
import * as moment from 'moment';
import { environment } from '../../environments/environment';
import {isNullOrUndefined} from "util";
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import { DomSanitizer } from '@angular/platform-browser';
import {TranslateService,LangChangeEvent} from '@ngx-translate/core';
import { Title, Meta }     from '@angular/platform-browser';

declare var require: any;
declare var $: any;
@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
  host: {
    '(document:click)': 'onDocumentClick($event)',
  },
})
export class CheckoutComponent implements OnInit {

  @ViewChild('iframe') iframe: ElementRef;
  @ViewChild('staticModal') public staticModal: ModalDirective;
  @ViewChild('momoLink') momoLink: ElementRef;
  @ViewChild('notificationModal') public notificationModal: ModalDirective;
  @ViewChild('staticSMSEmailModal') public staticSMSEmailModal: ModalDirective;
  @ViewChild('scrollTo') scrollTo: ElementRef;
  channelDropdownClicked = false;
  paymentDropdownClicked = false;
  isClicked = false;
  showOrder = false;
  momoPaymentWindow: any;

  customer_name_sms: string;
  customer_name_msg: string;
  customer_name_fb: string;
  customer_name_viber: string;
  customer_name_whatsapp: string;
  customer_name_em: string;

  customer_email_em: string;
  customer_email_msg: string;
  customer_email_fb: string;
  customer_email_viber: string;
  customer_email_whatsapp: string;

  receiver_name_sms: string;
  receiver_name_em: string;
  receiver_name_msg: string;
  receiver_name_fb: string;
  receiver_name_whatsapp: string;
  receiver_name_viber: string;

  receiver_phone: string;
  receiver_email_em: string;

  message_sms: string;
  message_em: string;
  message_msg: string;
  message_fb: string;
  message_viber: string;
  message_whatsapp: string;

  name_card: string;
  card_number: string;
  expired_date: string;
  security_code: string;

  atm_bank: string = 'SML';
  momo_payment_url: string;

  step1Finish = false;
  step2Finish = false;
  step3Finish = false;
  selectedChannelSend = 'sms';
  selectedChannelPayment = 'credit';
  selectedChannelPaymentText = 'Credit Card';

  signatureData: any;
  formLoaded = false;
  cybersourceFormAction: string;

  cybersourceForm: FormGroup;

  db: any;
  /*productInfo get from product page when user click send gift*/
  productID: any;
  priceID: any;
  quantity: number;
  priceValue: number;
  productNmEn: any;
  productNmVi: any;
  brandNm: any;
  productImage: any;
  userData: any;
  paymentSession:any;
  isMobile: boolean = false;
  transactionFailed: boolean = true;
  processing: boolean = false;
  maskCvn: any;
  maskExpiredDate: any;
  
  voucherObjectData: any;
  isSentSmsEmail: boolean = false;
  isSendingSmsEmail: boolean = false;
  isLoading: boolean = false;
  noticeMessage: string;
  listBank = [
    {
      'code':'VCB',
      'name':'Vietcombank',
      'img':'vietcombank.png'
    },
    {
      'code':'TCB',
      'name':'Techcombank',
      'img':'techcombank.png'
    },
    {
      'code':'VIB',
      'name':'VIB  Bank',
      'img':'vibbank.png'
    },
    {
      'code':'ABB',
      'name':'ABBank',
      'img':'abbank.png'
    },
    {
      'code':'STB',
      'name':'Sacombank',
      'img':'sacombank.png'
    },
    {
      'code':'MSB',
      'name':'Maritime  Bank',
      'img':'maritimebank.png'
    },
    {
      'code':'NVB',
      'name':'Navibank',
      'img':'navibank.png'
    },
    {
      'code':'CTG',
      'name':'Vietinbank',
      'img':'viettinbank.png'
    },
    {
      'code':'DAB',
      'name':'DongABank',
      'img':'dongabank.png'
    },
    {
      'code':'HDB',
      'name':'HDBank',
      'img':'hdbank.png'
    },
    {
      'code':'VAB',
      'name':'VietABank',
      'img':'vietabank.png'
    },
    {
      'code':'VPB',
      'name':'VPBank',
      'img':'vpbank.png'
    },
    {
      'code':'ACB',
      'name':'ACB',
      'img':'acb.png'
    },
    {
      'code':'MB',
      'name':'MBBank',
      'img':'mbbank.png'
    },
    {
      'code':'GPB',
      'name':'GPBank',
      'img':'gpbank.png'
    },
    {
      'code':'EIB',
      'name':'Eximbank',
      'img':'eximbank.png'
    },
    {
      'code':'OJB',
      'name':'OceanBank',
      'img':'oceanbank.png'
    },
    {
      'code':'NASB',
      'name':'BacABank',
      'img':'bacabank.png'
    },
    {
      'code':'OCB',
      'name':'OricomBank',
      'img':'ocb.png'
    },
    {
      'code':'TPB',
      'name':'TPBank',
      'img':'tpbank.png'
    },
    {
      'code':'LPB',
      'name':'LienVietPostBank',
      'img':'lienvietpostbank.png'
    },
    {
      'code':'BIDV',
      'name':'BIDV',
      'img':'bidv.png'
    },
    {
      'code':'VARB',
      'name':'AgriBank',
      'img':'agribank.png'
    },
    {
      'code':'BVB',
      'name':'BaoVietBank',
      'img':'baovietbank.png'
    },
    {
      'code':'SHB',
      'name':'SHB',
      'img':'shb.png'
    },
    {
      'code':'KLB',
      'name':'KienLongBank',
      'img':'kienlongbank.png'
    },
    {
      'code':'SCB',
      'name':'SCB',
      'img':'scb.png'
    },
    {
      'code':'SEAB',
      'name':'Seabank',
      'img':'seabank.png'
    },
  ]

  clickPayment: boolean = false;

  constructor(public _menuService: MenuService,
    private checkoutService: CheckoutService,
    private _fb: FormBuilder ,
    private _productService: ProductService,
    private router: Router,
    private afAuth: AngularFireAuth,
    private authService: AuthService,
    private cdr: ChangeDetectorRef,
    private dateFormatPipe: dateFormatPipe,
    private datePipe: DatePipe,
    private fb: FacebookService,
    private http: Http,
    private sanitizer:DomSanitizer,
    public translate: TranslateService,
    private titleService: Title,
    private meta: Meta,
    @Inject(PLATFORM_ID) private platformId: Object
    ) {
    this.cybersourceForm =  new FormGroup({});
    this.db = firebase.firestore();
    if (isPlatformBrowser(this.platformId)){
	    fb.init({
	      appId: '468266393371236',
	      version: 'v2.4',
	      cookie     : true,  // enable cookies to allow the server to access
	      xfbml      : true,
	    });
	}
    translate.get('checkout_title').subscribe((res: string) => {
        this.titleService.setTitle(res);
    });
    this.meta.addTags([
      {name: 'description', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
      {name: 'copyright', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
      {name: 'keywords', content: 'quà tặng doanh nghiệp , quà tặng đối tác , voucher quà tặng , quà tặng khách hàng , quà tặng Tết, quà tặng sự kiện , tặng thưởng nhân viên , quà tặng doanh nhân , egift, quà tặng online, tặng quà trực tuyến, tặng quà online , gift voucher , phiếu quà tặng , quà tặng trực tuyến ,thẻ quà tặng , tặng voucher ,tặng quà online , quà online , bán gift card , phiếu tặng quà , giftcards , mua phiếu quà tặng'}
    ]);
  }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)){
	    window.scrollTo(0, 0);
	    var ua = navigator.userAgent;
	    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/i.test(ua)){
	      this.isMobile = true;
	    }
	    else{
	      this.isMobile = false;
	    }
	    this._menuService.hide();
	    
	}

	
 	
 	this.productID = this._productService.productID;
    this.priceID = this._productService.priceID;
    this.quantity = this._productService.quantity;
    this.priceValue = this._productService.priceValue;
    this.productNmEn = this._productService.productNameEn;
    this.productNmVi = this._productService.productNameVi;
    this.productImage = this._productService.imgPath;
    this.brandNm = this._productService.brandName;


    if(!this.productID || !this.priceID || !this.quantity){
     this.router.navigate(['/send-egift']);
    }
    console.log("proID: "+this.productID+" - priceID: "+ this.priceID+ " - value: "+this.priceValue+ "- quantity: "+ this.quantity);
    console.log(this.productNmEn, this.productNmVi)

    if(firebase.auth().currentUser && firebase.auth().currentUser != null){
    	console.log('co auth')
	    this.customer_name_sms = firebase.auth().currentUser.displayName;
	    this.customer_name_msg = firebase.auth().currentUser.displayName;
	    this.customer_name_fb = firebase.auth().currentUser.displayName;
	    this.customer_name_viber = firebase.auth().currentUser.displayName;
	    this.customer_name_whatsapp = firebase.auth().currentUser.displayName;
	    this.customer_name_em = firebase.auth().currentUser.displayName;

	    this.customer_email_em = firebase.auth().currentUser.email;
	    this.customer_email_msg = firebase.auth().currentUser.email;
	    this.customer_email_fb = firebase.auth().currentUser.email;
	    this.customer_email_viber = firebase.auth().currentUser.email;
	    this.customer_email_whatsapp = firebase.auth().currentUser.email;
	}
    // this.atm_bank = this.listBank[0].code;

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.translate.get('checkout_title').subscribe((res: string) => {
        this.titleService.setTitle(res);
      });
    });
    
    this.maskCvn = [/[0-9]/, /[0-9]/, /[0-9]/];
	this.maskExpiredDate = [/[0-1]/, /[0-9]/, '\/', /[0-9]/, /[0-9]/];
  }

  ngAfterViewInit(){
  	if (isPlatformBrowser(this.platformId)){
  		// setTimeout (() => {
		    
		// },500);
	    var w_height = $(window).height();
	    $('#pg_checkout .parent').css({'min-height':(w_height - 60)+'px'});

	    var w_width = $(window).width();
	    var content_width = $('.checkout-section .parent').width();
	    var path_right_width = (w_width - content_width) / 2;
	    $('.right-fixed-wrap').css({'width': path_right_width});

	    const footer = document.getElementsByTagName('footer')[0];
	    footer.classList.add('hide');

	    let wrap_checkout = document.getElementsByClassName('wrapper-content')[0];
	    wrap_checkout.classList.add('no-padding');

	    this.cdr.detectChanges();
    }
  }

  onResize(event){
  	if (isPlatformBrowser(this.platformId)){
	    var w_width = $(window).width();
	    var content_width = $('.product-section .parent').width();
	    if (w_width >= content_width) {
	      var path_right_width = (w_width - content_width) / 2;
	    }
	    else {
	      path_right_width = 0;
	    }
	    $('.right-fixed-wrap').css({'width': path_right_width});
	}
  }

  ngOnDestroy() {
  	if (isPlatformBrowser(this.platformId)){
	    const footer = document.getElementsByTagName('footer')[0];
	    footer.classList.remove('hide');

	    let wrap_checkout = document.getElementsByClassName('wrapper-content')[0];
	    wrap_checkout.classList.remove('no-padding');
	    // let menu = document.getElementsByClassName('navbar')[0];
	    // console.log(menu.classList);
	    // menu.classList.remove("hideMenu");
	}
  }

  handleInput(event) {

  }

  payCreditCard():void {
  	if (isPlatformBrowser(this.platformId)){
	    let control: FormControl = new FormControl("TEST", []);
	    this.cybersourceForm.addControl("TETET", control);

	    this.checkoutService.getSignature(100000).then(data => {

	      for (var key in data) {
	        if (data.hasOwnProperty(key)) {
	          let control: FormControl = new FormControl(data[key], []);
	          this.cybersourceForm.addControl(key, control);

	          console.log(key + ": " + data[key]);
	        }
	      }
	      console.log(this.cybersourceForm);

	      // var cybersourceForm = <HTMLFormElement>document.getElementById('cybersourceForm');
	      // cybersourceForm.submit();
	    });
	}
  }
  choosebank(val){
    this.atm_bank = val;
  }
  openMoMoPaymentUrl (): void {
    if (isPlatformBrowser(this.platformId)){
	    //let momoPaymentWindow = window.open(this.momo_payment_url, "", "width=1000,height=800");
	    this.isLoading = false;
	    
	    window.addEventListener('message', (e) => {
	      console.log(e.data);
	      setTimeout (() => {
	        this.momoPaymentWindow.close();
	        //call send sms or email if choose this method
	        if(e.data.success == true){//payment success
	          this.step2Finish = true;
	          this.db.collection('user').doc(firebase.auth().currentUser.uid).collection('sent').doc(this.paymentSession).get()
	          .then((doc) => {
	          if (doc.exists) {
	            this.voucherObjectData = doc.data();
	            //check send method && not isMobile
	            if(!this.isMobile){
	              if(doc.data().sendType == 'sms' || doc.data().sendType == 'email'){
	                //call function send email sms
	                this.isSendingSmsEmail = true;
	                this.checkoutService.paymentSession =  this.paymentSession;
	                this.checkoutService.uid = firebase.auth().currentUser.uid;
	                return this.checkoutService.sendEmailOrSms().then((res)=>{
	                  let dataRes = JSON.parse(res._body);
	                  if(dataRes.hasOwnProperty("stt")){
	                  this.isSentSmsEmail = true;
	                  if(this.translate.currentLang == "vi"){
	                    this.noticeMessage = "Xin chúc mừng. Món quà của bạn đã được gửi qua "+(doc.data().sendType == 'sms'? 'SMS' : 'Email' )+" thành công.";
	                  }
	                  else{
	                    this.noticeMessage = "Congratulations. Your egift was sent by "+(doc.data().sendType == 'sms'? 'SMS' : 'Email' )+" successfully.";
	                  }
	                }
	                else{
	                  this.isSentSmsEmail = false;
	                  if(this.translate.currentLang == "vi"){
	                    this.noticeMessage = "Quá trình gửi quà bị lỗi. Vui lòng vào trang <a  href='/gift-sent'>Quà đã tặng</a> và nhấn gửi lại.";
	                  }
	                  else{
	                    this.noticeMessage = "Send eGift failed. Please go to <a  href='/gift-sent'>eGift Sent</a> and send again.";
	                  }
	                }
	                this.isSendingSmsEmail = false;
	                this.staticSMSEmailModal.show();
	                }).catch((error)=>{
	                  console.log(error);
	                  this.isSentSmsEmail = false;
	                  this.isSendingSmsEmail = false;
	                });
	              }
	            }

	          }
	          }).catch((error)=>{
	            console.log(error);
	          });

	        }
	        else{ //payment failed
	          this.notificationModal.show();
	        }
	        
	        
	      }, 5000)
	      setTimeout (() => {
	        window.scrollTo(0,document.body.scrollHeight);
	      }, 1000)
	    });
	}
  }

  payAtm(): void {
  	if (isPlatformBrowser(this.platformId)){
	    this.isLoading = true;
	    let currentUser = firebase.auth().currentUser;
	    let params = {
	      uid: currentUser.uid,
	      paymentSession: this.paymentSession,
	      amount: this.priceValue * this.quantity,
	      atmBank: this.atm_bank
	    };

	    this.momoPaymentWindow = window.open("/payment-waiting", "", "width=1000,height=800");

	    this.checkoutService.getMoMoPaymentUrl(params).then(data => {
	      console.log(data);
	      this.momo_payment_url = data.payUrl;
	      this.openMoMoPaymentUrl();
	      this.momoPaymentWindow.location.href = data.payUrl;
	    });
	}
  }

  payCreditCard2(): void {
    if (isPlatformBrowser(this.platformId)){
	    this.transactionFailed = false;
	    this.notificationModal.hide();
	    let currentUser = firebase.auth().currentUser;

	    let paymentId = this.paymentSession;
	    let cardNumber = this.card_number;

	    let expireDateMoment = moment(this.expired_date, 'MM/YY');

	    let expireDate = this.expired_date;
	    let securityCode = this.security_code;

	    let checkoutService = this.checkoutService;

	    let copyIframe = this.iframe;

	    let copyModal = this.staticModal;
	    copyIframe.nativeElement.src = '';
	    copyModal.show();

	    this.db.collection('user').doc(currentUser.uid).get().then((doc) => {
	      if (doc.exists) {
	         var data = doc.data();

	         var fname = data.fname;
	         var lname = data.lname;

	        let params = {
	          paymentId: paymentId,
	          uid: currentUser.uid,
	          cardNumber: cardNumber,
	          expireDate: expireDateMoment.format('MM-YYYY'),
	          securityCode: securityCode,
	          firstName: fname,
	          lastName: lname,
	          email: currentUser.email
	        };
	        console.log(JSON.stringify(params));

	        this.processing = true;
	        checkoutService.getSignature(params).then(data => {
	          console.log(JSON.stringify(data));
	          var queryString = '';
	          for (var key in data) {
	            queryString += encodeURIComponent(key) + '=' + encodeURIComponent(data[key]) + '&';
	          }
	          this.processing = false;
	          copyIframe.nativeElement.src = environment.firebaseFnsUrl + '/checkout/creditcard/confirm?' + queryString;

	          copyModal.show();

	        });

	      }
	    });

	    if(this.clickPayment == false){
	      window.addEventListener('message', (e) => {
	        this.clickPayment = true;
	        console.log(JSON.stringify(e));
	        if (e.data && e.data.success) {

	          console.log("Payment successfully");
	          this.staticModal.hide();
	          this.step2Finish = true;
	          
	          this.db.collection('user').doc(firebase.auth().currentUser.uid).collection('sent').doc(this.paymentSession).get()
	          .then((doc) => {
	          if (doc.exists) {
	            this.voucherObjectData = doc.data();
	            //check send method && not isMobile
	            if(!this.isMobile){
	              if(doc.data().sendType == 'sms' || doc.data().sendType == 'email'){
	                //call function send email sms
	                this.isSendingSmsEmail = true;
	                this.checkoutService.paymentSession =  this.paymentSession;
	                this.checkoutService.uid = firebase.auth().currentUser.uid;
	                return this.checkoutService.sendEmailOrSms().then((res)=>{
	                  let dataRes = JSON.parse(res._body);
	                  if(dataRes.hasOwnProperty("stt")){
	                    this.isSentSmsEmail = true;
	                    if(this.translate.currentLang == "vi"){
	                      this.noticeMessage = "Xin chúc mừng. Món quà của bạn đã được gửi qua "+(doc.data().sendType == 'sms'? 'SMS' : 'Email' )+" thành công.";
	                    }
	                    else{
	                      this.noticeMessage = "Congratulations. Your egift was sent by "+(doc.data().sendType == 'sms'? 'SMS' : 'Email' )+" successfully.";
	                    }
	                  }
	                  else{
	                    this.isSentSmsEmail = false;
	                    if(this.translate.currentLang == "vi"){
	                      this.noticeMessage = "Quá trình gửi quà bị lỗi. Vui lòng vào trang <a  href='/gift-sent'>Quà đã tặng</a> và nhấn gửi lại.";
	                    }
	                    else{
	                      this.noticeMessage = "Send eGift failed. Please go to <a  href='/gift-sent'>eGift Sent</a> and send again.";
	                    }
	                  }
	                  this.isSendingSmsEmail = false;
	                  this.staticSMSEmailModal.show();
	                }).catch((error)=>{
	                  this.isSentSmsEmail = false;
	                  this.isSendingSmsEmail = false;
	                });
	              }
	            }

	          }
	          }).catch((error)=>{
	          console.log(error);
	          })
	          setTimeout (() => {
	            window.scrollTo(0,document.body.scrollHeight);
	          },1000)
	        }else {
	          this.transactionFailed = true;
	        }
	      });
	    }
    }
  }

  generateInvoiceNumber(): string {
  	if (isPlatformBrowser(this.platformId)){
	    var hrtime = require('browser-process-hrtime')
	    var nanoSecond = hrtime();
	    let invoiceNo = this.datePipe.transform(new Date(), 'yyyyMMddHmmss') + new SlicePipe().transform(nanoSecond[1]+'',0,6);
	    return invoiceNo;
	}
  }

  next_process(event): void {
    if (isPlatformBrowser(this.platformId)){
	    if (event.target.classList.contains('next-write-message')) {
	      this.step1Finish = true;
	    }
	    console.log(123);
	    //Save data to fireStore
	    //check method user choose
	    var customInfo:any;
	    var hrtime = require('browser-process-hrtime')
	    var nanoSecond = hrtime();
	    let invoiceNo = this.datePipe.transform(new Date(), 'yyyyMMddHmmss') + new SlicePipe().transform(nanoSecond[1]+'',0,6);

	    let generalInfor = {
	      invoiceNo: this.paymentSession ? this.paymentSession : invoiceNo,
	      productID:this.productID,
	      productNmEn: this.productNmEn,
	      productNmVi: this.productNmVi,
	      brandNm: this.brandNm,
	      productImage: this.productImage,
	      createdAt: this.datePipe.transform(new Date(), 'yyyy-MM-dd H:mm:ss'),
	      priceID:this.priceID,
	      price:this.priceValue,
	      quantity: this.quantity,
	      totalAmount: this.priceValue * this.quantity,
	      paymentStatus: 0,
	      step:1
	    }

	    if (this.selectedChannelSend == 'sms'){
	      let receiver_phone_dt = '';
	      if(this.receiver_phone){
	        if(this.receiver_phone.indexOf('+84') != -1){
	          receiver_phone_dt = this.receiver_phone.replace('+84','0');
	        }
	        else{
	          receiver_phone_dt = this.receiver_phone;
	        }
	      }
	      customInfo = {
	        sendType: this.selectedChannelSend,
	        senderName: this.customer_name_sms ? this.customer_name_sms : '',

	        senderEmail: firebase.auth().currentUser.email ? firebase.auth().currentUser.email : '',

	        receiverName : this.receiver_name_sms ? this.receiver_name_sms : '',
	        receiverPhone: receiver_phone_dt,
	        receiverEmail: '',
	        message : this.message_sms ? this.message_sms : ''
	      };
	    }
	    else if (this.selectedChannelSend == 'email'){
	      customInfo = {
	        sendType:this.selectedChannelSend,
	        senderName: this.customer_name_em ? this.customer_name_em : '',
	        senderEmail: this.customer_email_em ? this.customer_email_em : '',

	        receiverName : this.receiver_name_em ? this.receiver_name_em : '',
	        receiverPhone: '',
	        receiverEmail: this.receiver_email_em ? this.receiver_email_em : '',
	        message : this.message_em ? this.message_em : ''
	      };
	    }
	    else if (this.selectedChannelSend == 'messenger'){
	      customInfo = {
	        sendType: this.selectedChannelSend,
	        senderName: this.customer_name_msg ? this.customer_name_msg : '',
	        senderEmail: this.customer_email_msg ? this.customer_email_msg : '',

	        receiverName : this.receiver_name_msg ? this.receiver_name_msg : '',
	        receiverPhone: '',
	        receiverEmail: '',
	        message : this.message_msg ? this.message_msg : ''
	      }
	    }
	    else if(this.selectedChannelSend == 'facebook'){
	      customInfo = {
	        sendType:this.selectedChannelSend,
	        senderName: this.customer_name_fb ? this.customer_name_fb : '',
	        senderEmail: this.customer_email_fb ? this.customer_email_fb : '',

	        receiverName : this.receiver_name_fb ? this.receiver_name_fb : '',
	        receiverPhone:'',
	        receiverEmail: '',
	        message : this.message_fb ? this.message_fb : ''
	      }
	    }
	    else if(this.selectedChannelSend == 'viber'){
	      customInfo = {
	        sendType:this.selectedChannelSend,
	        senderName: this.customer_name_viber ? this.customer_name_viber : '',
	        senderEmail: this.customer_email_viber ? this.customer_email_viber : '',

	        receiverName : this.receiver_name_viber ? this.receiver_name_viber : '',
	        receiverPhone:'',
	        receiverEmail: '',
	        message : this.message_viber ? this.message_viber : ''
	      }
	    }
	    else if(this.selectedChannelSend == 'whatsapp'){
	      customInfo = {
	        sendType:this.selectedChannelSend,
	        senderName: this.customer_name_whatsapp ? this.customer_name_whatsapp : '',
	        senderEmail: this.customer_email_whatsapp ? this.customer_email_whatsapp : '',

	        receiverName : this.receiver_name_whatsapp ? this.receiver_name_whatsapp : '',
	        receiverPhone:'',
	        receiverEmail: '',
	        message : this.message_whatsapp ? this.message_whatsapp : ''
	      }
	    }

	    let allData = Object.assign(generalInfor,customInfo);
	    console.log(allData);

	    if(this.paymentSession){
	      this.db.collection('user').doc(firebase.auth().currentUser.uid).collection("sent").doc(this.paymentSession).update(allData).then((res)=>{
	        console.log('Update info payment success');
	      }).catch((error)=>{
	        console.log(error);
	      })
	    }
	    else{
	      this.db.collection('user').doc(firebase.auth().currentUser.uid).collection("sent").doc(invoiceNo).set(allData).then((res)=>{
	        this.paymentSession = invoiceNo;
	        console.log(this.paymentSession)
	      }).catch((error)=>{
	        console.log(error);
	      })
	    }






	    // else if (event.target.classList.contains('next-payment')) {
	    //   let productPrice = localStorage.getItem('productPrice');
	    //   let productQuantity = localStorage.getItem('productQuantity') ;
	    //   let amount = parseInt(productPrice, 10) * (parseInt(productQuantity, 10) == 0 ? 1 : parseInt(productQuantity, 10));
	    //   let name = this.name_card.split(' ');
	    //   this.checkoutService.firstName = name[0];
	    //   this.checkoutService.lastName = this.name_card.replace(name[0] + ' ', '');
	    //   this.checkoutService.amount = amount.toString();
	    //
	    //
	    //
	    //
	    //   this.checkoutService.getSignature().then(data => {
	    //     console.log(data);
	    //
	    //     this.signatureData = Object.keys(data).map((k) => {
	    //       return { key: k, value: data[k] };
	    //     });
	    //     this.formLoaded = true;
	    //     this.checkoutService.cybersourceData = data;
	    //     this.checkoutService.cardNumber = this.card_number;
	    //     this.checkoutService.cardType = '';
	    //     this.checkoutService.expiredDate = this.expired_date;
	    //     this.checkoutService.securityCode = this.security_code;
	    //     /*this.checkoutService.checkoutPayment().then(data => {
	    //       console.log(data);
	    //     });*/
	    //   });
	    //   if (this.signatureData !== '') {
	    //     this.step2Finish = true;
	    //   }
	    //   if (this.step2Finish === true) {
	    //     this.step3Finish = true;
	    //   }
	    // }
	}
  }

  changeMessageStep(): void {
    this.step1Finish = false;
    this.step2Finish = false;
    this.step3Finish = false;
    this.selectedChannelPayment = 'credit';
  }

  selectChannelSend(value): void {
    this.selectedChannelSend = value;
  }

  selectChannelPayment(value): void {
    this.selectedChannelPayment = value;
    if (value === 'credit') {
      this.selectedChannelPaymentText = 'Credit Card';
    } else if (value === 'zalopay') {
      this.selectedChannelPaymentText = 'Zalo Pay';
    } else if (value === 'momo') {
      this.selectedChannelPaymentText = 'Mo Mo';
    } else if (value == 'atm') {
      this.selectedChannelPaymentText = 'ATM';
    }
  }

  toggleClick(event): void {
    this.isClicked = !this.isClicked;
    const target = event.currentTarget;
    const idAttr = target.attributes.id.nodeValue;
    if (idAttr === 'tab_all_chanel') {
      this.channelDropdownClicked = !this.channelDropdownClicked;
    }
    if (idAttr === 'tab_all_payment') {
      this.paymentDropdownClicked = !this.paymentDropdownClicked;
    }
  }

  onDocumentClick(event) {
  	if (isPlatformBrowser(this.platformId)){
	    if (!event.target.closest('.wrapper-dropdown')) {
	      this.channelDropdownClicked = false;
	      this.paymentDropdownClicked = false;
	    }
	}
  }

  validatePayment(event) {
    console.log(event);
  }

  showOrderInfo(): void {
    this.showOrder = !this.showOrder;
  }

  private detectCardType(number) {
    var re = {
      electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
      maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
      dankort: /^(5019)\d+$/,
      interpayment: /^(636)\d+$/,
      unionpay: /^(62|88)\d+$/,
      visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
      mastercard: /^5[1-5][0-9]{14}$/,
      amex: /^3[47][0-9]{13}$/,
      diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
      discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
      jcb: /^(?:2131|1800|35\d{3})\d{11}$/
    }

    for (var key in re) {
      if (re[key].test(number)) {
        if (key == 'visa'){
          return '001';
        }else if (key == 'mastercard'){
          return '002';
        }else if (key == 'jcb'){
          return '007';
        }
      }
    }
  }

  phoneNumberPress(event){
  	if (isPlatformBrowser(this.platformId)){
	    const pattern = /[0-9\+\-\ ]/;

	    let inputChar = String.fromCharCode(event.charCode);
	    if (event.keyCode != 8 && !pattern.test(inputChar)) {
	      event.preventDefault();
	    }
	}
  }
  cardNumberPress(event){
  	if (isPlatformBrowser(this.platformId)){
	    const pattern = /[0-9\+\-\ ]/;

	    let inputChar = String.fromCharCode(event.charCode);
	    if (event.keyCode != 8 && !pattern.test(inputChar)) {
	      event.preventDefault();
	    }
	}
  }
  onBlurCardNumber(value){
  	if (isPlatformBrowser(this.platformId)){
	    this.card_number = value.split(' ').join('');
	    if(this.card_number.length > 16){
	      this.card_number = new SlicePipe().transform(this.card_number+'',0,16)
	    }
	}
  }

  share() {
  	if (isPlatformBrowser(this.platformId)){
	    this.db.collection('user').doc(firebase.auth().currentUser.uid).collection('sent').doc(this.paymentSession).get()
	    .then((doc) => {
	      if (doc.exists) {
	        console.log(doc)
	        console.log(doc.data())
	        this.getLoginStatus().then((res)=>{
	        if(res.status == 'connected'){
	          this.fb.api('/me/permissions').then((response)=>{
	            var declined = [];
	            for (var i = 0; i < response.data.length; i++) {
	              console.log(response.data[i].status);
	                if (response.data[i].status == 'declined') {
	                    declined.push(response.data[i].permission)
	                }
	            }
	            if(declined.length > 0){
	              console.log(123)
	              var message_note = "Your current Facebook settings do not allow <strong>Got It</strong> access to your account.<br> If you wish to send this gift via Facebook, please click the sign in button below and <strong>Got It</strong> will assist you in changing these settings hassle free.<br> You can also send this gift via <strong>SMS</strong> or <strong>Email</strong>.";
	              alert(message_note);
	            }
	            else{
	              this.fb.ui({
	                method: 'send',
	                link: doc.data().voucherLink,
	              }).then((res) => {
	                console.log(res)
	                if(!res.hasOwnProperty("error_code")){
	                  this.db.collection('user').doc(firebase.auth().currentUser.uid).collection("sent").doc(this.paymentSession).update({
	                    step: 3
	                  }).then((res)=>{
	                    console.log('Update info payment success');
	                  }).catch((error)=>{
	                    console.log(error);
	                  })
	                }
	              })
	              .catch((error)=>{
	                console.log(1);
	                console.log(error);
	              });
	            }
	            console.log(declined);
	          })
	        }

	    });
	      }
	    });
	}

  }

  send_sms_or_email(){
  	if (isPlatformBrowser(this.platformId)){
	    console.log(this.paymentSession);
	    this.checkoutService.paymentSession =  this.paymentSession;
	    this.checkoutService.uid = firebase.auth().currentUser.uid;
	    this.db.collection('user').doc(firebase.auth().currentUser.uid).collection('sent').doc(this.paymentSession).get()
	    .then((doc) => {
	      if (doc.exists) {
	        return this.checkoutService.sendEmailOrSms().then((res)=>{
	          console.log(res);
	        }).catch((error)=>{
	          console.log(error);
	        });
	      }
	    });
	}
  }

  private extractData(res: Response) {
  	if (isPlatformBrowser(this.platformId)){
	    let body = res.json();
	    console.log(body);
	}
  }

  getLoginStatus() {
    return this.fb.getLoginStatus();
    //  .then(console.log.bind(console))
    //  .catch(console.error.bind(console));
  }

  /**
   * This is a convenience method for the sake of this example project.
   * Do not use this in production, it's better to handle errors separately.
   * @param error
   */
  private handleError(error) {
    console.error('Error processing action', error);
  }



  sanitize(url:string){
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  sendVoucherAppToApp(objectData){
    this.db.collection('user').doc(firebase.auth().currentUser.uid).collection("sent").doc(objectData.invoiceNo).update({
      step: 3
    }).then((res)=>{
      console.log('Update info payment success');
    }).catch((error)=>{
      console.log(error);
    })

  }

}
