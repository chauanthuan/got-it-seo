import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import * as firebase from 'firebase/app';
import { environment } from '../../environments/environment';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class CheckoutService {

  getSignatureUrl: string;
  loading: boolean;
  headers: Headers;
  params: URLSearchParams = new URLSearchParams();
  firstName: string;
  lastName: string;
  amount: number;
  cybersourceData: any;
  formParams: URLSearchParams = new URLSearchParams();
  cardNumber: string;
  expiredDate: string;
  securityCode: string;
  cardType: string;
  fingerprint: any;
  paymentSession: any;
  uid: any;
  atmBank: string;


  constructor(private http: Http) {
    this.getSignatureUrl = environment.firebaseFnsUrl + '/checkout/creditcard/sign';
    this.headers = new Headers({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': environment.origin
    });
  }

  getSignature(params): Promise<any> {
    this.params.set('paymentId', params.paymentId);
    this.params.set('uid', params.uid);
    this.params.set('cardCvn', params.securityCode);
    this.params.set('cardExpire', params.expireDate);
    this.params.set('cardNumber', params.cardNumber);
    this.params.set('firstName', params.firstName);
    this.params.set('lastName', params.lastName);
    this.params.set('email', params.email);


    return this.http
      .post(this.getSignatureUrl, this.params, {headers: this.headers})
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  checkoutPayment(): Promise<any> {
    console.log("checkoutPayment()");
    //let csUrl = this.cybersourceData.action;
    let csForm = this.cybersourceData.form;
    this.cardType = this.detectCardType(this.cardNumber);

    var card_type = '';


    var exp_date = this.expiredDate;
    exp_date = exp_date.replace('/', '-20');

    for (let data in csForm) {
      if (data == 'signature'){
        this.formParams.set(data, csForm[data].toString());
      }else{
        this.formParams.set(data, csForm[data]);
      }
    }
     let csUrl = 'https://testsecureacceptance.cybersource.com/silent/pay';
    // this.formParams.set('card_number', this.cardNumber);
    // this.formParams.set('card_type', card_type);
    // this.formParams.set('card_expiry_date', exp_date);
    // this.formParams.set('card_cvn', this.securityCode);

    return this.http.post(csUrl, this.formParams, {headers: new Headers({'Access-Control-Allow-Origin': '*'})})
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  private detectCardType(number) {
    var re = {
      electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
      maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
      dankort: /^(5019)\d+$/,
      interpayment: /^(636)\d+$/,
      unionpay: /^(62|88)\d+$/,
      visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
      mastercard: /^5[1-5][0-9]{14}$/,
      amex: /^3[47][0-9]{13}$/,
      diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
      discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
      jcb: /^(?:2131|1800|35\d{3})\d{11}$/
    }

    for (var key in re) {
      if (re[key].test(number)) {
        return key;
      }
    }
  }

  sendEmailOrSms(): Promise<any> {
    let params = {"id":this.paymentSession,"uid":this.uid};
    console.log(this.headers);
    return this.http.post(environment.firebaseFnsUrl+'/send/email_or_sms', params, {headers: this.headers})
              .toPromise();
              //.then(this.extractData)
              //.catch(this.handleError);
  }

  getMoMoPaymentUrl(params): Promise<any> {
    return this.http.post(environment.firebaseFnsUrl + '/momo/atm/checkout', params, {headers: this.headers})
      .toPromise()
      .then(this.extractData);
  }

}
