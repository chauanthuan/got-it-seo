import { Component, OnInit, ElementRef, ViewChild, Inject, PLATFORM_ID  } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import {MenuService} from '../menu/menu.service';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import {Http, Headers, Response, RequestOptions,URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {Router} from '@angular/router';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from '../../environments/environment';
import {CheckoutService} from '../checkout/checkout.service';
import { FacebookService, LoginResponse, LoginOptions, UIResponse, UIParams, FBVideoComponent } from 'ngx-facebook';
import { DomSanitizer } from '@angular/platform-browser';
import {ModalDirective} from 'ngx-bootstrap/modal';
import {TranslateService,LangChangeEvent} from '@ngx-translate/core';
import { Title, Meta }     from '@angular/platform-browser';

declare var $: any;

@Component({
  selector: 'app-gift-sent',
  templateUrl: './gift-sent.component.html',
  styleUrls: ['./gift-sent.component.scss'],
   host: {
    '(document:click)': 'onDocumentClick($event)',
    '(window:resize)': 'onResize($event)',
    '(window:scroll)': 'onScroll($event)'
  },
})
export class GiftSentComponent implements OnInit {
	filterDropdownClicked:boolean = false;
	isClicked:boolean = false;
	filterValue: any =[];
	filterValueSelected: any = [];
	sort_val: string;
	user: Observable<firebase.User>;
	voucherData: any = [];
	voucherDataTemp:any = [];
	voucherDataTempLoadMore;
	user_uid:string;
	userData: any;
    numOfVoucher:number = 0;
	db:any;
	lastFirebaseLoaded: any;
	limit:number = 5;
	showLoadBtn:boolean = false;
	loadMoreClicked: boolean = false;
	dataLoaded:boolean = false;
	loadMoreLoading: boolean = false;
	isMobile: boolean = false;
	noticeMessage: string;
	isLoading: boolean = false;
	@ViewChild('staticModal') public staticModal: ModalDirective;
	@ViewChild('lazyPoint') lazyPoint: ElementRef;
	constructor(
		public _menuService: MenuService,
	    private afAuth: AngularFireAuth,
	    private _http: Http,
	    private router: Router, 
	    public el: ElementRef,
	    public checkoutService:CheckoutService,
	    private fb: FacebookService,
	    private sanitizer:DomSanitizer,
	    public translate: TranslateService,
	    private titleService: Title,
	    private meta: Meta,
		@Inject(PLATFORM_ID) private platformId: Object
	) {
		this.db = firebase.firestore();
		if (isPlatformBrowser(this.platformId)){
		    var ua = navigator.userAgent;
		    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/i.test(ua)){
		      this.isMobile = true;
		    }
		    else{
		      this.isMobile = false;
		    }

		    fb.init({
		      appId: '468266393371236',
		      version: 'v2.4',
		      cookie     : true,  // enable cookies to allow the server to access
		      xfbml      : true,
		    });
		}
	    translate.get('gift_sent_title').subscribe((res: string) => {
	        this.titleService.setTitle(res);
	    });
	    this.meta.addTags([
	      {name: 'description', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
	      {name: 'copyright', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
	      {name: 'keywords', content: 'quà tặng doanh nghiệp , quà tặng đối tác , voucher quà tặng , quà tặng khách hàng , quà tặng Tết, quà tặng sự kiện , tặng thưởng nhân viên , quà tặng doanh nhân , egift, quà tặng online, tặng quà trực tuyến, tặng quà online , gift voucher , phiếu quà tặng , quà tặng trực tuyến ,thẻ quà tặng , tặng voucher ,tặng quà online , quà online , bán gift card , phiếu tặng quà , giftcards , mua phiếu quà tặng, quản lý quà tặng, quà đã tặng'}
	    ]);
	}


	ngOnInit() {
		if (isPlatformBrowser(this.platformId)){
			window.scrollTo(0, 0)
	  		this.user_uid = firebase.auth().currentUser.uid;
	  		this._menuService.show();
	  		var w_width = $(window).width();
	  		var w_height = $(window).height();
	  		var footer_height = $('footer').height();
	  		$('.managegift-section .parent').css({'min-height':(w_height - footer_height - 60)+'px'})
	  		
	  		var content_width = $('.managegift-section .parent').width();
	  		var path_right_width = (w_width - content_width)/2;
	  		$('.left-fixed-wrap').css({'width':path_right_width});
	  	}
  		this.filterValue = [
          {
            "name":"State_Sent",
            "value":3
          },
          {
            "name":"State_Purchased",
            "value":2
          }				
  			];
  		var db = firebase.firestore();
  		this.voucherData = [];
  		var first = db.collection('user').doc(this.user_uid).collection("sent").where('state','<',9).orderBy('state').orderBy('createdAt','desc');
		this.loadDataFireStore(first);

		this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
			this.translate.get('gift_sent_title').subscribe((res: string) => {
			  this.titleService.setTitle(res);
			});
		});
	}
	loadDataFireStore(collection, loadFirst = true){
  		//count all document show total
  		if(loadFirst){
  			collection.get().then((snapshot)=> {      
			    this.numOfVoucher = snapshot.size;
			    if(this.numOfVoucher < this.limit){
			    	this.showLoadBtn = false;
			    }
			    else{
			    	this.showLoadBtn = true;
			    }
			});
  		}
  	
  		if(!this.loadMoreClicked){
        return collection.limit(this.limit).onSnapshot((snapshot)=>{
          this.voucherDataTemp = [];
          this.lastFirebaseLoaded = snapshot.docs[snapshot.docs.length-1];
          if(snapshot.size < this.limit){

            this.showLoadBtn = false;
          }
          else{
            this.showLoadBtn = true;
          }
          this.voucherDataTemp = this.voucherDataTemp.concat(snapshot.docs);
          this.dataLoaded = true;
          this.loadMoreLoading = false;
        });
      }
      else{
        collection = collection.startAfter(this.lastFirebaseLoaded);
        return collection.limit(this.limit).get().then((snapshot)=>{
       // Get the last visible document
          this.lastFirebaseLoaded = snapshot.docs[snapshot.docs.length-1];

          if(snapshot.size < this.limit){
           this.showLoadBtn = false;
          }
          else{
           this.showLoadBtn = true;
          }
          this.voucherDataTemp = this.voucherDataTemp.concat(snapshot.docs);
          this.dataLoaded = true;
          this.loadMoreLoading = false;
        }).catch(err => {
           console.log("Error getting sub-collection documents", err);
         });
      } 
  	}

  	loadMore(){
  		this.loadMoreLoading = true;
  		this.loadMoreClicked = true;
      this.dataLoaded = false;
		
		this.filterDataFuction();
  	}
	
  	
  	updateCheckedOptions(option, event) {
  		this.dataLoaded = false;
  		this.showLoadBtn = false;
  		this.loadMoreClicked = false;
  		this.voucherDataTemp = [];
  		if(event.target.checked){
  			this.filterValueSelected.push(option);
  		}
  		else{
  			const index: number = this.filterValueSelected.indexOf(option);
		    if (index !== -1) {
		        this.filterValueSelected.splice(index, 1);
		    }    
  		}
  		
  		this.filterDataFuction();
	}

	
	onSortChange(val) {
	    this.sort_val = val;
	    this.dataLoaded = false;
  		this.showLoadBtn = false;
  		this.loadMoreClicked = false;
  		this.voucherDataTemp = [];
  		this.filterDataFuction();
	}

	filterDataFuction(){
		var filterCollection:any = {};
  		var voucherCollection = this.db.collection('user').doc(this.user_uid).collection("sent");
  		var sortValue = '';
  		if(this.sort_val == 'value'){
  			sortValue = 'price';
  		}
  		else if(this.sort_val == 'ordertime'){
  			sortValue = 'createdAt';
  		}
  		
  		if(this.filterValueSelected.length > 0){
  			this.filterValueSelected.sort( function(object1, object2) {
			    if ( object1.value < object2.value ){
			    	return -1;
			    }else if( object1.value > object2.value ){
			        return 1;
			    }else{
			    	return 0;	
			    }
			  });
  			//check filter choose one or more
  			
  			if(this.filterValueSelected.length == 1){
  				filterCollection = voucherCollection.where("step","==",this.filterValueSelected[0].value).where('state','<',9).orderBy('state');
          if(sortValue != ''){
            this.loadDataFireStore(filterCollection.orderBy(sortValue,'desc'), true);
          }
          else{
            this.loadDataFireStore(filterCollection.orderBy('createdAt','desc'), true);
          }
          return;
  			}
  			else if(this.filterValueSelected.length == 2){
  				filterCollection = voucherCollection.where('state','<',9);
          //if(sortValue != ''){
            this.loadDataFireStore(filterCollection.orderBy('state').orderBy('createdAt','desc'), true);
          // }
          // else{
          //   this.loadDataFireStore(filterCollection.orderBy('state').orderBy('createdAt','desc'), true);
          // }
          return;
  			}

  		} // no filter
  		else{
  			filterCollection = voucherCollection;
        this.loadDataFireStore(filterCollection.where('state','<',9).orderBy('state').orderBy('createdAt','desc'), true);
        // if(sortValue != ''){
        //   this.loadDataFireStore(filterCollection.where("step",">",1).orderBy('step').orderBy(sortValue,'desc'), true);
        // }
        // else{
        //   this.loadDataFireStore(filterCollection.where("step",">",1).orderBy('step').orderBy('createdAt','desc'), true);
        // }

  		}
	}

	calculateDay(date){
		let date1 = new Date();//today
		let date2 = new Date(date);//this is expired date
		var timeDiff = date2.getTime() - date1.getTime();
		let dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
		return dayDifference;
	}

	toggleClick(event):void{
    this.isClicked = !this.isClicked;
    var target = event.currentTarget;
    var idAttr = target.attributes.id.nodeValue;
    if(idAttr == 'filter'){
      this.filterDropdownClicked = !this.filterDropdownClicked;
    }
	}

	elementClicked(event){
	  	console.log(event);
	    //event.target.closest(".wrapper-dropdown").querySelector('small').innerHTML = event.target.innerText;
	}

	onDocumentClick(event) {
	    if (!event.target.closest(".filter_box")) { 
	        this.filterDropdownClicked = false;
	    }
	}

	onResize(event){
	    var w_width = $(window).width();
  		var w_height = $(window).height();
  		var footer_height = $('footer').height();
  		$('.managegift-section .parent').css({'min-height':(w_height - footer_height - 60)+'px'});

		var content_width = $('.managegift-section .parent').width();
		if(w_width >= content_width){
		  var path_right_width = (w_width - content_width)/2;
		}
		else{
		  path_right_width = 0;
		}
		$('.left-fixed-wrap').css({'width':path_right_width});
	}

	
	onScroll(event){
    	if(this.dataLoaded && this.lastFirebaseLoaded != undefined){
    		if ((window.innerHeight + window.pageYOffset) >= ($('.managegift-section').height() + 60)) {
	    		this.loadMore();
	    	}
    	}
    	
		
	}
	removeSelected(option,event){
	  	const index: number = this.filterValueSelected.indexOf(option);
	    if (index !== -1) {
	        this.filterValueSelected.splice(index, 1);
	    }  
	  	event.target.remove();
	  	this.dataLoaded = false;
  		this.showLoadBtn = false;
  		this.loadMoreClicked = false;
  		this.voucherDataTemp = [];
  		this.filterDataFuction();
	}

	removeRadioSelected(option,event){
		this.sort_val = null;  
		event.target.remove();
		this.dataLoaded = false;
		this.showLoadBtn = false;
		this.loadMoreClicked = false;
		this.voucherDataTemp = [];
		this.filterDataFuction();
	}
	send_sms_or_email(objectData){
	    this.isLoading = true;
	    console.log(objectData);
	    this.checkoutService.paymentSession =  objectData.invoiceNo;
	    this.checkoutService.uid = firebase.auth().currentUser.uid;
	    this.db.collection('user').doc(firebase.auth().currentUser.uid).collection('sent').doc(objectData.invoiceNo).get()
	    .then((doc) => {
	    	console.log(doc);
	      if (doc.exists) {
	        return this.checkoutService.sendEmailOrSms().then((res)=>{
	          let dataRes = JSON.parse(res._body);
	          if(dataRes.hasOwnProperty("stt")){
	            if(this.translate.currentLang == 'vi'){
	               this.noticeMessage = 'Voucher đã được gửi lại thành công.'
	            }else{
	               this.noticeMessage = 'Voucher has been resent successfully.';
	            }
	           
	          }
	          else{
	            this.noticeMessage = dataRes.msg;
	          }
	          this.isLoading = false;
	          
	          this.staticModal.show();
	        }).catch((error)=>{
	          console.log(error);
	          this.isLoading = false;
	          if(this.translate.currentLang == 'vi'){
	            this.noticeMessage = 'Quà tặng chưa được gửi thành công, vui lòng thực hiện lại.';         
	          }
	           else
	            this.noticeMessage = 'Something when wrong, please try again.';
	          	this.staticModal.show();
	        });
	      }
	      this.isLoading = false;
	    });
	}

	sendFbOrMsg(objectData){
	    if(this.isMobile){
	      this.noticeMessage = "Please open this page in 'Desktop' and try Send Now or Send Again.";
	      this.staticModal.show();
	    }
	    else{
	      this.getLoginStatus().then((res)=>{
	        if(res.status == 'connected'){
	          this.fb.api('/me/permissions').then((response)=>{
	            var declined = [];
	            for (var i = 0; i < response.data.length; i++) {
	              console.log(response.data[i].status);
	                if (response.data[i].status == 'declined') {
	                    declined.push(response.data[i].permission)
	                }
	            }
	            if(declined.length > 0){
	              var message_note = "Your current Facebook settings do not allow <strong>Got It</strong> access to your account.<br> If you wish to send this gift via Facebook, please click the sign in button below and <strong>Got It</strong> will assist you in changing these settings hassle free.<br> You can also send this gift via <strong>SMS</strong> or <strong>Email</strong>.";
	              alert(message_note);
	            }
	            else{
	              //share fb
	              this.fb.ui({
	                method: 'send',
	                link: objectData.voucherLink,
	              }).then((res) => {
	                console.log(res)
	                if(!res.hasOwnProperty("error_code")){
	                  this.db.collection('user').doc(firebase.auth().currentUser.uid).collection("sent").doc(objectData.invoiceNo).update({
	                    step: 3
	                  }).then((res)=>{
	                    console.log('Update info payment success');
	                  }).catch((error)=>{
	                    console.log(error);
	                  })
	                } 
	              })
	              .catch((error)=>{
	                console.log(1);
	                console.log(error);
	              });
	            }
	            console.log(declined);
	          })
	        }

	      });
	    }
	}

	getLoginStatus() {
		return this.fb.getLoginStatus();
	}

	sanitize(url:string){
	return this.sanitizer.bypassSecurityTrustUrl(url);
	}

	sendVoucherAppToApp(objectData){
		if(this.isMobile){
		  this.db.collection('user').doc(firebase.auth().currentUser.uid).collection("sent").doc(objectData.invoiceNo).update({
		    step: 3
		  }).then((res)=>{
		    console.log('Update info payment success');
		  }).catch((error)=>{
		    console.log(error);
		  })
		}
		else{
		  this.noticeMessage = "Please open this page in 'Mobile' and try Send Now or Send Again.";
		  this.staticModal.show();
		}
	}

}
