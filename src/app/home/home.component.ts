import { isPlatformBrowser } from '@angular/common';
import { Component, OnInit, ElementRef, Inject, PLATFORM_ID } from '@angular/core';
import { MenuService } from "../menu/menu.service";
import * as firebase from 'firebase/app';
import {TranslateService,LangChangeEvent} from '@ngx-translate/core';
import { Title, Meta }     from '@angular/platform-browser';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  host: {
    '(document:click)': 'onDocumentClick($event)',
  },
})
export class HomeComponent implements OnInit {

  faqDropdownClicked:boolean = false;
  brandDropdownClicked:boolean = false;
  isClicked:boolean = false;
  listDataAnnouncements: any;
  title: string = "Home"
  constructor(
    public _menuService: MenuService,
    private _eref: ElementRef,
    public translate: TranslateService,
    private titleService: Title,
    private meta: Meta,
    @Inject(PLATFORM_ID) private platformId: Object
    ) { 
    let title = translate.get('home_title').subscribe((res: string) => {
        this.titleService.setTitle(res);
    });
    this.meta.addTags([
      {name: 'description', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
      {name: 'copyright', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
      {name: 'keywords', content: 'got it , gotit ,voucher ,e voucher, egift, quà tặng doanh nghiệp , quà tặng đối tác , voucher quà tặng , quà tặng khách hàng , quà tặng Tết, quà tặng sự kiện , tặng thưởng nhân viên , quà tặng doanh nhân , egift, quà tặng online, tặng quà trực tuyến, tặng quà online , gift voucher , phiếu quà tặng , quà tặng trực tuyến ,thẻ quà tặng , tặng voucher ,tặng quà online , quà online , bán gift card , phiếu tặng quà , giftcards , mua phiếu quà tặng'}
    ]);
  }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)){
      window.scrollTo(0, 0)
    }
  	this._menuService.show();
    //get data announcement
    var ref = firebase.database().ref("announcements");
    
    
    ref.on("value", (snapshot) => {
      this.listDataAnnouncements = [];
      snapshot.forEach((childSnapshot)=>{
        this.listDataAnnouncements.push(childSnapshot.val());
        return false;
      });
    });
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.translate.get('home_title').subscribe((res: string) => {
        this.titleService.setTitle(res);
      });
    });
  }

  toggleClick(event):void{
    this.isClicked = !this.isClicked;
    var target = event.currentTarget;
    var idAttr = target.attributes.id.nodeValue;
  //  console.log(event.target.innerText);
  //  let small_element = document.getElementsByTagName('small')[0];
   // console.log(small_element);
    //$(this).closest('#tab_all_brand').find('span small').text($(this).text());
    if(idAttr == 'tab_all_faq'){
      this.faqDropdownClicked = !this.faqDropdownClicked;
    }
    if(idAttr == 'tab_all_brand'){
      this.brandDropdownClicked = !this.brandDropdownClicked;
    }
    // let body = document.getElementsByTagName('body')[0];
    // if(!this.isClicked){
    //   body.classList.remove('out');
    // }
    // else{
    //   body.classList.add('out');
    // }
  }

  elementClicked(event){
    event.target.closest(".wrapper-dropdown").querySelector('small').innerHTML = event.target.innerText;
  }

  onDocumentClick(event) {
    // console.log(event);
    if (!event.target.closest(".wrapper-dropdown")) { 
        this.brandDropdownClicked = false;
        this.faqDropdownClicked = false;
    }
  }

}
