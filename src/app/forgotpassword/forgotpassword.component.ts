import { Component, OnInit,ViewChild, ElementRef, TemplateRef, Inject, PLATFORM_ID  } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import {MenuService} from '../menu/menu.service';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import {Router} from '@angular/router';
import { Title, Meta }     from '@angular/platform-browser';
import {TranslateService,LangChangeEvent} from '@ngx-translate/core';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.scss']
})
export class ForgotpasswordComponent implements OnInit {
  isBtnActive:boolean = false;
  email:string;
  error:string;
  messageSuccess:string;
  user: Observable<firebase.User>;
  private userDetails: firebase.User = null;
  constructor(
    private router: Router,
    public _menuService: MenuService,
    private afAuth: AngularFireAuth,
    private titleService: Title,
    private meta: Meta,
    public translate: TranslateService,
    @Inject(PLATFORM_ID) private platformId: Object) {
    this.afAuth.auth.onAuthStateChanged(auth=>{
      if(auth){
          this.router.navigate(['/']); 
        }
        // else{
        //   this.userDetails = auth;
        // }
      });
    translate.get('forgotpass_title').subscribe((res: string) => {
        this.titleService.setTitle(res);
    });
    this.meta.addTags([
      {name: 'description', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
      {name: 'copyright', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
      {name: 'keywords', content: 'quà tặng doanh nghiệp , quà tặng đối tác , voucher quà tặng , quà tặng khách hàng , quà tặng Tết, quà tặng sự kiện , tặng thưởng nhân viên , quà tặng doanh nhân , egift, quà tặng online, tặng quà trực tuyến, tặng quà online , gift voucher , phiếu quà tặng , quà tặng trực tuyến ,thẻ quà tặng , tặng voucher ,tặng quà online , quà online , bán gift card , phiếu tặng quà , giftcards , mua phiếu quà tặng'}
    ]);
  }

  ngOnInit() {
  	if (isPlatformBrowser(this.platformId)){
  		window.scrollTo(0, 0)
	    // console.log(firebase.auth());
	    this._menuService.show();
	    let body = document.getElementsByTagName('body')[0];

	    if(!this.isBtnActive){
	    	body.classList.remove('out');
	    }
	    else{
	    	body.classList.add('out');
	    }
	    let footer = document.getElementsByTagName('footer')[0];
	    body.classList.add('forgotpassword');
	    footer.classList.add('hide');
	    body.click();
	    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
	    	this.translate.get('forgotpass_title').subscribe((res: string) => {
	    		this.titleService.setTitle(res);
	    	});
	    });
	}

	}
  forgot(){
      if(this.email){
      var actionCodeSettings = {
        url: environment.origin,
        handleCodeInApp: false
      };
      this.afAuth.auth.sendPasswordResetEmail(this.email, actionCodeSettings)
        .then( 
          (resp) => {
            if(this.translate.currentLang == 'vi'){
               this.messageSuccess = 'Liên kết đặt lại mật khẩu đã được gửi đến email của bạn. Vui lòng kiểm tra email của bạn.';
            }else{
              this.messageSuccess = 'Link reset password has sent to your email. Please check your email.';
            }
         
          } 
        ).catch((error) => {
          this.error = error.message;      
        });
      }
      else{
        this.error = 'Please enter your email..';
      }
  }
  ngOnDestroy() {
  	if (isPlatformBrowser(this.platformId)){
      let body = document.getElementsByTagName('body')[0];
      let footer = document.getElementsByTagName('footer')[0];
      body.classList.remove("login");
      footer.classList.remove('hide');
    }  
  }
}
