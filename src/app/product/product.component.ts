import { Component, OnInit,ViewChild, ElementRef, TemplateRef, Inject, PLATFORM_ID, ViewChildren, QueryList  } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import {SelectComponent} from "ng2-select/ng2-select";
import {AgmMap} from '@agm/core';
import {MapsAPILoader} from '@agm/core/services/maps-api-loader/maps-api-loader';
import {Http, Headers, Response, RequestOptions, URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {ListbrandComponent} from "../listbrand/listbrand.component";
import {UrlService} from '../common/url.service';
import { MenuService } from "../menu/menu.service";
import { ProductService } from "./product.service";
import {Router} from '@angular/router';
import { environment } from '../../environments/environment';
import { LandingService } from "../landing/landing.service";
import * as firebase from 'firebase/app';
import { TranslateService , LangChangeEvent} from '@ngx-translate/core';
import { Title, Meta }     from '@angular/platform-browser';

declare var google: any
declare var $: any;

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  host: {
    '(window:resize)': 'onResize($event)'
  },
})
export class ProductComponent implements OnInit {
  brandDropdownClicked: boolean = false;
  isClicked: boolean = false;
  lat: number = 16.452302;
  long: number = 107.554779;
  latlngBounds: any;
  markers: any;
  zoom: number = 6;
  priceValue: number; // this value will set after get data from realtime database
  quantity: number = 1;

  dataProduct: any;
  listBrand: any;
  listCity: any;
  listDistrict: any;
  selectedCity_val: any;
  selectedBrand_val: any;
  selectedDistrict_val: any;
  listOurBrand: any;
  listTempOurBrand: any;
  category_id: number = 0;
  productID: any = environment.defaultProductId;
  priceID: any; // this value will set after get data from realtime database
  productNameEn: any;
  productNameVi: any;
  brandName: any;
  imgPath: any = 'https://img.gotit.vn/compress/580x580/2018/03/1521708477_ocpeM.png';
  private apiUrlProductDetail = 'product/detail';
  private productIdLoadData = environment.defaultProductId;
//    private apiUrlLoadBrand = 'api/brand/';

  public staticProductData: any;
  public listProductPrice: any;
  private productStaticImg: any;
  private productStaticNm: any;
  ListData:any;
  productShortDesc: any;
  productDesc: any;
  serviceGuide:any;
  isLoading: boolean= false;
  public value: any = {};
  public _disabledV: string = '0';
  public disabled: boolean = false;
  public isCollapsedBrand: boolean = false;
  public isCollapsedHowwork: boolean = false;
  public isCollapsedTermCondition: boolean = false;
  isbrowser:boolean = false;
  @ViewChildren(SelectComponent) selectElements: QueryList<SelectComponent>
  @ViewChild(AgmMap) myMap: AgmMap;
  @ViewChild('selectBrand') selectBrand;
  @ViewChild('selectCity') selectCity;
  @ViewChild('selectDistrict') selectDistrict;
  constructor(
    private mapsAPILoader: MapsAPILoader,
    private _http: Http,
    private urlService: UrlService,
    public _menuService: MenuService,
    private _productService : ProductService,
    private router: Router,
    private _landingService: LandingService,
    public translate: TranslateService,
    private titleService: Title,
    private meta: Meta,
    @Inject(PLATFORM_ID) private platformId: Object,
    ) {
    translate.get('send_gift_title').subscribe((res: string) => {
        this.titleService.setTitle(res);
    });
    this.meta.addTags([
      {name: 'description', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
      {name: 'copyright', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
      {name: 'keywords', content: 'got it , gotit ,voucher ,e voucher, egift, quà tặng doanh nghiệp , quà tặng đối tác , voucher quà tặng , quà tặng khách hàng , quà tặng Tết, quà tặng sự kiện , tặng thưởng nhân viên , quà tặng doanh nhân , egift, quà tặng online, tặng quà trực tuyến, tặng quà online , gift voucher , phiếu quà tặng , quà tặng trực tuyến ,thẻ quà tặng , tặng voucher ,tặng quà online , quà online , bán gift card , phiếu tặng quà , giftcards , mua phiếu quà tặng'}
    ]);
  }
 getProductData(): Promise<any> {
    let url = environment.firebaseFnsUrl+'/app/product/detail?product_id='+this.productIdLoadData+'&language='+this.translate.currentLang;
    return this._http.get(url).toPromise().then(response => response.json());
  }


  ngOnInit() {
    if (isPlatformBrowser(this.platformId)){
      this.isbrowser = true;
    this.isLoading = true;
    //check landing page chooseProduct clicked or not
    if(this._landingService.productID || this._landingService.priceID || this._landingService.priceValue){

      this.productID = this._landingService.productID;
      this.priceID = this._landingService.priceID;
      this.priceValue = this._landingService.priceValue;
     
      //get data
      var ref = firebase.database().ref("products/"+this.productID);
      ref.once("value").then((snapshot) => {
        this.staticProductData = snapshot.val();
        //set image path
        this.imgPath = snapshot.val().image;
        //set list price + value
        this.listProductPrice = snapshot.val().prices;
        this.productNameEn = snapshot.val().name_en;
        this.productNameVi = snapshot.val().name_vi;
        this.isLoading = false;
      }); 
    }
    else{
        var ref = firebase.database().ref("products/"+this.productID);
        ref.once("value").then((snapshot) => {
          console.log();
          this.staticProductData = snapshot.val();
          //set image path
          this.imgPath = snapshot.val().image;
          //set list price + value
          this.listProductPrice = snapshot.val().prices;
          //set product price and product value
          this.priceID = Object.keys(this.listProductPrice)[0];    
          this.priceValue = this.listProductPrice[this.priceID];
          this.productNameEn = snapshot.val().name_en;
          this.productNameVi = snapshot.val().name_vi;
          this.isLoading = false;
        }); 
    }
   
      window.scrollTo(0, 0);
      var w_width = $(window).width();
      var content_width = $('.product-section .parent').width();
      var path_right_width = (w_width - content_width) / 2;
      $('.right-fixed-wrap').css({'width': path_right_width});
    
    this._menuService.show();
    //get data return from this function

    this.dataProduct = this.getProductData();
      this.dataProduct.then(data => {
        //this.productName = data.productNm;
        this.brandName = data.brandNm;
        this.productDesc = data.productDesc;
        this.productShortDesc = data.productShortDesc;
        this.serviceGuide = data.serviceGuide;
        let StoreList = data.storeList;
        this.ListData = data.storeList;
        this.markers = [];
        this.listCity = [{id: '0', text: this.translate.currentLang == 'vi'?'Tất cả':'All'}];
        this.listDistrict = [{id: '0', text: this.translate.currentLang == 'vi'?'Tất cả':'All'}];
        this.listBrand = [{id: '0', text: this.translate.currentLang == 'vi'?'Tất cả':'All'}];
        for (let key in StoreList) {
          var value = StoreList[key];
          //add value isOpen in storeList
          value['isOpen'] = false;
          if (!(this.listCity.find(x => x.id == value.city_id))) {
            this.listCity.push({id: value.city_id, text: value.city});
          }
          if (!(this.listDistrict.find(x => x.id == value.dist_id))) {
            this.listDistrict.push({id: value.dist_id, text: value.district});
          }
          if (!(this.listBrand.find(x => x.id == value.brand.brandId))) {
            this.listBrand.push({id: value.brand.brandId, text: value.brand.brandNm});
          }
          this.markers.push(value);
        }
        this.loadGoogleMap();

        this.selectBrand.items = this.listBrand;
        this.selectBrand.active.push(this.listBrand[0]);
        this.selectCity.items = this.listCity;
        this.selectCity.active.push(this.listCity[0]);
        this.selectDistrict.items = this.listDistrict;
        this.selectDistrict.active.push(this.listDistrict[0]);
        // console.log(this.markers);
      });

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.translate.get('send_gift_title').subscribe((res: string) => {
        this.titleService.setTitle(res);
      });
      
      this.dataProduct = this.getProductData();
      this.dataProduct.then(data => {
        //this.productName = data.productNm;
        this.brandName = data.brandNm;
        this.productDesc = data.productDesc;
        this.productShortDesc = data.productShortDesc;
        this.serviceGuide = data.serviceGuide;
        let StoreList = data.storeList;
        this.ListData = data.storeList;
        this.markers = [];
        this.listCity = [{id: '0', text: this.translate.currentLang == 'vi'?'Tất cả':'All'}];
        this.listDistrict = [{id: '0', text: this.translate.currentLang == 'vi'?'Tất cả':'All'}];
        this.listBrand = [{id: '0', text: this.translate.currentLang == 'vi'?'Tất cả':'All'}];
        for (let key in StoreList) {
          var value = StoreList[key];
          //add value isOpen in storeList
          value['isOpen'] = false;
          if (!(this.listCity.find(x => x.id == value.city_id))) {
            this.listCity.push({id: value.city_id, text: value.city});
          }
          if (!(this.listDistrict.find(x => x.id == value.dist_id))) {
            this.listDistrict.push({id: value.dist_id, text: value.district});
          }
          if (!(this.listBrand.find(x => x.id == value.brand.brandId))) {
            this.listBrand.push({id: value.brand.brandId, text: value.brand.brandNm});
          }
          this.markers.push(value);
        }
        this.loadGoogleMap();

        this.selectBrand.items = this.listBrand;
        this.selectBrand.active.push(this.listBrand[0]);
        this.selectCity.items = this.listCity;
        this.selectCity.active.push(this.listCity[0]);
        this.selectDistrict.items = this.listDistrict;
        this.selectDistrict.active.push(this.listDistrict[0]);
        // console.log(this.markers);
      });
    });

    // this.getBrandData(this.category_id).then(dataBrand=>{
    //   this.listOurBrand = dataBrand;
    //   this.listTempOurBrand = dataBrand;
    // });

    }

  }

  onResize(event){
    if (isPlatformBrowser(this.platformId)){
      var w_width = $(window).width();
      var content_width = $('.product-section .parent').width();
      if (w_width >= content_width) {
        var path_right_width = (w_width - content_width) / 2;
      }
      else {
        path_right_width = 0;
      }
      $('.right-fixed-wrap').css({'width': path_right_width});
    }
  }


  loadGoogleMap(): void {
    if (isPlatformBrowser(this.platformId)){
      this.mapsAPILoader.load().then(() => {
        this.latlngBounds = new window['google'].maps.LatLngBounds();
        this.markers.forEach((location) => {
          this.latlngBounds.extend(new window['google'].maps.LatLng(location.lat, location.long))
        });

      });
    }
  }

  ngAfterViewInit() {
  
  }

  ngAfterViewContent() {
  
  }
  markerClick(marker): void {
    if (isPlatformBrowser(this.platformId)){
      this.latlngBounds = null;
      this.zoom = 15;
      this.myMap.triggerResize()
        .then(() => {
          (this.myMap as any)._mapsWrapper.setCenter({lat: marker.lat, lng: marker.long});
        });
    }
  }

  zoomChange(event) {
    if (isPlatformBrowser(this.platformId)){
      if(event > 15){
        event = 15;
      }
      this.zoom = event;
    }
  }

  public get disabledV(): string {
    return this._disabledV;
  }

  public set disabledV(value: string) {
    this._disabledV = value;
    this.disabled = this._disabledV === '1';
  }

  

  public selectedBrand(value: any) {
    if (isPlatformBrowser(this.platformId)){
      if (value.id == '0') {
        this.selectedBrand_val = undefined;
      }
      else
        this.selectedBrand_val = value;

      this.selectedCity_val = undefined;
      this.selectCity.active = [{id: '0', text: this.translate.currentLang == 'vi'?'Tất cả':'All'}];
      this.selectedDistrict_val = undefined;
      this.selectDistrict.active = [{id: '0', text: this.translate.currentLang == 'vi'?'Tất cả':'All'}];
      this.filterDataFunction(this.ListData, true, true);
    }
  }

  public selectedCity(value: any) {
    if (isPlatformBrowser(this.platformId)){
      if (value.id == '0') {
        this.selectedCity_val = undefined;
      }
      else
        this.selectedCity_val = value;
      console.log('Selected City value is: ', value);

      this.selectedDistrict_val = undefined;
      this.selectDistrict.active = [{id: '0', text: this.translate.currentLang == 'vi'?'Tất cả':'All'}];

      this.filterDataFunction(this.ListData, false, true);
    }
  }

  public selectedDistrict(value: any) {
    if (isPlatformBrowser(this.platformId)){
      console.log('Selected District value is: ', value);
      if (value.id == '0') {
        this.selectedDistrict_val = undefined;
      }
      else
        this.selectedDistrict_val = value;

      this.filterDataFunction(this.ListData , false, false);
    }
  }

  filterDataFunction(dataSort, loadCity, loadDistrict){
    console.log(loadCity, loadDistrict)
    console.log(this.selectedBrand_val, this.selectedCity_val, this.selectedDistrict_val)
    let listNewData = [];
    listNewData = dataSort.filter(
        store => {
          //has brand and don't has city, don't has district
          if (typeof this.selectedBrand_val != 'undefined' && typeof this.selectedCity_val == 'undefined' && typeof this.selectedDistrict_val == 'undefined') {
            //console.log('vao1');
            return store.brand.brandId == this.selectedBrand_val.id;
          }
          //has city and don't has brand, don't has district
          else if (typeof this.selectedBrand_val == 'undefined' && typeof this.selectedCity_val != 'undefined' && typeof this.selectedDistrict_val == 'undefined') {
            //console.log('vao2');
            return store.city_id == this.selectedCity_val.id;
          }
          //has district and don't has brand, don't has city
          else if (typeof this.selectedBrand_val == 'undefined' && typeof this.selectedCity_val == 'undefined' && typeof this.selectedDistrict_val != 'undefined') {
            //console.log('vao3');
            return store.dist_id == this.selectedDistrict_val.id;
          }
          //has brand and has city, don't has district
          else if (typeof this.selectedBrand_val != 'undefined' && typeof this.selectedCity_val != 'undefined' && typeof this.selectedDistrict_val == 'undefined') {
            //console.log('vao4');
            return store.brand.brandId == this.selectedBrand_val.id && store.city_id == this.selectedCity_val.id;
          }
          //has brand and has district, don't has city
          else if (typeof this.selectedBrand_val != 'undefined' && typeof this.selectedCity_val == 'undefined' && typeof this.selectedDistrict_val != 'undefined') {
            //console.log('vao5');
            return store.brand.brandId == this.selectedBrand_val.id && store.dist_id == this.selectedDistrict_val.id;
          }
          //has city and has district, don't has brand
          else if (typeof this.selectedBrand_val == 'undefined' && typeof this.selectedCity_val != 'undefined' && typeof this.selectedDistrict_val != 'undefined') {
            //console.log('vao6');
            return store.dist_id == this.selectedDistrict_val.id && store.city_id == this.selectedCity_val.id;
          }
          //has city and has district and has brand
          else if (typeof this.selectedBrand_val != 'undefined' && typeof this.selectedCity_val != 'undefined' && typeof this.selectedDistrict_val != 'undefined') {
            //console.log('vao7');
            return store.brand.brandId == this.selectedBrand_val.id && store.city_id == this.selectedCity_val.id && store.dist_id == this.selectedDistrict_val.id;
          }
          else {
            //console.log('vao8');
            return store;
          }
        }
      )
    //reset all mapdata
    this.markers = [];
   // this.listCity = [{id: '0', text: this.translate.currentLang == 'vi'?'Tất cả':'All'}];
   // this.listDistrict = [{id: '0', text: this.translate.currentLang == 'vi'?'Tất cả':'All'}];
    //this.listBrand = [{id:'0',text:this.translate.currentLang == 'vi'?'Tất cả':'All'}];

    //reset select box here
  
    if(loadCity && loadDistrict){
      //click chon brand => load lại city va distric
      this.listCity = [{id: '0', text: this.translate.currentLang == 'vi'?'Tất cả':'All'}];
      this.listDistrict = [{id: '0', text: this.translate.currentLang == 'vi'?'Tất cả':'All'}];

      for (let key in listNewData) {

        var value = listNewData[key];

        if (!(this.listCity.find(x => x.id == value.city_id))) {
          this.listCity.push({id: value.city_id, text: value.city});
        }
        if (!(this.listDistrict.find(x => x.id == value.dist_id))) {
          this.listDistrict.push({id: value.dist_id, text: value.district});
        }
        this.markers.push(value);
      }
    }
    if(!loadCity && loadDistrict){
      //click chon city => chỉ load lại district
      this.listDistrict = [{id: '0', text: this.translate.currentLang == 'vi'?'Tất cả':'All'}];
      

      for (let key in listNewData) {

        var value = listNewData[key];
        if (!(this.listDistrict.find(x => x.id == value.dist_id))) {
          this.listDistrict.push({id: value.dist_id, text: value.district});
        }
        this.markers.push(value);
      }
    }

    if(!loadCity && !loadDistrict){
      for (let key in listNewData) {
        var value = listNewData[key];
        if (!(this.listDistrict.find(x => x.id == value.dist_id))) {
          this.listDistrict.push({id: value.dist_id, text: value.district});
        }
        this.markers.push(value);
      }
    }

    this.loadGoogleMap();

    //this.selectBrand.items = this.listBrand;
    //this.selectBrand.active.push(this.listBrand[0]);
    this.selectCity.items = this.listCity;
    //this.selectCity.active.push(this.listCity[0]);
    this.selectDistrict.items = this.listDistrict;
    //this.selectDistrict.active.push(this.listDistrict[0]);
  }


  public removed(value: any) {
    console.log('Removed value is: ', value);
  }

  public typed(value: any) {
    console.log('New search input: ', value);
  }

  public refreshValue(value: any) {
    this.value = value;
  }


  public closeOtherSelects(element) {
    if (isPlatformBrowser(this.platformId)){
      if (element.optionsOpened == true) {
        let elementsToclose = this.selectElements.filter(function (el: any) {
          return (el != element && el.optionsOpened == true)
        });
        elementsToclose.forEach(function (e: SelectComponent) {
          e.clickedOutside();
        });
      }
    }
  }

  public collapsed(event: any): void {
    console.log(event);
  }

  public expanded(event: any): void {
    console.log(event);
  }

  find_nearest_store(): void {
    if (isPlatformBrowser(this.platformId)){
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(position => {
          this.lat = position.coords.latitude;
          this.long = position.coords.longitude;

          //this function return list location and sort with distance
          this.dataProduct.then(data => {
            let locations = [];
            let StoreList = data.storeList;
            for (let key in StoreList) {
              locations[key] = StoreList[key];

              let dist = this.Haversine(locations[key]['lat'], locations[key]['long'], this.lat, this.long);
              locations[key]['distance'] = dist;
            }

            locations.sort(function (a, b) {
              return parseFloat(a.distance) - parseFloat(b.distance);
            });

            let listNewData = locations.filter(
              store => {
                if (typeof this.selectedBrand_val != 'undefined' && typeof this.selectedCity_val == 'undefined' && typeof this.selectedDistrict_val == 'undefined') {
                  //console.log('vao1');
                  return store.brand.brandId == this.selectedBrand_val.id;
                }
                else if (typeof this.selectedBrand_val == 'undefined' && typeof this.selectedCity_val != 'undefined' && typeof this.selectedDistrict_val == 'undefined') {
                  //console.log('vao2');
                  return store.city_id == this.selectedCity_val.id;
                }
                else if (typeof this.selectedBrand_val == 'undefined' && typeof this.selectedCity_val == 'undefined' && typeof this.selectedDistrict_val != 'undefined') {
                  //console.log('vao3');
                  return store.dist_id == this.selectedDistrict_val.id;
                }
                else if (typeof this.selectedBrand_val != 'undefined' && typeof this.selectedCity_val != 'undefined' && typeof this.selectedDistrict_val == 'undefined') {
                  //console.log('vao4');
                  return store.brand.brandId == this.selectedBrand_val.id && store.city_id == this.selectedCity_val.id;
                }
                else if (typeof this.selectedBrand_val != 'undefined' && typeof this.selectedCity_val == 'undefined' && typeof this.selectedDistrict_val != 'undefined') {
                  //console.log('vao5');
                  return store.brand.brandId == this.selectedBrand_val.id && store.dist_id == this.selectedDistrict_val.id;
                }
                else if (typeof this.selectedBrand_val == 'undefined' && typeof this.selectedCity_val != 'undefined' && typeof this.selectedDistrict_val != 'undefined') {
                  //console.log('vao6');
                  return store.dist_id == this.selectedDistrict_val.id && store.city_id == this.selectedCity_val.id;
                }
                else if (typeof this.selectedBrand_val != 'undefined' && typeof this.selectedCity_val != 'undefined' && typeof this.selectedDistrict_val != 'undefined') {
                  //console.log('vao7');
                  return store.brand.brandId == this.selectedBrand_val.id && store.city_id == this.selectedCity_val.id && store.dist_id == this.selectedDistrict_val.id;
                }
                else {
                  //console.log('vao8');
                  return store;
                }

              }
            )

            this.markers = [];
            this.listCity = [{id: '0', text: this.translate.currentLang == 'vi'?'Tất cả':'All'}];
            this.listDistrict = [{id: '0', text: this.translate.currentLang == 'vi'?'Tất cả':'All'}];
            //this.listBrand = [{id:'0',text:this.translate.currentLang == 'vi'?'Tất cả':'All'}];
            for (let key in listNewData) {

              var value = listNewData[key];

              if (!(this.listCity.find(x => x.id == value.city_id))) {
                this.listCity.push({id: value.city_id, text: value.city});
              }
              if (!(this.listDistrict.find(x => x.id == value.dist_id))) {
                this.listDistrict.push({id: value.dist_id, text: value.district});
              }
              // if(!(this.listBrand.find(x => x.id == value.brand.brandId))){
              //    this.listBrand.push({id: value.brand.brandId, text: value.brand.brandNm});
              // }
              this.markers.push(value);
            }

            this.markers[0]['isOpen'] = true;
            this.latlngBounds = null;
            this.zoom = 15;
            this.myMap.triggerResize()
              .then(() => {
                (this.myMap as any)._mapsWrapper.setCenter({lat: this.markers[0].lat, lng: this.markers[0].long});
              })
            ;
          });
        });
      }
      this.isClicked = false;
    }
  }

  filterMapClick(event): void {
    this.isClicked = !this.isClicked;
  }


  // Convert Degress to Radians
  Deg2Rad(deg) {
    return deg * Math.PI / 180;
  }

  // Get Distance between two lat/lng points using the Haversine function
  // First published by Roger Sinnott in Sky & Telescope magazine in 1984 (“Virtues of the Haversine”)
  //
  Haversine(lat1, long1, lat2, long2) {
    var R = 6372.8; // Earth Radius in Kilometers

    var dLat = this.Deg2Rad(lat2 - lat1);
    var dlong = this.Deg2Rad(long2 - long1);

    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(this.Deg2Rad(lat1)) * Math.cos(this.Deg2Rad(lat2)) * Math.sin(dlong / 2) * Math.sin(dlong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;

    // Return Distance in Kilometers
    return d;
  }

  applyFilter(event): void {
    if (isPlatformBrowser(this.platformId)){
      this.isClicked = false;
      let listNewData = [];

      this.dataProduct.then(data => {
        let StoreList = data.storeList;

        this.filterDataFunction(StoreList , false, false);
      });
    }
  }

  toggleClick(event): void {
    if (isPlatformBrowser(this.platformId)){
      var target = event.currentTarget;
      var idAttr = target.attributes.id.nodeValue;

      if (idAttr == 'tab_all_brand') {
        this.brandDropdownClicked = !this.brandDropdownClicked;
      }
    }
  }

  elementClicked(event) {
    if (isPlatformBrowser(this.platformId)){
      this.category_id = event.currentTarget.attributes['data-id'].nodeValue;
      this.listTempOurBrand = this.listOurBrand.filter(
        brand => {

          if (this.category_id != 0) {
            for (var i = 0; i < brand['categoryID'].length; i++) {
              return brand['categoryID'].toString() == this.category_id;
            }
            ;
          }
          else {
            return brand;
          }
        });
      
        event.target.closest(".wrapper-dropdown").querySelector('small').innerHTML = event.target.innerText;
      
    }
  }

  onDocumentClick(event) {
    // console.log(event);
    if (isPlatformBrowser(this.platformId)){
      if (!event.target.closest(".wrapper-dropdown")) {
        this.brandDropdownClicked = false;
      }
    }
  }

  quantitykeyPress(event: any) {
    if (isPlatformBrowser(this.platformId)){
      const pattern = /[0-9\+\-\ ]/;

      let inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode != 8 && !pattern.test(inputChar)) {
        event.preventDefault();
      }
    }
  }

  onRadioChange(val,productID,priceID) {
    // console.log(elementRef.nativeElement.getAttribute('data-productID'));
    this.productID = productID;
    this.priceID = priceID;
    this.priceValue = val;
    // this.imgPath = imgPath;
    // $('.main-img img').attr({'src':imgPath});
    //localStorage.setItem('productPrice', val);
  }

  changeQuantity(action) {
    if (action == 'minus') {
      if (this.quantity > 1) {
        this.quantity = Number(this.quantity) - 1;
      }
      else {
        this.quantity = 1;
      }
      //localStorage.setItem('productQuantity', this.quantity.toString());
    }
    else {
      this.quantity = Number(this.quantity) + 1;
      //localStorage.setItem('productQuantity', this.quantity.toString());
    }
  }

  send_gift(){
    this._productService.productID =  this.productID;
    this._productService.priceID = this.priceID;
    this._productService.quantity = this.quantity;
    this._productService.priceValue = this.priceValue; 
    this._productService.productNameEn = this.productNameEn ? this.productNameEn : 'Got It eGift';
    this._productService.productNameVi = this.productNameVi ? this.productNameVi : 'Quà tặng Got It';
    this._productService.brandName = this.brandName ? this.brandName : 'Got It';
    this._productService.imgPath = this.imgPath ? this.imgPath :'https://img.gotit.vn/compress/580x580/2018/03/1521708477_ocpeM.png'; 
    this.router.navigate(['/checkout']);
  }
}