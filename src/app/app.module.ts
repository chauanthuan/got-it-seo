import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
// import {TranslateModule} from 'ng2-translate';
import { RouterModule, Routes } from '@angular/router';
import {Http, HttpModule} from '@angular/http'
// import { TranslateLoader, TranslateStaticLoader } from "ng2-translate";
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HomeComponent } from './home/home.component';
import { ListbrandComponent } from './listbrand/listbrand.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from '../environments/environment';
import { AppRoutes } from "./app.routes";

import { DatePipe } from '@angular/common';
import {ConvertArrayPipe} from './common/convertarray.pipe';
import {ChunkPipe} from './common/chunkArray.pipe';
import {dateFormatPipe} from './common/formatDate.pipe';
import {UrlService} from './common/url.service';
import { MenuComponent } from './menu/menu.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import {MenuService} from './menu/menu.service';
import { BsModalService, CollapseModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';
import * as firebase from 'firebase/app';
import { LandingComponent } from './landing/landing.component';
import {LandingService} from './landing/landing.service';
import { ProductComponent } from './product/product.component';
import {ProductService} from './product/product.service';
import { AboutComponent } from './about/about.component';
import { SupportComponent } from './support/support.component';
import {AccordionModule} from 'ng2-accordion';
import { SelectModule } from 'ng2-select';
import { AgmCoreModule } from '@agm/core';
import { AgmSnazzyInfoWindowModule } from '@agm/snazzy-info-window';
import { FooterComponent } from './footer/footer.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { OperatingregulationComponent } from './operatingregulation/operatingregulation.component';
import { CheckoutComponent } from './checkout/checkout.component';
import {TextMaskModule} from "angular2-text-mask";
import {CheckoutService} from './checkout/checkout.service';
import { FacebookModule } from 'ngx-facebook';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { EmailVerifyComponent } from './email-verify/email-verify.component';
import { AccountComponent } from './account/account.component';
import { AuthGuard } from './services/auth-guard.service';
import { MenuLeftComponent } from './menu-left/menu-left.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { PaymentWaitingComponent } from './payment-waiting/payment-waiting.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { GiftSentComponent } from './gift-sent/gift-sent.component';
import { GiftReceivedComponent } from './gift-received/gift-received.component';

firebase.initializeApp(environment.firebase);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ListbrandComponent,
    ConvertArrayPipe,
    ChunkPipe,
    dateFormatPipe,
    MenuComponent,
    LandingComponent,
    ProductComponent,
    AboutComponent,
    SupportComponent,
    FooterComponent,
    TermsComponent,
    PrivacypolicyComponent,
    OperatingregulationComponent,
    SupportComponent,
    CheckoutComponent,
        ForgotpasswordComponent,
    ResetpasswordComponent,
    EmailVerifyComponent,
    AccountComponent,
    MenuLeftComponent,
    ChangepasswordComponent,
    PaymentWaitingComponent,
    SignupComponent,
    LoginComponent,
    GiftSentComponent,
    GiftReceivedComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'ssr-app'}),
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: (createTranslateLoader),
            deps: [HttpClient]
        }
    }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule, // imports firebase/database, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features
    AppRoutes,
    AccordionModule,
    SelectModule,
    CollapseModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAwFOq5b6SR8A7nBS8ekxJE70A_88VQxx4'
    }),
    AgmSnazzyInfoWindowModule,
    TextMaskModule,
    FacebookModule.forRoot()
  ],
  providers: [
    DatePipe,
    UrlService,
    MenuService,
    AuthGuard,
    AuthService,
    BsModalService,
    LandingService,
    ProductService,
    CheckoutService,
    dateFormatPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}