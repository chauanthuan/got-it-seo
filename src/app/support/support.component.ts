import { Component, OnInit,ViewChild, ElementRef, TemplateRef, Inject, PLATFORM_ID  } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { MenuService } from "../menu/menu.service";
import { environment } from '../../environments/environment';
import { Title, Meta }     from '@angular/platform-browser';
import {TranslateService,LangChangeEvent} from '@ngx-translate/core';

declare var $: any;

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss'],
  host: {
    '(document:click)': 'onDocumentClick($event)',
  },
})
export class SupportComponent implements OnInit {
	faqDropdownClicked:boolean = false;
	brandDropdownClicked:boolean = false;
	isClicked:boolean = false;
	webURL:string

  	constructor(
  		public _menuService: MenuService,
	    private _eref: ElementRef,
	    private titleService: Title,
	    private meta: Meta,
	    public translate: TranslateService,
	    @Inject(PLATFORM_ID) private platformId: Object
  		) {
  		translate.get('support_title').subscribe((res: string) => {
	        this.titleService.setTitle(res);
	    });
	    this.meta.addTags([
	      {name: 'description', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
	      {name: 'copyright', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
	      {name: 'keywords', content: 'quà tặng doanh nghiệp , quà tặng đối tác , voucher quà tặng , quà tặng khách hàng , quà tặng Tết, quà tặng sự kiện , tặng thưởng nhân viên , quà tặng doanh nhân , egift, quà tặng online, tặng quà trực tuyến, tặng quà online , gift voucher , phiếu quà tặng , quà tặng trực tuyến ,thẻ quà tặng , tặng voucher ,tặng quà online , quà online , bán gift card , phiếu tặng quà , giftcards , mua phiếu quà tặng'}
	    ]);
  	}

  	ngOnInit() {
  		if (isPlatformBrowser(this.platformId)){
	      	var w_width = $(window).width();
		    var w_height = $(window).height();
		    var footer_height = $('footer').height();
		    $('.accordion-section').css({'min-height':(w_height - footer_height - 60)+'px'});
	    }
	  	
	    this.webURL= environment.origin;
	  	this._menuService.show();
	    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
	      this.translate.get('support_title').subscribe((res: string) => {
	        this.titleService.setTitle(res);
	      });
	    });
  	}
  	toggleClick(event):void{
	    this.isClicked = !this.isClicked;
	    var target = event.currentTarget;
	    var idAttr = target.attributes.id.nodeValue;
	    if(idAttr == 'tab_all_faq'){
	      this.faqDropdownClicked = !this.faqDropdownClicked;
	    }
	    if(idAttr == 'tab_all_brand'){
	      this.brandDropdownClicked = !this.brandDropdownClicked;
	    }
  	}

  	elementClicked(event){
    	event.target.closest(".wrapper-dropdown").querySelector('small').innerHTML = event.target.innerText;
  	}

  	onDocumentClick(event) {
	    // console.log(event);
	    if (!event.target.closest(".wrapper-dropdown") && !event.target.closest(".wrapper-dropdown")) { 
	        this.brandDropdownClicked = false;
	        this.faqDropdownClicked = false;
	    }
  	}

}
