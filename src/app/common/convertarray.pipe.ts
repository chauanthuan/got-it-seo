import {Pipe,PipeTransform} from "@angular/core";

@Pipe({name: 'convertArray'})
export class ConvertArrayPipe implements PipeTransform {
  transform(value, args:string[]) : any {
    if (!value) {
      return value;
    }

    let keys = [];
    for (let key in value) {
      keys.push({key: key, value: value[key]});
    }
    return keys;
  }
}
