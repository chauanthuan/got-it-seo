import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'chunks'
})
export class ChunkPipe implements PipeTransform {
  transform(arr: any, chunkSize: number) {
    if(typeof arr != 'undefined'){
        return arr.reduce((prev, cur, index) => (index % chunkSize) ? prev : prev.concat([arr.slice(index, index + chunkSize)]), []);
    }
    return false;
  }
}