import { Component, Inject, PLATFORM_ID  } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  	title = 'Got It';
   	constructor(public translate: TranslateService, @Inject(PLATFORM_ID) private platformId: Object) {
      translate.addLangs(["en", "vi"]);
      if (isPlatformBrowser(this.platformId)){
        let browserLang = translate.getBrowserLang();
        if (localStorage.getItem("CurrentLanguage") === null) {
          localStorage.setItem("CurrentLanguage",(browserLang == 'vi' ? 'vi' : 'en'));
        }
        translate.setDefaultLang(localStorage.getItem("CurrentLanguage"));
        translate.use(localStorage.getItem("CurrentLanguage"));
      }
    }
}
