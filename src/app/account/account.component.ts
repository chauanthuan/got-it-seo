import { Component, OnInit ,ViewChildren, QueryList, ViewChild ,ElementRef, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { SelectComponent } from "ng2-select/ng2-select";
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import {Router} from '@angular/router';
import {MenuService} from '../menu/menu.service';
import { TranslateService , LangChangeEvent} from '@ngx-translate/core';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { Title, Meta }     from '@angular/platform-browser';

declare var $: any;
@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class AccountComponent implements OnInit {

	gender: string = 'm' ;
  selectedDay_val:any;
  selectedMonth_val:any;
  selectedYear_val:any;
  Daynumber:number = 31;
  phonenumber: string;
  email:string;
  month: string;
  year:string;
  day:string;
  lastname:string;
  firstname:string;
  db:any;
  messageSuccess: string;
  // year_items:any;
  // day_item:any;
  public value:any = {};
  public _disabledV:string = '0';
  public disabled:boolean = false;
  user: Observable<firebase.User>;
  private userDetails: firebase.User = null;
  nowYear = (new Date()).getFullYear();
  isLoading:boolean = false;
  user_uid:string;
  userData: any;
   user_firebase:any;
  public month_items = [];
  public month_items_vi = [{id:'01',text:'Tháng 1'},{id:'02',text:'Tháng 2'}, {id:'03',text:'Tháng 3'},{id:'04',text:'Tháng 4'},
  {id:'05',text:'Tháng 5'},{id:'06',text:'Tháng 6'},{id:'07',text:'Tháng 7'},{id:'08',text: 'Tháng 8'},{id:'09',text: 'Tháng 9'},
  {id:'10',text: 'Tháng 10'},{id:'11',text:'Tháng 11'},{id:'12',text: 'Tháng 12'}]; 

  public month_items_en = [{id:'01',text:'January'},{id:'02',text:'February'}, {id:'03',text:'March'},{id:'04',text:'April'},
  {id:'05',text:'May'},{id:'06',text:'June'},{id:'07',text:'July'},{id:'08',text: 'August'},{id:'09',text: 'September'},{id:'10',text: 'October'},
  {id:'11',text:'November'},{id:'12',text: 'December'}]; 

  public day_items = [];
  public year_items = [];
 
  

  @ViewChildren(SelectComponent) selectElements: QueryList<SelectComponent>
  @ViewChild('selectMonth') selectMonth;
  @ViewChild('selectDay') selectDay;
  @ViewChild('selectYear') selectYear;
  constructor(private afAuth: AngularFireAuth,
   private router: Router,
   public _menuService: MenuService,
   public translate: TranslateService, 
   private titleService: Title,
   private meta: Meta,
   @Inject(PLATFORM_ID) private platformId: Object
     ){
    // this.afAuth.auth.onAuthStateChanged(auth=>{
    //   if(!auth){
    //     this.router.navigate(['/']); 
    //   }
    //   else{
    //     this.user_uid = auth.uid;
    //   }
    // });
    this.db = firebase.firestore();
    translate.get('account_title').subscribe((res: string) => {
        this.titleService.setTitle(res);
    });
    this.meta.addTags([
      {name: 'description', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
      {name: 'copyright', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
      {name: 'keywords', content: 'quà tặng doanh nghiệp , quà tặng đối tác , voucher quà tặng , quà tặng khách hàng , quà tặng Tết, quà tặng sự kiện , tặng thưởng nhân viên , quà tặng doanh nhân , egift, quà tặng online, tặng quà trực tuyến, tặng quà online , gift voucher , phiếu quà tặng , quà tặng trực tuyến ,thẻ quà tặng , tặng voucher ,tặng quà online , quà online , bán gift card , phiếu tặng quà , giftcards , mua phiếu quà tặng'}
    ]);
  }

  ngOnInit() {
     if (isPlatformBrowser(this.platformId)){
    window.scrollTo(0, 0);
  }
    this._menuService.show();
    this.user_uid = firebase.auth().currentUser.uid;
    if(this.translate.currentLang == "vi"){
      this.month_items = this.month_items_vi;
    }
    else{
      this.month_items = this.month_items_en;
     
    }
    this.selectMonth.active=[];
    this.selectDay.active=[];
    this.selectYear.active=[];

    this.db.collection('user').doc(this.user_uid).get().then((snapshot)=>{
      if(snapshot.exists){
        this.user_firebase = snapshot.data();
        this.firstname = this.user_firebase.fname;
        this.lastname = this.user_firebase.lname;
        this.email = firebase.auth().currentUser.email;
        this.phonenumber = this.user_firebase.phone;
        this.gender = this.user_firebase.gender ? this.user_firebase.gender : 'M';
    
          let birthday = this.user_firebase.birthday ? this.user_firebase.birthday : '';
          if(birthday){
            var splitted = birthday.split("-");
            this.selectedYear_val = splitted[0];
            this.selectedMonth_val = splitted[1];
            this.selectedDay_val = splitted[2];   
            this.selectMonth.active.push(this.month_items[Number(splitted[1]) - 1]);
            this.selectDay.active.push(this.day_items[Number(splitted[2]) - 1]);
            this.selectYear.active.push(this.year_items[Number(this.nowYear) - Number(splitted[0])]);
          }
      }
      

    });

    
    this.generateday(this.Daynumber);
    this.year_items = [];
    for(let i= this.nowYear ; i>=Number(this.nowYear - 100); i--)
    {
      this.year_items.push({id:String(i),text:String(i)});
    }

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      console.log(123);
      if(this.translate.currentLang == 'vi'){
         this.month_items = this.month_items_vi;
      }
      else{
         this.month_items = this.month_items_en;
      } 
      if(this.user_firebase){
        let birthday = this.user_firebase.birthday ? this.user_firebase.birthday : '';
        if(birthday){
          this.selectMonth.active=[];
          this.selectDay.active=[];
          this.selectYear.active=[];
          var splitted = birthday.split("-");
          this.selectedYear_val = splitted[0];
          this.selectedMonth_val = splitted[1];
          this.selectedDay_val = splitted[2];
          this.selectMonth.active.push(this.month_items[Number(splitted[1]) - 1]);
          this.selectDay.active.push(this.day_items[Number(splitted[2]) - 1]);
          this.selectYear.active.push(this.year_items[Number(this.nowYear) - Number(splitted[0])]);
        }
      }

      this.translate.get('account_title').subscribe((res: string) => {
        this.titleService.setTitle(res);
      });
    });

  }


  ngAfterViewInit(){
    if (isPlatformBrowser(this.platformId)){
      var w_width = $(window).width();
      var w_height = $(window).height();
      var footer_height = $('footer').height();
      $('.accountsetting-section .parent').css({'min-height':(w_height - footer_height - 60)+'px'})
      var content_width = $('.accountsetting-section .parent').width();
      var path_right_width = (w_width - content_width)/2;
      $('.left-fixed-wrap').css({'width':path_right_width});
    }
  }

  onResize(event){
     if (isPlatformBrowser(this.platformId)){
      var w_width = $(window).width();
      var w_height = $(window).height();
      var footer_height = $('footer').height();
      $('.accountsetting-section .parent').css({'min-height':(w_height - footer_height - 60)+'px'});

      var content_width = $('.accountsetting-section .parent').width();
      if(w_width >= content_width){
        var path_right_width = (w_width - content_width)/2;
      }
      else{
        path_right_width = 0;
      }
      $('.left-fixed-wrap').css({'width':path_right_width});
    }
  }
  

  updateProfile(){
    this.isLoading = true;
     var updates = {};
      updates['fname'] = this.firstname;
      updates['lname'] = this.lastname;
      updates['gender'] = this.gender;

      if(this.phonenumber){
        let account_phone_dt = '';
        if(this.phonenumber.indexOf('+84') != -1){
          account_phone_dt = this.phonenumber.replace('+84','0');
        }
        else{
          account_phone_dt = this.phonenumber;
        }
        updates['phone'] = account_phone_dt;
      }
      
      if( Number(this.selectedYear_val) && Number(this.selectedMonth_val) && Number(this.selectedDay_val)){
        updates['birthday'] =  Number(this.selectedYear_val)+'-'+Number(this.selectedMonth_val)+'-'+Number(this.selectedDay_val);
      }
      this.db.collection('user').doc(this.user_uid).update(updates).then(()=>{
          this.isLoading = false;
          if(this.translate.currentLang == 'vi')
          {
              this.messageSuccess = 'Thay đổi thành công.';
          }
           else
              this.messageSuccess = 'Change account successfully.';
             
          window.scrollTo(0, 0);
      }).catch((error) => {
              this.isLoading = false;
                console.log(error)
      });
  }


  onRadioChange(val) {
    this.gender = val;
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  public selectedDay(value:any) {
      this.selectedDay_val = value.id;
      }
  public selectedMonth(value:any) {
    this.selectedMonth_val = value.id;   
    if(this.selectedMonth_val == '1' || this.selectedMonth_val == '3'  || this.selectedMonth_val == '5'  ||  this.selectedMonth_val == '7'  || this.selectedMonth_val == '8'  || this.selectedMonth_val == '10'  || this.selectedMonth_val == '12' )
    {
      this.Daynumber = 31;    
    }
    else{
         if(this.selectedMonth_val == '2')
          {
            if(typeof this.selectedYear_val  != 'undefined' && this.selectedYear_val%4 == 0)
            {
               this.Daynumber = 29 ;

            }
            else
                this.Daynumber = 28 ;
          }
          else{
             this.Daynumber = 30 ;   
          }
      }
      
       this.generateday(this.Daynumber);
  }


  public selectedYear(value:any) {
    this.selectedYear_val = value.id ;
  }

  public removed(value:any) {
  console.log('Removed value is: ', value);
  }

  public typed(value:any) {
  console.log('New search input: ', value);
  }

  public refreshValue(value:any) {
  this.value = value;
  }


  public closeOtherSelects(element) {
    if (element.optionsOpened == true) {
      let elementsToclose= this.selectElements.filter(function (el: any) {
        return (el != element && el.optionsOpened == true)
      });
      elementsToclose.forEach(function (e: SelectComponent) {
        e.clickedOutside();
      })
    }
   }

  generateday(Ngay){
    this.day_items = [];
     for(let i = 1 ; i <= Ngay ;i++)
      {
         this.day_items.push({id:String(i),text:String(i)});
      }
  }

}
