import { Routes, RouterModule } from '@angular/router';

import { ListbrandComponent } from './listbrand/listbrand.component';
import { HomeComponent } from './home/home.component';
import { LandingComponent } from './landing/landing.component';
import { ProductComponent } from './product/product.component';
import { AboutComponent } from './about/about.component';
import { SupportComponent } from './support/support.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { OperatingregulationComponent } from './operatingregulation/operatingregulation.component';

import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { EmailVerifyComponent } from './email-verify/email-verify.component';
import { AccountComponent } from './account/account.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { PaymentWaitingComponent } from './payment-waiting/payment-waiting.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { GiftSentComponent } from './gift-sent/gift-sent.component';
import { GiftReceivedComponent } from './gift-received/gift-received.component';

import { AuthGuard } from './services/auth-guard.service';
const appRoutes: Routes = [
    { path: '', component: HomeComponent, pathMatch: 'full' },
    { path: 'about-egift',component: LandingComponent, pathMatch: 'full' },
    { path: 'send-egift',component: ProductComponent, pathMatch: 'full' },
    { path: 'about',component: AboutComponent, pathMatch: 'full' },
    { path: 'support',component: SupportComponent, pathMatch: 'full' },
    { path: 'terms',component: TermsComponent, pathMatch: 'full' },
    { path: 'privacypolicy',component: PrivacypolicyComponent, pathMatch: 'full' },
    { path: 'operatingregulation',component: OperatingregulationComponent, pathMatch: 'full' },
    { path: 'checkout',component: CheckoutComponent, pathMatch: 'full' },
    { path: 'forgotpassword', component: ForgotpasswordComponent, pathMatch: 'full' },
    { path: 'resetpassword', component: ResetpasswordComponent, pathMatch: 'full' },
    { path: 'email-verify', component: EmailVerifyComponent, pathMatch: 'full' },

    //
    { path: 'account', canActivate: [AuthGuard], component: AccountComponent, pathMatch: 'full' },
    { path: 'changepassword', canActivate: [AuthGuard], component: ChangepasswordComponent, pathMatch: 'full' },
    { path: 'payment-waiting', component: PaymentWaitingComponent, pathMatch: 'full' },
    { path: 'signup', component: SignupComponent, pathMatch: 'full' },
    { path: 'login', component: LoginComponent, pathMatch: 'full' },
    { path: 'gift-sent', canActivate: [AuthGuard], component: GiftSentComponent, pathMatch: 'full' },
    { path: 'login', component: LoginComponent, pathMatch: 'full' },
    { path: 'gift-received', canActivate: [AuthGuard], component: GiftReceivedComponent, pathMatch: 'full' },
];

export const AppRoutes = RouterModule.forRoot(appRoutes);