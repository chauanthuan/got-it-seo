import { Component, OnInit, ViewChild, ElementRef,Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import {Router} from '@angular/router';
import {MenuService} from '../menu/menu.service';
import {TranslateService,LangChangeEvent} from '@ngx-translate/core';
import { Title, Meta}     from '@angular/platform-browser';

declare var $: any;
@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.scss'],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class ChangepasswordComponent implements OnInit {
	@ViewChild('changepassword') changepassword: ElementRef;

	confirmpassword:string;
	password:string;
	newpassword:string;
	confirmed:boolean = false;
	isLoading: boolean = false;
	user_uid:string;
	userData: any = null;
	errorCurrentPass: string;
	messageSuccess: string;
	user: Observable<firebase.User>;
	private userDetails: firebase.User = null;
	height_content:any;
	userProviderLogin: any;
	textProiverLogin:any;
	constructor(private afAuth: AngularFireAuth,
		private router: Router,
		public _menuService: MenuService,
		public translate: TranslateService,
		private titleService: Title,
		private meta: Meta,
		  @Inject(PLATFORM_ID) private platformId: Object
		 ) { 
		// this.afAuth.auth.onAuthStateChanged(auth=>{
	 //      if(!auth){
	 //        this.router.navigate(['/']); 
	 //      }
	 //      else{
	 //      	this.userDetails = auth;
	 //      }
	 //    });
	 translate.get('changepass_title').subscribe((res: string) => {
        this.titleService.setTitle(res);
    });
    this.meta.addTags([
      {name: 'description', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
      {name: 'copyright', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
      {name: 'keywords', content: 'quà tặng doanh nghiệp , quà tặng đối tác , voucher quà tặng , quà tặng khách hàng , quà tặng Tết, quà tặng sự kiện , tặng thưởng nhân viên , quà tặng doanh nhân , egift, quà tặng online, tặng quà trực tuyến, tặng quà online , gift voucher , phiếu quà tặng , quà tặng trực tuyến ,thẻ quà tặng , tặng voucher ,tặng quà online , quà online , bán gift card , phiếu tặng quà , giftcards , mua phiếu quà tặng'}
    ]);

	}

	ngOnInit() {
	if (isPlatformBrowser(this.platformId)){
		window.scrollTo(0, 0);
	}
		this._menuService.show();
		 this.afAuth.authState.subscribe((user)=>{
		
		 	if(user){
		 		user.providerData.forEach((item)=>{
		 			this.userProviderLogin = item.providerId;
		 			if(this.userProviderLogin == 'password'){
		 				this.userProviderLogin = item.providerId;
		 			}else if(this.userProviderLogin == 'facebook.com' ){
		 						this.textProiverLogin = "Facebook ";	
		 			}else{
		 				this.textProiverLogin = "Google ";

		 			} 
		 		})
		 	}	 		
		})	
		// this.user_uid = JSON.parse(localStorage.getItem('currentUser'));
	}
	ngAfterViewInit(){
	    if (isPlatformBrowser(this.platformId)){	
		var w_width = $(window).width();
		var w_height = $(window).height();
  		var footer_height = $('footer').height();
  		$('.changepassword-section .parent').css({'min-height':(w_height - footer_height - 60)+'px'});
  		var content_width = $('.changepassword-section .parent').width();
		var path_right_width = (w_width - content_width)/2;
		$('.left-fixed-wrap').css({'width':path_right_width});
		}
	}

	onResize(event){
		if (isPlatformBrowser(this.platformId)){
	    var w_width = $(window).width();
	    var w_height = $(window).height();
	    var footer_height = $('footer').height();
	    $('.changepassword-section .parent').css({'min-height':(w_height - footer_height - 60)+'px'});

	    var content_width = $('.changepassword-section .parent').width();
	    if(w_width >= content_width){
	      var path_right_width = (w_width - content_width)/2;
	    }
	    else{
	      path_right_width = 0;
	    }
	    $('.left-fixed-wrap').css({'width':path_right_width});
		}
	}
	
	confirmPassType(event:any) { // without type info
	    var target = event.currentTarget;
	    var idAttr = target.attributes.id.nodeValue;
	    if(idAttr == 'newpassword'){
	      this.newpassword = event.target.value;
	    }
	    else{
	      this.confirmpassword = event.target.value;
	    }
	    
	    if(this.confirmpassword == this.newpassword ){
	      this.confirmed = true;
	    }
	    else{
	      this.confirmed = false;
	    }
	    console.log(this.confirmed);
	}
	changePassword(){
		this.isLoading = true;
		firebase.auth().currentUser
			.reauthenticateWithCredential(firebase.auth.EmailAuthProvider.credential(firebase.auth().currentUser.email, this.password))
			.then(()=>{
				firebase.auth().currentUser.updatePassword(this.newpassword).then(() => {
	        	// Update successful. Update password to database again
	        	if(this.translate.currentLang == 'vi')
	        	{
	        		this.messageSuccess = 'Đổi mật khẩu thành công . Hệ thống sẽ tự động đăng xuất và chuyển hướng sau 3s.'
	        	}else{
	        		this.messageSuccess = 'Change password successfully. We will auto log out and redirect after 3s.';
	        	}
	        	
	        		this.isLoading = false;
					setTimeout(()=>{ 
					      	this.router.navigate(['/']);
				        	this.afAuth.auth.signOut();
							// localStorage.removeItem('currentUser');
					 },3000);
			        
		        }).catch((error) => {
		        	this.isLoading = false;
		          	if(error.code == 'auth/requires-recent-login'){
		          		this.errorCurrentPass = error.message;
		          	}
		        });
			}).catch((error) => {
				this.isLoading = false;
	          	if(error.code == 'auth/requires-recent-login'){
	          		this.errorCurrentPass = error.message;
	          	}
	          	if(error.code == 'auth/wrong-password'){
	          		if(this.translate.currentLang == 'vi'){
	          			this.errorCurrentPass = "Mật khẩu cũ không chính xác "
	          		}else{
	          			this.errorCurrentPass = "Old Password Incorrect ";
	          		}
	          		
	          	}
	        });
	}

}
