import { Component, OnInit } from '@angular/core';
import {Http, Headers, Response, RequestOptions,URLSearchParams} from '@angular/http';
import { Router } from '@angular/router';
import {TranslateService,LangChangeEvent} from '@ngx-translate/core';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-listbrand',
  templateUrl: './listbrand.component.html',
  styleUrls: ['./listbrand.component.scss'],
  host: {
    '(document:click)': 'onDocumentClick($event)',
  },
})
export class ListbrandComponent implements OnInit {
	brandDropdownClicked:boolean = false;
	isShowMoreClicked:boolean = false;
	listOurBrand: any;
	listTempOurBrand: any;
	category_id: number = 0;
	currentUrl:string = '';
	chunkSize: number = 7;
	rowDisplay: number = 3; //row of brand
	private productIdLoadData = environment.defaultProductId;
	dataBrandByProduct: any;
	listBrandTemp:any;
	private apiUrlLoadBrand = 'brand/';

  	constructor(
	  	private _http: Http,
		private router:Router,
	  	public translate: TranslateService
  	) { }

	ngOnInit() {
		this.getBrandData(this.category_id).then(dataBrand=>{
			this.listOurBrand = dataBrand;
			this.listTempOurBrand = dataBrand;
		});
		
		this.currentUrl = this.router.url;
		if(this.currentUrl == '/send-egift'){
			this.chunkSize = 5;

			//call product detail data to filter brand
			this.dataBrandByProduct = this.getProductData();
			this.dataBrandByProduct.then(data=>{
				let brandArr = [];
				this.listBrandTemp = [];
				for(var i in data.storeList){
					if(brandArr.indexOf(Number(data.storeList[i].brand.brandId)) == -1){
						brandArr.push(Number(data.storeList[i].brand.brandId));
					}
				}
				let filteredData = [];
				filteredData = this.listOurBrand.filter(item => brandArr.indexOf(item.brandId) > -1);
				
				this.listOurBrand = filteredData;
				this.listTempOurBrand = filteredData;
			 });
		}
	}

	getProductData(): Promise<any> {
	    let url = environment.firebaseFnsUrl+'/app/product/detail?product_id='+this.productIdLoadData+'&language='+this.translate.currentLang;
	    return this._http.get(url).toPromise().then(response => response.json());
	  }

	getBrandData(category_id): Promise<any>{
  		let url = environment.firebaseFnsUrl+'/app/brand/getAll';
  		return this._http.get(url).toPromise().then(response=>response.json());
  	}

	toggleClick(event):void{
		var target = event.currentTarget;
		var idAttr = target.attributes.id.nodeValue;

		if(idAttr == 'tab_all_brand'){
			this.brandDropdownClicked = !this.brandDropdownClicked;
		}
	}

	toggleShowmoreClick(event):void{
		this.isShowMoreClicked = !this.isShowMoreClicked;
	}

	elementClicked(event){
		this.isShowMoreClicked = false;
		this.category_id = event.currentTarget.attributes['data-id'].nodeValue;
		this.listTempOurBrand = this.listOurBrand.filter(
		brand => {
			if(this.category_id != 0) {
				// for (var i = 0; i < brand['categoryID'].length; i++) {
					return brand['categoryID'] == this.category_id;
				// };
			}
			else{
				return brand;
			}
		});
		event.target.closest(".wrapper-dropdown").querySelector('small').innerHTML = event.target.innerText;
	}

	onDocumentClick(event) {
		if (!event.target.closest(".wrapper-dropdown")) {
			this.brandDropdownClicked = false;
		}
	}

}
