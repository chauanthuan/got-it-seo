import { Component, OnInit, ElementRef,ViewChild, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {Router, ActivatedRoute} from '@angular/router';
import { AuthService } from '../services/auth.service';
import {ModalDirective} from 'ngx-bootstrap/modal';
import { Title, Meta }     from '@angular/platform-browser';
import {TranslateService,LangChangeEvent} from '@ngx-translate/core';
declare var bootbox:any;
declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	@ViewChild('staticModal') public staticModal: ModalDirective;
  	@ViewChild('passwordRequiredInput') focusInput:ElementRef;
	@ViewChild('input1') focus1:ElementRef;
	private userDetails: firebase.User = null;
	user: Observable<firebase.User>;
	provider1 = new firebase.auth.GoogleAuthProvider;
  	provider2 = new firebase.auth.FacebookAuthProvider;
	email:string;
	password:string = null;
	loginPage:boolean = true;
	isLoading:boolean = false;
	errorLogin:string;
	passwordRequired: string;

	returnUrl: string;

 	constructor(
 		private afAuth: AngularFireAuth, 
 		private route: ActivatedRoute, 
 		private router: Router,
 		private authService: AuthService,
 		private titleService: Title,
 		private meta: Meta,
 		public translate: TranslateService,
  		@Inject(PLATFORM_ID) private platformId: Object
  	) {
 		if  ( this.authService.isLoggedIn() ) {
	      	this.router.navigate(['/']);
	    }
	    translate.get('login_title').subscribe((res: string) => {
	        this.titleService.setTitle(res);
	    });
	    this.meta.addTags([
	      {name: 'description', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
	      {name: 'copyright', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
	      {name: 'keywords', content: 'quà tặng doanh nghiệp , quà tặng đối tác , voucher quà tặng , quà tặng khách hàng , quà tặng Tết, quà tặng sự kiện , tặng thưởng nhân viên , quà tặng doanh nhân , egift, quà tặng online, tặng quà trực tuyến, tặng quà online , gift voucher , phiếu quà tặng , quà tặng trực tuyến ,thẻ quà tặng , tặng voucher ,tặng quà online , quà online , bán gift card , phiếu tặng quà , giftcards , mua phiếu quà tặng'}
	    ]);
  	}

	ngOnInit() {
		if (isPlatformBrowser(this.platformId)){
			window.scrollTo(0, 0)
			this.password = null ;
			let body = document.getElementsByTagName('body')[0];
			let footer = document.getElementsByTagName('footer')[0];
	    	body.classList.add('login');
	    	footer.classList.add('hide');
	    	body.click();
	    }
		this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

		this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
	      this.translate.get('login_title').subscribe((res: string) => {
	        this.titleService.setTitle(res);
	      });
	    });
	}
	loginRegular() {
  		this.isLoading = true;
      	this.authService.loginRegular(this.email, this.password)
        .then((authData) => {
        	this.isLoading = false;
        	if(authData.emailVerified){
				// localStorage.setItem('currentUser', JSON.stringify(authData.uid));
     			this.router.navigateByUrl(this.returnUrl);
        	}
        	else{
        		this.errorLogin = 'Please verify your email address before login and try again.';
        		return; 	
        	} 
			
     		//this.router.navigate(['/']);
        })
        .catch((error) => {
          this.isLoading = false;
          if( error.code == 'auth/wrong-password' || error.code == 'auth/user-not-found'){
            if(this.translate.currentLang == 'vi'){
              this.errorLogin = 'Tên đăng nhập hoặc mật khẩu của bạn không chính xác';
            }
            else{
              this.errorLogin = 'Your username or password is incorrect.';
            }
          }         
          else if(error.code == 'auth/cancelled-popup-request' || error.code == 'auth/popup-closed-by-user'){
            this.errorLogin = '';
          } 
          else if(error.code == 'auth/network-request-failed'){
            if(this.translate.currentLang == 'vi'){
              this.errorLogin = 'Lỗi mạng. Vui lòng thử lại sau.';
            }
            else{
              this.errorLogin = 'A network error. Please try again later.';
            }
          }
          else{
            this.errorLogin = error.message;
          }  
        });
    }


  	signInWithFacebook() {
  		this.isLoading = true;
		this.authService.signInWithFacebook()
		.then((authData) => { 
			var userProfile = authData.additionalUserInfo.profile;
			var db = firebase.firestore();    
			let userData = authData.user; 

			let birthday = '';
			console.log(userProfile);
			if(userProfile.birthday){
				var splitted = userProfile.birthday.split("/");//mm/dd/yyyy
				birthday = splitted[2]  + '-' + splitted[0]+ '-' + splitted[1];//to yyyy-mm-dd
			}
			db.collection("user").doc(userData.uid).get().then((response)=>{
				if(!response || !response.exists){
				  db.collection("user").doc(userData.uid).set({
				    phone: userData.phoneNumber,
				    fname: userProfile.first_name,
				    lname:userProfile.last_name,
				    gender: (userProfile.gender == 'male') ? 'M':'F',
				    birthday:birthday
				  }).then(() => {
				      console.log("Document successfully written!");
				  })
				  .catch((error) => {
				      console.error("Error writing document: ", error);
				  });
				}
				else{
				  db.collection("user").doc(userData.uid).update({
				    phone: response.data().phone ? response.data().phone : userData.phoneNumber,
				    fname: response.data().fname ? response.data().fname :  userProfile.first_name,
				    lname: response.data().lname ? response.data().lname : userProfile.last_name,
				     gender: response.data().gender ? response.data().gender : (userProfile.gender == 'male') ? 'M':'F',
				    birthday:response.data().birthday ? response.data().birthday :  birthday
				  }).then(() => {
				      console.log("Document successfully update!");
				  })
				  .catch((error) => {
				      console.error("Error update document: ", error);
				  });
				}
			}).catch((error)=>{
			console.log(error);
			})
			this.isLoading = false;
			// localStorage.setItem('currentUser', JSON.stringify(authData.user.uid));
			this.router.navigateByUrl(this.returnUrl);
			//this.router.navigate(['/']);
		})
		.catch((error) => {
			if(error.code === 'auth/account-exists-with-different-credential' )
			{
			  var pendingCred = error.credential;
			  // The provider account's email address.
			  var email = error.email;
			  firebase.auth().fetchProvidersForEmail(email).then((providers)=>{
			    console.log(providers);
			    if (providers[0] === 'password') {
			      this.passwordRequired = '';
			      //this.staticModal.show();
			      

			      	let title_box = '';
		            let message_box = '';
		            if(this.translate.currentLang == 'vi'){
		              title_box = "Nhập mật khẩu";
		              message_box = "Email trong FB này đã được đăng kí với hệ thống bằng phương thức truyền thống (email và mật khẩu). Vui lòng nhập mật khẩu của tài khoản này.";
		            }
		            else{
		              title_box = "Enter your password";
		              message_box = "An account already exists with the same email address but different sign-in credentials. Please enter your password with this email address.";
		            }


		            bootbox.prompt({
		              title: title_box,
		              inputType: 'password',
		              buttons: {
		                cancel: {
		                  label: this.translate.currentLang == 'vi' ? 'Đóng' : 'Cancel',
		                  className: 'btn-default',
		                },
		                confirm: {
		                  label: this.translate.currentLang == 'vi' ? 'Xác nhận' : 'Confirm',
		                  className: 'btn-success',
		                }
		              },
		              callback:  (password) => {
		                  if(password === null){
		                    console.log('click Close btm')
		                  }
		                  else{
		                    if(password != ''){
		                     this.authService.loginRegular(email, password )
		                      .then((authData) => {
		                        console.log(authData);
		                        return authData.linkWithCredential(pendingCred).then(()=>{
		                          this.isLoading = false
		                          this.router.navigateByUrl(this.returnUrl);
		                          $('.bootbox').modal('hide');
		                        })
		                        
		                      })
		                      .catch((e) => {
		                        console.log(e);
		                        if(e.code == 'auth/wrong-password'){
		                          if(this.translate.currentLang == 'vi'){
		                            this.errorLogin = 'Mật khẩu của bạn không chính xác';
		                          }
		                          else{
		                            this.errorLogin = 'Your password is incorrect.';
		                          }
		                        }
		                        else{
		                          this.errorLogin = e.message;
		                        }
		                        
		                        this.isLoading = false;
		                        $('.bootbox-input').val('');
		                        $('.bootbox-body').find('.err').html('').prepend('<p class="err" style="color:#ff5f5f;">'+this.errorLogin+'</p>');

		                        return false;
		                      });
		                    return false;
		                   }
		                  else{
		                   if(this.translate.currentLang == 'vi'){
		                      this.errorLogin = 'Vui lòng nhập mật khẩu.';
		                    }
		                    else{
		                      this.errorLogin = 'Please enter your password.';
		                    }
		                    $('.bootbox-body').find('.err').html('').prepend('<p class="err" style="color:#ff5f5f;">'+this.errorLogin+'</p>');

		                    return false;
		                 }
		               }
		              }
		          }).find('.bootbox-body').prepend('<p>'+message_box+'</p><div class="err"></div>');
			    }
			    else if(providers[0] == 'google.com'){
			      firebase.auth().signInWithPopup(new firebase.auth.GoogleAuthProvider())
			      .then((result) => {
			          result.user.linkWithCredential(error.credential)
			          .catch((error)=>{
			              this.errorLogin = error.message;
			          });
			          // localStorage.setItem('currentUser', JSON.stringify(result.user.uid));
			          this.router.navigate(['/']);
			      });
			    }
			  });
			}
			else {
			  	this.isLoading = false;
		        this.errorLogin = error.message;
		        if(error.code == 'auth/cancelled-popup-request' || error.code == 'auth/popup-closed-by-user'){
		          this.errorLogin = '';
		        }
		        if(error.code == 'auth/network-request-failed'){
		          if(this.translate.currentLang == 'vi'){
		            this.errorLogin = 'Lỗi mạng. Vui lòng thử lại sau.';
		          }
		          else{
		            this.errorLogin = 'A network error. Please try again later.';
		          }
		        }
			}
		});
    }

  	signInWithGoogle(){
  		this.isLoading = true;
  		
  		this.authService.signInWithGoogle()
		.then((authData) => { 
			console.log(authData);
			var userProfile = authData.additionalUserInfo.profile;
			var db = firebase.firestore();    
			let userData = authData.user; 
			let birthday = '';
			if(userProfile.birthday){
				var splitted = userProfile.birthday.split("/");
				birthday = splitted[2] + '-' + splitted[1] + '-' + splitted[0];
			}
			db.collection("user").doc(userData.uid).get().then((response)=>{
			if(!response || !response.exists){
			  db.collection("user").doc(userData.uid).set({
			      phone: userData.phoneNumber,
                  fname: userProfile.given_name,
                  lname:userProfile.family_name,
                  gender: (userProfile.gender == 'male') ? 'M':'F',
                  birthday:birthday
			  }).then(() => {
			      console.log("Document successfully written!");
			  })
			  .catch((error) => {
			      console.error("Error writing document: ", error);
			  });
			}
			else{
			  db.collection("user").doc(userData.uid).update({
			    phone: response.data().phone ? response.data().phone : userData.phoneNumber,
			    fname: response.data().fname ? response.data().fname :  userProfile.given_name,
			    lname: response.data().lname ? response.data().lname : userProfile.family_name,
			    gender: response.data().gender ? response.data().gender : (userProfile.gender == 'male') ? 'M':'F',
                birthday:response.data().birthday ? response.data().birthday :  birthday
			  }).then(() => {
			      console.log("Document successfully update!");
			  })
			  .catch((error) => {
			      console.error("Error update document: ", error);
			  });
			}
			}).catch((error)=>{
			console.log(error);
			})
		  	this.isLoading = false;    
			this.router.navigateByUrl(this.returnUrl);

		})
		.catch((error) => {
	        console.log(error);
	        this.isLoading = false;
	        
	        if(error.code == 'auth/cancelled-popup-request' || error.code == 'auth/popup-closed-by-user'){
	          this.errorLogin = '';
	        }
	        else if(error.code == 'auth/network-request-failed'){
	          if(this.translate.currentLang == 'vi'){
	            this.errorLogin = 'Lỗi mạng. Vui lòng thử lại sau.';
	          }
	          else{
	            this.errorLogin = 'A network error. Please try again later.';
	          }
	        }
	        else{
	          this.errorLogin = error.message;
	        }
		});
  	}

  	ngOnDestroy() {
  		if (isPlatformBrowser(this.platformId)){
		    let body = document.getElementsByTagName('body')[0];
		    let footer = document.getElementsByTagName('footer')[0];
		    body.classList.remove("login");
		    footer.classList.remove('hide');
		}
	}

}
