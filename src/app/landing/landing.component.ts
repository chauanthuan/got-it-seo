import { Component, OnInit,ViewChild, ElementRef, TemplateRef, Inject, PLATFORM_ID  } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { environment } from '../../environments/environment';
import * as firebase from 'firebase/app';
import { TranslateService , LangChangeEvent} from '@ngx-translate/core';
import { Title, Meta }     from '@angular/platform-browser';
import { MenuService } from "../menu/menu.service";
import { LandingService } from "./landing.service";
@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  host: {
    '(document:click)': 'onDocumentClick($event)',
  }
})
export class LandingComponent implements OnInit {
	isClicked:boolean = false;
	brandDropdownClicked:boolean = false;

	private imgPath: any;
	private priceID: any;
	private priceValue: any;
	public staticProductData: any;
	public listProductPrice: any;
	private productStaticId = environment.defaultProductId;
	private productStaticImg: any;
	private productStaticNm: any;

	constructor(
		public translate: TranslateService,
		private titleService: Title,
		private meta: Meta,
		@Inject(PLATFORM_ID) private platformId: Object,
		public _menuService: MenuService,
		public _landingService: LandingService
		) {
		translate.get('what_is_gotit_title').subscribe((res: string) => {
	        this.titleService.setTitle(res);
	    });
	    this.meta.addTags([
	      {name: 'description', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
	      {name: 'copyright', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
	      {name: 'keywords', content: 'got it , gotit ,voucher ,e voucher, egift, quà tặng doanh nghiệp , quà tặng đối tác , voucher quà tặng , quà tặng khách hàng , quà tặng Tết, quà tặng sự kiện , tặng thưởng nhân viên , quà tặng doanh nhân , egift, quà tặng online, tặng quà trực tuyến, tặng quà online , gift voucher , phiếu quà tặng , quà tặng trực tuyến ,thẻ quà tặng , tặng voucher ,tặng quà online , quà online , bán gift card , phiếu tặng quà , giftcards , mua phiếu quà tặng'}
	    ]);
	}

	ngOnInit() {
		if (isPlatformBrowser(this.platformId)){
			window.scrollTo(0, 0)
		}

		this._menuService.show();
  		var ref = firebase.database().ref("products/"+this.productStaticId);
		    ref.once("value").then((snapshot) => {
		      this.staticProductData = snapshot.val();
		      //set image path
		      this.imgPath = snapshot.val().image;
		      //set list price + value
		      this.listProductPrice = snapshot.val().prices;
		      //set product price and product value
		      this.priceID = Object.keys(this.listProductPrice)[0];    
		      this.priceValue = this.listProductPrice[this.priceID];
		      // console.log(priceID,priceValue);  

		      // this.priceValue = this.listProductPrice[Object.keys(this.listProductPrice)[0]];
	    });
	}

	toggleClick(event):void{
	    this.isClicked = !this.isClicked;
	    var target = event.currentTarget;
	    var idAttr = target.attributes.id.nodeValue;
	    if(idAttr == 'tab_all_brand'){
	      this.brandDropdownClicked = !this.brandDropdownClicked;
	    }
	}
	elementClicked(event){
		event.target.closest(".wrapper-dropdown").querySelector('small').innerHTML = event.target.innerText;
	}

  	onDocumentClick(event) {
    	if (!event.target.closest(".wrapper-dropdown")) { 
	        this.brandDropdownClicked = false;
    	}
  	}
  	chooseProduct(val,productID,priceID){
  		this._landingService.productID = productID;
	    this._landingService.priceID = priceID;
	    this._landingService.priceValue = val;
  	}

}
