import 'rxjs/add/operator/map';
import 'rxjs/add/operator/first';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { Router, CanActivate , ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import * as firebase from 'firebase/app';

import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class AuthGuard implements CanActivate {

    public

  constructor(private router: Router,
              private authService: AuthService,
              private afAuth: AngularFireAuth) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
  	return this.afAuth.authState.map((user)=>{
      if(user != null){
        return true;
      }else{
        this.router.navigate(['/login'],{ queryParams: { returnUrl: state.url }});
        return false;
      }
    }).first();
  }
}