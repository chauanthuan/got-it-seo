import { Component, OnInit,ViewChild, ElementRef, TemplateRef, Inject, PLATFORM_ID  } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

declare var $: any;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  host: {
    '(window:scroll)': 'onScroll($event)'
  }
})
export class FooterComponent implements OnInit {
	isMobile: boolean = false;
  constructor(@Inject(PLATFORM_ID) private platformId: Object) { }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)){
    	var ua = navigator.userAgent;
    
      if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/i.test(ua)){
        this.isMobile = true;
      }
      else{
        this.isMobile = false;
      }  
    }
  }

  backtop(){
    if (isPlatformBrowser(this.platformId)){
  	   $('html, body').animate({scrollTop: '0px'}, 500);
    }
  }
  onScroll(event){
  	if(this.isMobile){
       if (isPlatformBrowser(this.platformId)){
      		if($(window).scrollTop() >= $(window).height() + 150) {
    	  		$('.backtop').addClass('showscroll');
    	    }
    	    else{
    	    	$('.backtop').removeClass('showscroll');
    	    }
       }
  	}
  	
  }
}
