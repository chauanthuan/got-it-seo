import { Component, OnInit,ViewChild, ElementRef, TemplateRef, Inject, PLATFORM_ID  } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import {MenuService} from '../menu/menu.service';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import {Router, ActivatedRoute} from '@angular/router';
import { Title, Meta }     from '@angular/platform-browser';
import {TranslateService,LangChangeEvent} from '@ngx-translate/core';
import { environment } from '../../environments/environment';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {
  @ViewChild(NgForm) resetForm: NgForm;
  isBtnActive:boolean = false;
  email:string;
  error:string;
  messageSuccess:string;
  user: Observable<firebase.User>;
  confirmpassword:string;
  newpassword:string;
  confirmed:boolean = false;
  isLoading: boolean = false;
  resetCode: string;
  private userDetails: firebase.User = null;
  constructor(
    private activatedRoute: ActivatedRoute, 
    private router: Router,
    public _menuService: MenuService,
    private afAuth: AngularFireAuth,
    private titleService: Title,
    private meta: Meta,
    public translate: TranslateService,
    @Inject(PLATFORM_ID) private platformId: Object) { 
    this.afAuth.auth.onAuthStateChanged(auth=>{
      if(auth){
          this.router.navigate(['/']); 
        }
        // else{
        //   this.userDetails = auth;
        // }
      });
    translate.get('resetpass_title').subscribe((res: string) => {
        this.titleService.setTitle(res);
    });
    this.meta.addTags([
      {name: 'description', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
      {name: 'copyright', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
      {name: 'keywords', content: 'quà tặng doanh nghiệp , quà tặng đối tác , voucher quà tặng , quà tặng khách hàng , quà tặng Tết, quà tặng sự kiện , tặng thưởng nhân viên , quà tặng doanh nhân , egift, quà tặng online, tặng quà trực tuyến, tặng quà online , gift voucher , phiếu quà tặng , quà tặng trực tuyến ,thẻ quà tặng , tặng voucher ,tặng quà online , quà online , bán gift card , phiếu tặng quà , giftcards , mua phiếu quà tặng'}
    ]);
    this.activatedRoute.queryParams.subscribe(params => {
        this.resetCode = params['oobCode'];
    });
  }

  ngOnInit() {
  	if (isPlatformBrowser(this.platformId)){
  		window.scrollTo(0, 0)
		// console.log(firebase.auth());
		this._menuService.show();
		let body = document.getElementsByTagName('body')[0];

		if(!this.isBtnActive){
			body.classList.remove('out');
		}
		else{
			body.classList.add('out');
		}
		let footer = document.getElementsByTagName('footer')[0];
		body.classList.add('resetpassword');
		footer.classList.add('hide');
		body.click();
		this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
			this.translate.get('resetpass_title').subscribe((res: string) => {
				this.titleService.setTitle(res);
			});
		});
	}
  }
  confirmPassType(event:any) { // without type info
    if (isPlatformBrowser(this.platformId)){
	    var target = event.currentTarget;
	    var idAttr = target.attributes.id.nodeValue;
	    if(idAttr == 'newpassword'){
	      this.newpassword = event.target.value;
	    }
	    else{
	      this.confirmpassword = event.target.value;
	    }
	    
	    if(this.confirmpassword == this.newpassword ){
	      this.confirmed = true;
	    }
	    else{
	      this.confirmed = false;
	    }
	    console.log(this.confirmed);
     }

	}
  resetpass(){
  	this.isLoading = true;
  	firebase.auth().confirmPasswordReset(this.resetCode, this.newpassword).then(()=>{
  		this.isLoading = false;
  		
  		if(this.translate.currentLang == 'vi'){
			this.messageSuccess = 'Đặt lại mật khẩu thành công. Bây giờ bạn có thể đăng nhập với mật khẩu mới.';
		}
		else{
			this.messageSuccess = 'Password changed. You can now log in with your new password.';
		}
		this.resetForm.reset()
  	}).catch((err)=>{
  		this.isLoading = false;
  		if(err.code == 'auth/invalid-action-code'){
  			if(this.translate.currentLang == 'vi'){
				this.error = 'Liên kết đặt lại mật khẩu không đúng. Vui lòng kiểm tra lại.';
			}
			else{
				this.error = 'Reset password link not right. Please try again.';
			}
  		}
  		
  	})
  }

}
