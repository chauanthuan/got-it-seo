import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentWaitingComponent } from './payment-waiting.component';

describe('PaymentWaitingComponent', () => {
  let component: PaymentWaitingComponent;
  let fixture: ComponentFixture<PaymentWaitingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentWaitingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentWaitingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
