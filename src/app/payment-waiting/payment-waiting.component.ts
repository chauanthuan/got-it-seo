import { Component, OnInit ,  Inject, PLATFORM_ID} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
declare var $: any;
@Component({
  selector: 'app-payment-waiting',
  templateUrl: './payment-waiting.component.html',
  styleUrls: ['./payment-waiting.component.scss']
})
export class PaymentWaitingComponent implements OnInit {

  constructor(
	@Inject(PLATFORM_ID) private platformId: Object
  	) { }

  ngOnInit() {
  	if (isPlatformBrowser(this.platformId)){
	  	let footer = document.getElementsByTagName('footer')[0];
	    footer.classList.add('hide');
	    let menu = document.getElementsByTagName('nav')[0];
	    menu.classList.add('hide');

	  	var w_width = $(window).width();
	    var w_height = $(window).height();
	    var footer_height = $('footer').height();
	    $('.wrapper-content').css({'padding':'0'});
	    $('html, body').css({'min-width':'0'});
	    $('.payment-waiting').css({'min-height':(w_height)+'px'});
	}
  }

}
