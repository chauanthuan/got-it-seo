import { Component, OnInit , ElementRef,ViewChild, Inject, PLATFORM_ID} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {Router} from '@angular/router';
import { MenuService } from "../menu/menu.service";
import { AuthService } from '../services/auth.service';
import {ModalDirective} from 'ngx-bootstrap/modal';
import { NgForm } from '@angular/forms';
import { Title, Meta }     from '@angular/platform-browser';
import {TranslateService,LangChangeEvent} from '@ngx-translate/core';
import { environment } from '../../environments/environment';

declare var bootbox:any;
declare var $:any;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
	@ViewChild(NgForm) signupForm: NgForm;
	private userDetails: firebase.User = null;
	user: Observable<firebase.User>;
	public firstname:string;
	public lastname:string;
	public password:string;
	public email: string;
	public confirmpassword:string;
	public phone:string;
	public confirmed: boolean = false;
	isBtnActive:boolean = false;
	submitAttempt: boolean = false;
	isLoading:boolean = false;
	errorSignup: string;
	messageSuccess: string;
	provider1 = new firebase.auth.GoogleAuthProvider;
	provider2 = new firebase.auth.FacebookAuthProvider;
	provider3 = new firebase.auth.EmailAuthProvider;
	askPassWord:boolean = false;
	passwordRequired: string;
 	constructor(
      private afAuth: AngularFireAuth, 
      private router: Router, 
      public _menuService: MenuService,
      private authService: AuthService,
      public translate: TranslateService,
      private titleService: Title,
      private meta: Meta,
      @Inject(PLATFORM_ID) private platformId: Object
      ) {
      if( this.authService.isLoggedIn() ) {
      this.router.navigate(['/']);
    }

    translate.get('signup_title').subscribe((res: string) => {
      this.titleService.setTitle(res);
    });
    this.meta.addTags([
      {name: 'description', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
      {name: 'copyright', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
      {name: 'keywords', content: 'quà tặng doanh nghiệp , quà tặng đối tác , voucher quà tặng , quà tặng khách hàng , quà tặng Tết, quà tặng sự kiện , tặng thưởng nhân viên , quà tặng doanh nhân , egift, quà tặng online, tặng quà trực tuyến, tặng quà online , gift voucher , phiếu quà tặng , quà tặng trực tuyến ,thẻ quà tặng , tặng voucher ,tặng quà online , quà online , bán gift card , phiếu tặng quà , giftcards , mua phiếu quà tặng'}
    ]);
  }

	ngOnInit() {
		console.log(isPlatformBrowser(this.platformId));
		if (isPlatformBrowser(this.platformId)){
			window.scrollTo(0, 0)
			this._menuService.show();
			this.password = null ;
			let body = document.getElementsByTagName('body')[0];
			if(!this.isBtnActive){
				body.classList.remove('out');
			}
			else{
				body.classList.add('out');
			}
			let footer = document.getElementsByTagName('footer')[0];
			body.classList.add('signup');
			footer.classList.add('hide');
		    body.click();
		}
		this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
			this.translate.get('signup_title').subscribe((res: string) => {
				this.titleService.setTitle(res);
			});
		});
	}

	phoneNumberPress(event){
		if (isPlatformBrowser(this.platformId)){
		    const pattern = /[0-9\+\-\ ]/;

		    let inputChar = String.fromCharCode(event.charCode);
		    if (event.keyCode != 8 && !pattern.test(inputChar)) {
		      event.preventDefault();
		    }
		}
	}

	confirmPassType(event:any) { // without type info
	    var target = event.currentTarget;
	    var idAttr = target.attributes.id.nodeValue;
	    if(idAttr == 'password'){
	      this.password = event.target.value;
	    }
	    else{
	      this.confirmpassword = event.target.value;
	    }
	    
	    if(this.confirmpassword == this.password ){
	      this.confirmed = true;
	    }
	    else{
	      this.confirmed = false;
	    }
	    console.log(this.confirmed);
	}
  	signUpRegular(){
	    this.isLoading = true;
	    this.authService.signUpRegular(this.email, this.password)
	    .then((authData) => {
	      //save to firestore
	      var db = firebase.firestore();    
	      db.collection("user").doc(authData.uid).set({
	        fname: this.firstname,
	        lname: this.lastname,
	        phone: this.phone,
	      }).then(() => {
	          console.log("Document successfully written!");
	      })
	      .catch((error) => {
	          console.error("Error writing document: ", error);
	      });
	      
	      //update profile user.
	      firebase.auth().currentUser.updateProfile({
	        displayName: this.firstname + " " + this.lastname,
	        photoURL:''
	      }).then(() => {
	        console.log("Update Profile when SignUp success");
	      }).catch((error) => {
	        console.log("Update Profile when SignUp Error: "+ error);
	      });

	      authData.sendEmailVerification().then(() => {
	        if(this.translate.currentLang == 'vi'){
	          this.messageSuccess = 'Đăng ký thành công, vui lòng xác thực email trước khi đăng nhập.';
	        }
	        else{
	          this.messageSuccess = 'Register account successfully, please verify your email before login.';
	        }
	        this.signupForm.reset();
	      }).catch((error) => {
	        console.log(error)
	      });
	      //avoid auto login
	      firebase.auth().signOut();

	      this.isLoading = false;
	      
	      // localStorage.setItem('currentUser', JSON.stringify(authData.uid));
	      //this.router.navigate(['/']);

	    })
	    .catch((error) => {
	      this.isLoading = false;
	      if(error.code == 'auth/network-request-failed'){
	        if(this.translate.currentLang == "vi"){
	          this.errorSignup = "Lỗi mạng. Vui lòng thử lại sau";
	        }
	        else{
	          this.errorSignup = "A network error. Please try again later";
	        }
	      }
	      else if(error.code == 'auth/email-already-in-use'){
	        if(this.translate.currentLang == "vi"){
	          this.errorSignup = "Email này đã được đăng ký bởi tài khoản khác.";
	        }
	        else{
	          this.errorSignup = "The email address is already in use by another account.";
	        }
	      }
	      else{
	        this.errorSignup = error.message;
	      }
	      
	    });
	}
  
  signInWithFacebook() {
    this.isLoading = true;
    this.authService.signInWithFacebook()
    .then((authData) => { 
      console.log(authData)
      var userProfile = authData.additionalUserInfo.profile;
      console.log(userProfile);
      
      var db = firebase.firestore();    
      let userData = authData.user; 

      let birthday = '';
      if(userProfile.birthday){
        var splitted = userProfile.birthday.split("/");//mm/dd/yyyy
        birthday = splitted[2]  + '-' + splitted[0]+ '-' + splitted[1];//to yyyy-mm-dd
      }
      db.collection("user").doc(userData.uid).get().then((response)=>{
        if(!response || !response.exists){
          db.collection("user").doc(userData.uid).set({
            phone: userData.phoneNumber,
            fname: userProfile.first_name,
            lname:userProfile.last_name,
            gender: (userProfile.gender == 'male') ? 'M':'F',
            birthday:birthday
          }).then(() => {
              console.log("Document successfully written!");
          })
          .catch((error) => {
              console.error("Error writing document: ", error);
          });
        }
        else{
          db.collection("user").doc(userData.uid).update({
            phone: response.data().phone ? response.data().phone : userData.phoneNumber,
            fname: response.data().fname ? response.data().fname :  userProfile.first_name,
            lname: response.data().lname ? response.data().lname : userProfile.last_name,
            gender: response.data().gender ? response.data().gender : (userProfile.gender == 'male') ? 'M':'F',
            birthday:response.data().birthday ? response.data().birthday :  birthday
          }).then(() => {
              console.log("Document successfully update!");
          })
          .catch((error) => {
              console.error("Error update document: ", error);
          });
        }
      }).catch((error)=>{
        console.log(error);
      })

      // this.authService.getUserInfo(userData.uid).get().then(response=>{//check user logged in
      //   console.log(response.data());
      //   let birthday = '';
      //   if(userProfile.birthday){
      //     var splitted = userProfile.birthday.split("/");
      //     birthday = splitted[2] + '/' + splitted[1] + '/' + splitted[0];
      //   }
      //     if(!response || !response.exists){

      //         this.authService.getUserInfo(userData.uid).set({
      //             phone: userData.phoneNumber,
      //             picture: userData.photoURL,
      //             uid: userData.uid,
      //             email:userProfile.email,
      //             fname: userProfile.first_name,
      //             lname:userProfile.last_name,
      //             name: userProfile.name,
      //             gender: (userProfile.gender == 'male') ? 'M':'F',
      //             birthday:birthday
      //           }).then(() => {
      //               console.log("Document successfully written!");
      //           })
      //           .catch((error) => {
      //               console.error("Error writing document: ", error);
      //           });
      //         }
      //         else{
      //           this.authService.getUserInfo(userData.uid).update({
      //               phone: response.data().phone ? response.data().phone : userData.phoneNumber,
      //               uid: userData.uid,
      //               picture: userData.photoURL,
      //               email:userProfile.email,
      //               fname: response.data().fname ? response.data().fname :  userProfile.first_name,
      //               lname: response.data().lname ? response.data().lname : userProfile.last_name,
      //               name: response.data().name ? response.data().name : userProfile.name,
      //               gender: (userProfile.gender == 'male') ? 'M':'F',
      //               birthday:birthday
      //             }).then(() => {
      //                 console.log("Document successfully updated!");
      //             })
      //             .catch((error) => {
      //                 console.error("Error writing document: ", error);
      //             });
      //         }
        
      // });

      this.isLoading = false;
      // localStorage.setItem('currentUser', JSON.stringify(authData.user.uid));
      this.router.navigate(['/']);
    })
    .catch((error) => {
      console.log('vao errror');
      if(error.code === 'auth/account-exists-with-different-credential' )
      {
          var pendingCred = error.credential;
          // The provider account's email address.
          var email = error.email;
          firebase.auth().fetchProvidersForEmail(email).then((providers)=>{
            console.log(providers);
            if (providers[0] === 'password') {
              this.passwordRequired = '';
              this.staticModal.show();
              
              let title_box = '';
            let message_box = '';
            if(this.translate.currentLang == 'vi'){
              title_box = "Nhập mật khẩu";
              message_box = "Email trong FB này đã được đăng kí với hệ thống bằng phương thức truyền thống (email và mật khẩu). Vui lòng nhập mật khẩu của tài khoản này.";
            }
            else{
              title_box = "Enter your password";
              message_box = "An account already exists with the same email address but different sign-in credentials. Please enter your password with this email address.";
            }


            bootbox.prompt({
              title: title_box,
              inputType: 'password',
              buttons: {
                cancel: {
                  label: this.translate.currentLang == 'vi' ? 'Đóng' : 'Cancel',
                  className: 'btn-default',
                },
                confirm: {
                  label: this.translate.currentLang == 'vi' ? 'Xác nhận' : 'Confirm',
                  className: 'btn-success',
                }
              },
              callback:  (password) => {
                  if(password === null){
                    console.log('click Close btm')
                  }
                  else{
                    if(password != ''){
                     this.authService.loginRegular(email, password )
                      .then((authData) => {
                        console.log(authData);
                        return authData.linkWithCredential(pendingCred).then(()=>{
                          this.isLoading = false;
                          this.router.navigate(['/']);
                          $('.bootbox').modal('hide');
                        })
                        
                      })
                      .catch((e) => {
                        console.log(e);
                        if(e.code == 'auth/wrong-password'){
                          if(this.translate.currentLang == 'vi'){
                            this.errorSignup = 'Mật khẩu của bạn không chính xác';
                          }
                          else{
                            this.errorSignup = 'Your password is incorrect.';
                          }
                        }
                        else{
                          this.errorSignup = e.message;
                        }
                        
                        this.isLoading = false;
                        $('.bootbox-input').val('');
                        $('.bootbox-body').find('.err').html('').prepend('<p class="err" style="color:#ff5f5f;">'+this.errorSignup+'</p>');

                        return false;
                      });
                    return false;
                   }
                  else{
                   if(this.translate.currentLang == 'vi'){
                      this.errorSignup = 'Vui lòng nhập mật khẩu.';
                    }
                    else{
                      this.errorSignup = 'Please enter your password.';
                    }
                    $('.bootbox-body').find('.err').html('').prepend('<p class="err" style="color:#ff5f5f;">'+this.errorSignup+'</p>');

                    return false;
                 }
               }
              }
          }).find('.bootbox-body').prepend('<p>'+message_box+'</p><div class="err"></div>');
            }
            else if(providers[0] == 'google.com'){
              firebase.auth().signInWithPopup(new firebase.auth.GoogleAuthProvider())
              .then((result) => {
                  result.user.linkWithCredential(error.credential)
                  .catch((error)=>{
                      this.errorSignup = error.message;
                  });
                  this.router.navigate(['/']);
              });
            }
          });

        
      }
      else {
          this.isLoading = false;
          this.errorSignup = error.message;
      }
    });
  }
  
  signInWithGoogle(){
      this.isLoading = true;
      this.authService.signInWithGoogle()
      .then((authData) => { 
          console.log(authData);
          var userProfile = authData.additionalUserInfo.profile;
          var db = firebase.firestore();    
          let userData = authData.user; 

          let birthday = '';
          if(userProfile.birthday){
            var splitted = userProfile.birthday.split("/");
            birthday = splitted[2] + '-' + splitted[1] + '-' + splitted[0];
          }
          db.collection("user").doc(userData.uid).get().then((response)=>{
            if(!response || !response.exists){
              db.collection("user").doc(userData.uid).set({
                phone: userData.phoneNumber,
                fname: userProfile.given_name,
                lname:userProfile.family_name,
                gender: (userProfile.gender == 'male') ? 'M':'F',
                birthday:birthday
              }).then(() => {
                  console.log("Document successfully written!");
              })
              .catch((error) => {
                  console.error("Error writing document: ", error);
              });
            }
            else{
              db.collection("user").doc(userData.uid).update({
                phone: response.data().phone ? response.data().phone : userData.phoneNumber,
                fname: response.data().fname ? response.data().fname :  userProfile.given_name,
                lname: response.data().lname ? response.data().lname : userProfile.family_name,
                gender: response.data().gender ? response.data().gender : (userProfile.gender == 'male') ? 'M':'F',
                birthday:response.data().birthday ? response.data().birthday :  birthday
              }).then(() => {
                  console.log("Document successfully update!");
              })
              .catch((error) => {
                  console.error("Error update document: ", error);
              });
            }
          }).catch((error)=>{
            console.log(error);
          })
          this.isLoading = false;
          this.router.navigate(['/']);
      })
      .catch((error) => {
        this.isLoading = false;
            this.errorSignup = error.message;
      });
    }

	ngOnDestroy() {
		if (isPlatformBrowser(this.platformId)){
			let body = document.getElementsByTagName('body')[0];
			let footer = document.getElementsByTagName('footer')[0];
			body.classList.remove("signup");
			footer.classList.remove('hide');
		}
	}

   	// Activity Modal
	@ViewChild('staticModal') public staticModal: ModalDirective;
	@ViewChild('passwordRequiredInput') focusInput:ElementRef;
}
