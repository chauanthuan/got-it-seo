import { Component, OnInit,ViewChild, ElementRef, TemplateRef, Inject, PLATFORM_ID  } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import {TranslateService,LangChangeEvent} from '@ngx-translate/core';
import { MenuComponent} from '../menu/menu.component';
import { Title, Meta }     from '@angular/platform-browser';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  	constructor(
	  	public translate: TranslateService, 
	  	private titleService: Title,
	  	private meta: Meta,
	  	@Inject(PLATFORM_ID) private platformId: Object,
	  	) {
	  	translate.get('about_gotit_title').subscribe((res: string) => {
	        this.titleService.setTitle(res);
	    });
	    this.meta.addTags([
	      {name: 'description', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
	      {name: 'copyright', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
	      {name: 'keywords', content: 'quà tặng doanh nghiệp , quà tặng đối tác , voucher quà tặng , quà tặng khách hàng , quà tặng Tết, quà tặng sự kiện , tặng thưởng nhân viên , quà tặng doanh nhân , egift, quà tặng online, tặng quà trực tuyến, tặng quà online , gift voucher , phiếu quà tặng , quà tặng trực tuyến ,thẻ quà tặng , tặng voucher ,tặng quà online , quà online , bán gift card , phiếu tặng quà , giftcards , mua phiếu quà tặng'}
	    ]);
  	}

  	ngOnInit() {
	  	if (isPlatformBrowser(this.platformId)){
			window.scrollTo(0, 0)
		}
	  	
	    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
	      this.translate.get('about_gotit_title').subscribe((res: string) => {
	        this.titleService.setTitle(res);
	      });
	    });
  	}

}
