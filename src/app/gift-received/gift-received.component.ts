import { Component, OnInit, ElementRef, ViewChild ,Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import {MenuService} from '../menu/menu.service';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import {Http, Headers, Response, RequestOptions,URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {Router} from '@angular/router';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import {ModalDirective} from 'ngx-bootstrap/modal';
import { environment } from '../../environments/environment';
import { DatePipe } from '@angular/common';
import {TranslateService,LangChangeEvent} from '@ngx-translate/core';
import { Title, Meta }     from '@angular/platform-browser';

declare var $: any;

@Component({
  selector: 'app-gift-received',
  templateUrl: './gift-received.component.html',
  styleUrls: ['./gift-received.component.scss'],
   host: {
    '(document:click)': 'onDocumentClick($event)',
    '(window:resize)': 'onResize($event)',
    '(window:scroll)': 'onScroll($event)'
  },
})
export class GiftReceivedComponent implements OnInit {
  @ViewChild('iframe') iframe: ElementRef;
  @ViewChild('wheretouseModal') public wheretouseModal: ModalDirective;
  @ViewChild('productDesModal') public productDesModal: ModalDirective;
  @ViewChild('termconditionModal') public termconditionModal: ModalDirective;

	filterDropdownClicked:boolean = false;
	isClicked:boolean = false;
  isSortClicked:boolean = false;
	filterValue: any =[];
	filterValueSelected: any = [];
	sort_val: string;
	user: Observable<firebase.User>;
	voucherData: any = [];
	voucherDataTemp:any = [];
	voucherDataTempLoadMore;
	user_uid:string;
	userData: any;
  numOfVoucher:number = 0;
	db:any;
	lastFirebaseLoaded: any;
	limit:number = 4;
	showLoadBtn:boolean = false;
	loadMoreClicked: boolean = false;
	dataLoaded:boolean = false;
	loadMoreLoading: boolean = false;
  productDesc: string;
  productTerm: string;
	constructor(public _menuService: MenuService,
       private afAuth: AngularFireAuth,
       private _http: Http,
       private router: Router, 
       public el: ElementRef,
       private datePipe: DatePipe,
       public translate: TranslateService,
       private titleService: Title,
       private meta: Meta,
        @Inject(PLATFORM_ID) private platformId: Object) {
    // this.afAuth.auth.onAuthStateChanged(auth=>{
   //    if(!auth){
   //      this.router.navigate(['/']); 
   //    }
   //    else{
   //      // console.log(auth);
   //      this.user_uid = auth.uid;
   //    }
   //  });
    this.db = firebase.firestore();
    translate.get('gift_received_title').subscribe((res: string) => {
        this.titleService.setTitle(res);
    });
    this.meta.addTags([
      {name: 'description', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
      {name: 'copyright', content: 'Got It - Quà tặng trao ngay, mọi lúc mọi nơi. Tặng quà dễ dàng hơn với Got It'},
      {name: 'keywords', content: 'quà tặng doanh nghiệp , quà tặng đối tác , voucher quà tặng , quà tặng khách hàng , quà tặng Tết, quà tặng sự kiện , tặng thưởng nhân viên , quà tặng doanh nhân , egift, quà tặng online, tặng quà trực tuyến, tặng quà online , gift voucher , phiếu quà tặng , quà tặng trực tuyến ,thẻ quà tặng , tặng voucher ,tặng quà online , quà online , bán gift card , phiếu tặng quà , giftcards , mua phiếu quà tặng, quản lý quà tặng, quà đã nhận, quà tặng đã nhận'}
    ]);
  }
  	
  	ngOnInit() {
      if (isPlatformBrowser(this.platformId)){
    		window.scrollTo(0, 0)
    		this.user_uid = firebase.auth().currentUser.uid;
    		this._menuService.show();
    		var w_width = $(window).width();
    		var w_height = $(window).height();
    		var footer_height = $('footer').height();
    		$('.managegift-section .parent').css({'min-height':(w_height - footer_height - 60)+'px'})
    		
    		var content_width = $('.managegift-section .parent').width();
    		var path_right_width = (w_width - content_width)/2;
    		$('.left-fixed-wrap').css({'width':path_right_width});
      }

  		this.filterValue = [
  				{
  					"name":"status_active",
  					"value":3
  				},
  				{
  					"name":"status_expired",
  					"value":8
  				},
  				{
  					"name":"status_used",
  					"value": 4
  				}
  			];

  		var db = firebase.firestore();
  		this.voucherData = [];
  		var first = db.collection('user').doc(this.user_uid).collection("received").orderBy("state", "asc");
     	this.loadDataFireStore(first);

  		this.filterValueSelected.push(this.filterValue[0]);
  		this.sort_val = 'expiry';
  		this.filterDataFuction();



  		db.collection('user').doc(this.user_uid).collection("received").where('state','==',3).get().then(snapshot => {
  			let total_count = 0;
  			snapshot.forEach(doc => {
  				total_count += doc.data().price;
  			});

  			console.log(total_count);
  		});

      this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
        this.translate.get('gift_received_title').subscribe((res: string) => {
          this.titleService.setTitle(res);
        });
      });
  	}

  	loadDataFireStore(collection, loadFirst = true){
  		//count all document
  		if(loadFirst){
  			collection.get().then((snapshot)=> {  
			    this.numOfVoucher = snapshot.size;
			    if(this.numOfVoucher < this.limit){
			    	this.showLoadBtn = false;
			    }
			    else{
			    	this.showLoadBtn = true;
			    }
        });
  		}
  		
  		if(!this.loadMoreClicked){
        return collection.limit(this.limit).onSnapshot((snapshot)=>{
          this.voucherDataTemp = [];
        this.lastFirebaseLoaded = snapshot.docs[snapshot.docs.length-1];
          if(snapshot.size < this.limit){
            this.showLoadBtn = false;
          }
          else{
            this.showLoadBtn = true;
          }
          this.voucherDataTemp = this.voucherDataTemp.concat(snapshot.docs);
          this.dataLoaded = true;
          this.loadMoreLoading = false;
        });
  		}
      else{
        collection = collection.startAfter(this.lastFirebaseLoaded);
        return collection.limit(this.limit).get().then((snapshot)=>{
       // Get the last visible document
          this.lastFirebaseLoaded = snapshot.docs[snapshot.docs.length-1];

          if(snapshot.size < this.limit){
           this.showLoadBtn = false;
          }
          else{
           this.showLoadBtn = true;
          }
          this.voucherDataTemp = this.voucherDataTemp.concat(snapshot.docs);
          this.dataLoaded = true;
          this.loadMoreLoading = false;
        }).catch(err => {
           console.log("Error getting sub-collection documents", err);
         });
      }	
   	}

  	loadMore(){
  		this.loadMoreLoading = true;
  		this.loadMoreClicked = true;
      this.dataLoaded = false;
		  this.filterDataFuction();
  	}
	
  	
  	updateCheckedOptions(option, event) {
  		this.dataLoaded = false;
  		this.showLoadBtn = false;
  		this.loadMoreClicked = false;
  		this.voucherDataTemp = [];
  		if(event.target.checked){
  			this.filterValueSelected.push(option);
  		}
  		else{
  			const index: number = this.filterValueSelected.indexOf(option);
		    if (index !== -1) {
		        this.filterValueSelected.splice(index, 1);
		    }    
  		}
  		
  		this.filterDataFuction();
	}

	
	onSortChange(val) {
	    this.sort_val = val;
	    this.dataLoaded = false;
  		this.showLoadBtn = false;
  		this.loadMoreClicked = false;
  		this.voucherDataTemp = [];
  		this.filterDataFuction();
	}
  

	filterDataFuction(){
		var filterCollection:any = {};
  		var voucherCollection = this.db.collection('user').doc(this.user_uid).collection("received");
  		var sortValue = '';
  		if(this.sort_val == 'value'){
  			sortValue = 'price';
  		}
  		else if(this.sort_val == 'brand'){
  			sortValue = 'brandName';
  		}
  		else if(this.sort_val == 'expiry'){
  			sortValue = 'expired_date';
  		}
  		if(this.filterValueSelected.length > 0){
  			this.filterValueSelected.sort( function(object1, object2) {
			    if ( object1.value < object2.value ){
			    	return -1;
			    }else if( object1.value > object2.value ){
			        return 1;
			    }else{
			    	return 0;	
			    }
			});
  			//check filter choose one or more
  			
  			if(this.filterValueSelected.length == 1){
  				
  				filterCollection = voucherCollection.where("state","==",this.filterValueSelected[0].value);
  				
  				if(sortValue != ''){
		  			this.loadDataFireStore(filterCollection.orderBy(sortValue), true);
		  		}
		  		else{
		  			this.loadDataFireStore(filterCollection, true);
		  		}
  				return;
  			}
  			else if(this.filterValueSelected.length == 2){
  				
  				if(this.filterValueSelected[0].value == 3){
  					if(this.filterValueSelected[1].value == 8){
  						filterCollection = voucherCollection.where("used","==",false);
  					}
  					else if(this.filterValueSelected[1].value == 4){
  						filterCollection = voucherCollection.where("state",">=",3).where("state","<=",4);
  					}
  				}
  				else{
  					filterCollection = voucherCollection.where("state","<=",8).where("state",">=",4);
  				}
  			}
  			else{
  		    //choose all 
  				filterCollection = voucherCollection.where("state","<=",8);
  			}		
  		}
  		else{

  			filterCollection = voucherCollection.where("state","<=",8);
  		}

  		if(sortValue != ''){
  			this.loadDataFireStore(filterCollection.orderBy('state').orderBy(sortValue), true);
  		}
  		else{
  			this.loadDataFireStore(filterCollection.orderBy('state'), true);
  		}
	}

	calculateDay(date){
		let date1 = new Date();//today
		let date2 = new Date(date);//this is expired date
		var timeDiff = date2.getTime() - date1.getTime();
		let dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
		return dayDifference;
	}

  	toggleClick(event):void{
	    this.isClicked = !this.isClicked;
	    var target = event.currentTarget;
	    var idAttr = target.attributes.id.nodeValue;
	    if(idAttr == 'filter'){
	      this.filterDropdownClicked = !this.filterDropdownClicked;
	    }

	}
  toggleClick2(event):void{
    console.log('togle clock 2');
      this.isSortClicked = !this.isSortClicked;
      var target = event.currentTarget;
      var idAttr = target.attributes.id.nodeValue;
      if(idAttr == 'filter'){
        this.filterDropdownClicked = !this.filterDropdownClicked;
      }
  }
  chooseSortCl(event){
    console.log('emem click 2')
    this.isSortClicked = false;
    event.target.closest(".filter_right_selectbox").querySelector('span').innerHTML = event.target.innerText;

  }
	elementClicked(event){
	  	console.log(event);
	    //event.target.closest(".wrapper-dropdown").querySelector('small').innerHTML = event.target.innerText;
	}

	onDocumentClick(event) {
	    if (!event.target.closest(".filter_box")) { 
	        this.filterDropdownClicked = false;
	    }
	}

	onResize(event){
     if (isPlatformBrowser(this.platformId)){
	    var w_width = $(window).width();
  		var w_height = $(window).height();
  		var footer_height = $('footer').height();
  		$('.managegift-section .parent').css({'min-height':(w_height - footer_height - 60)+'px'});

  		var content_width = $('.managegift-section .parent').width();
  		if(w_width >= content_width){
  		  var path_right_width = (w_width - content_width)/2;
  		}
  		else{
  		  path_right_width = 0;
  		}
  		$('.left-fixed-wrap').css({'width':path_right_width});
    }
	}

	@ViewChild('lazyPoint') lazyPoint: ElementRef;
	onScroll(event){
     if (isPlatformBrowser(this.platformId)){
    	if(this.dataLoaded && this.lastFirebaseLoaded != undefined){
    		if ((window.innerHeight + window.pageYOffset) >= ($('.managegift-section').height() + 60)) {
	    		this.loadMore();
	    	}
    	}
    	
		}
	}
	removeSelected(option,event){
	  	const index: number = this.filterValueSelected.indexOf(option);
	    if (index !== -1) {
	        this.filterValueSelected.splice(index, 1);
	    }  
	  	event.target.remove();
      this.dataLoaded = false;
      this.showLoadBtn = false;
      this.loadMoreClicked = false;
      this.voucherDataTemp = [];
      this.filterDataFuction();
	}

  showModalWhereUse(brandId){
    if(brandId == 46 || brandId == environment.gotitBrandId){
      this.iframe.nativeElement.src = 'https://brand.gotit.vn/';
    }
    else{
      this.iframe.nativeElement.src = 'https://brand.gotit.vn/detail.html?brandId='+brandId+'';
    }
    this.wheretouseModal.show();
  }

  showModalProductDes(productId){
    console.log(this.datePipe.transform(new Date(), 'yyyy-MM-dd'));
    let productData: any;
    if(this.translate.currentLang == 'vi'){
       productData = JSON.parse(localStorage.getItem("product_vi_"+productId+""));
    }
    else{
      productData = JSON.parse(localStorage.getItem("product_en_"+productId+""));
    }

    if(productData){
      if(this.translate.currentLang == 'vi'){
        this.productDesc = productData.productDescVi;
      }
      else{
        this.productDesc = productData.productDescEn;
      }
      this.productDesModal.show();
      //check today with time_save_info
      //if today > time_save_info => clear localStore and save again
      let date1 = new Date();//today
      let date2 = new Date(productData.time_save_info);//this is save date
      var timeDiff = date1.getDate() - date2.getDate();
      let dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
      if(dayDifference > 0){
        let url =  environment.firebaseFnsUrl +'/app/product/detail?product_id='+productId+'&language='+this.translate.currentLang;
        let dataProduct = this._http.get(url).toPromise().then(response => response.json());
        console.log(dataProduct);
        dataProduct.then(res=>{
          if(this.translate.currentLang == 'vi'){
            localStorage.setItem("product_vi_"+res.productId,JSON.stringify({'time_save_info':this.datePipe.transform(new Date(), 'yyyy-MM-dd'),'productDescVi':res.productDesc,'termsVi':res.terms}));
          }
          else{
            localStorage.setItem("product_en_"+res.productId,JSON.stringify({'time_save_info':this.datePipe.transform(new Date(), 'yyyy-MM-dd'),'productDescEn':res.productDesc,'termsEn':res.terms}));
          }
          
          this.productDesc = res.productDesc;
          this.productDesModal.show();
        });
      }
    }
    else{
      let url =  environment.firebaseFnsUrl +'/app/product/detail?product_id='+productId+'&language='+this.translate.currentLang;
      let dataProduct = this._http.get(url).toPromise().then(response => response.json());
      console.log(dataProduct);
      dataProduct.then(res=>{
        if(this.translate.currentLang == 'vi'){
            localStorage.setItem("product_vi_"+res.productId,JSON.stringify({'time_save_info':this.datePipe.transform(new Date(), 'yyyy-MM-dd'),'productDescVi':res.productDesc,'termsVi':res.terms}));
          }
          else{
            localStorage.setItem("product_en_"+res.productId,JSON.stringify({'time_save_info':this.datePipe.transform(new Date(), 'yyyy-MM-dd'),'productDescEn':res.productDesc,'termsEn':res.terms}));
          }
        this.productDesc = res.productDesc;
        this.productDesModal.show();
      });
    }
    
  }
  showModalTermCondition(productId){
    let productData: any;
    if(this.translate.currentLang == 'vi'){
       productData = JSON.parse(localStorage.getItem("product_vi_"+productId+""));
    }
    else{
      productData = JSON.parse(localStorage.getItem("product_en_"+productId+""));
    }
    if(productData){

      if(this.translate.currentLang == 'vi'){
        this.productTerm = productData.termsVi;
      }
      else{
        this.productTerm = productData.termsEn;
      }
      this.termconditionModal.show();

      //check today with time_save_info
        //if today > time_save_info => clear localStore and save again
        let date1 = new Date();//today
        let date2 = new Date(productData.time_save_info);//this is save date
        var timeDiff = date1.getDate() - date2.getDate();
        let dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if(dayDifference > 0){
          let url =  environment.firebaseFnsUrl +'/app/product/detail?product_id='+productId+'&language='+this.translate.currentLang;
          let dataProduct = this._http.get(url).toPromise().then(response => response.json());
          dataProduct.then(res=>{
            if(this.translate.currentLang == 'vi'){
              localStorage.setItem("product_vi_"+res.productId,JSON.stringify({'time_save_info':this.datePipe.transform(new Date(), 'yyyy-MM-dd'),'productDescVi':res.productDesc,'termsVi':res.terms}));
            }
            else{
              localStorage.setItem("product_en_"+res.productId,JSON.stringify({'time_save_info':this.datePipe.transform(new Date(), 'yyyy-MM-dd'),'productDescEn':res.productDesc,'termsEn':res.terms}));
            }
            this.productTerm = res.terms;
            this.termconditionModal.show();
          });
        }
    }
    else{
      let url =  environment.firebaseFnsUrl +'/app/product/detail?product_id='+productId+'&language='+this.translate.currentLang;
      let dataProduct = this._http.get(url).toPromise().then(response => response.json());
      dataProduct.then(res=>{
        if(this.translate.currentLang == 'vi'){
          localStorage.setItem("product_vi_"+res.productId,JSON.stringify({'time_save_info':this.datePipe.transform(new Date(), 'yyyy-MM-dd'),'productDescVi':res.productDesc,'termsVi':res.terms}));
        }
        else{
          localStorage.setItem("product_en_"+res.productId,JSON.stringify({'time_save_info':this.datePipe.transform(new Date(), 'yyyy-MM-dd'),'productDescEn':res.productDesc,'termsEn':res.terms}));
        }
        this.productTerm = res.terms;
        this.termconditionModal.show();

      });
    }
  }

}
