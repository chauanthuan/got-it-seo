import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperatingregulationComponent } from './operatingregulation.component';

describe('OperatingregulationComponent', () => {
  let component: OperatingregulationComponent;
  let fixture: ComponentFixture<OperatingregulationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperatingregulationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperatingregulationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
