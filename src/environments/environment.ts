// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,

  //this is gotit web project
  // firebase: {
  //   apiKey: "AIzaSyCxkQFg9phrFc0DTDvxcHNKVivJHxSkUcM",
  //   authDomain: "got-it-c2c.firebaseapp.com",
  //   databaseURL: "https://got-it-c2c.firebaseio.com",
  //   projectId: "got-it-c2c",
  //   storageBucket: "",
  //   messagingSenderId: "740062475980"
  // },


  //got it dev
  firebase : {
    apiKey: "AIzaSyCLPhzd2nQ6ONXl7AIHSTE0lTjBgTFNHsM",
    authDomain: "got-it-c2c-dev.firebaseapp.com",
    databaseURL: "https://got-it-c2c-dev.firebaseio.com",
    projectId: "got-it-c2c-dev",
    storageBucket: "got-it-c2c-dev.appspot.com",
    messagingSenderId: "280288933219"
  },

  //this is my project
  // firebase: {
  //   apiKey: "AIzaSyDpEo4ih9eo3kgVGADnbPP8e0fDVRer-Is",
  //   authDomain: "myfirstfirebase-de46f.firebaseapp.com",
  //   databaseURL: "https://myfirstfirebase-de46f.firebaseio.com",
  //   projectId: "myfirstfirebase-de46f",
  //   storageBucket: "myfirstfirebase-de46f.appspot.com",
  //   messagingSenderId: "449222656726"
  // },
  firebaseFnsUrl:'http://localhost:5000/got-it-c2c-dev/us-central1',
  origin: 'http://localhost:4200',
  gotitBrandId: 46,
  defaultProductId: 896
};