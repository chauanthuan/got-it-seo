export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCxkQFg9phrFc0DTDvxcHNKVivJHxSkUcM",
    authDomain: "got-it-c2c.firebaseapp.com",
    databaseURL: "https://got-it-c2c.firebaseio.com",
    projectId: "got-it-c2c",
    storageBucket: "got-it-c2c.appspot.com",
    messagingSenderId: "740062475980"
  },
  // apiUrl: 'https://biz.gotit.vn',
  firebaseFnsUrl:'https://us-central1-got-it-c2c.cloudfunctions.net',
  origin: 'https://beta.gotit.vn',
  gotitBrandId: 46,
  defaultProductId: 896
};
