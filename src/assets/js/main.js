$(document).ready(function(){
  
 
  

  //------------------------------------//
  //Navbar//
  //------------------------------------//
    	var menu = $('.navbar');
    	$(window).bind('scroll', function(e){
    		if($(window).scrollTop() > 140){
    			if(!menu.hasClass('open')){
    				menu.addClass('open');
    			}
    		}else{
    			if(menu.hasClass('open')){
    				menu.removeClass('open');
    			}
    		}
    	});
  
  
  //------------------------------------//
  //Scroll To//
  //------------------------------------//
  // $(".scroll").click(function(event){		
  // 	event.preventDefault();
  // 	$('html,body').animate({scrollTop:$(this.hash).offset().top}, 800);
  	
  // });
  
  //------------------------------------//
  //Wow Animation//
  //------------------------------------// 
  // wow = new WOW(
  //       {
  //         boxClass:     'wow',      // animated element css class (default is wow)
  //         animateClass: 'animated', // animation css class (default is animated)
  //         offset:       0,          // distance to the element when triggering the animation (default is 0)
  //         mobile:       false        // trigger animations on mobile devices (true is default)
  //       }
  //     );
  //     wow.init();

  $('.list-thumb span').on('click', function(){
    var dataImg = $(this).attr('data-img');
    var urlMainImg = $('.main-img img').attr('src');
    $('.list-thumb span').removeClass('active');
    $(this).addClass('active');
    $(this).attr('data-img',urlMainImg);
    $(this).find('img').attr('data-img',urlMainImg);
    $('.show-img .main-img img').fadeOut(function () {
      $('.show-img .main-img').html('<img src="'+ dataImg +'" style="width:100%">');
    });
  });
  var w_width = $(window).width();
    var content_width = $('.product-section .parent').width();
    var path_right_width = (w_width - content_width)/2;
    $('.right-fixed-wrap').css({'width':path_right_width});

    $( window ).resize(function() {
      var w_width = $(window).width();
      var content_width = $('.product-section .parent').width();
      if(w_width >= content_width){
        var path_right_width = (w_width - content_width)/2;
      }
      else{
        path_right_width = 0;
      }
      $('.right-fixed-wrap').css({'width':path_right_width});
  });
});
